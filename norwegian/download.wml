#use wml::debian::template title="Takk for at du laster ned Debian!" 
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="1d8e9363ec81c198712c4ba45ff77dd67145b026" maintainer="Hans F. Nordhaug"

{#meta#:
<meta http-equiv="refresh" content="3;url=<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">
:#meta#}

<p>Dette er Debian <:=substr '<current_initial_release>', 0, 2:>, med kodenavn <em><current_release_name></em>, netinst, for <: print $arches{'amd64'}; :>.</p>

<p>Hvis nedlastingen din ikke starter automatisk, klikk
<a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso</a>.</p>

<p>Last ned sjekksum: <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS">SHA512SUMS</a>
og tilhørende <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS.sign">signatur</a></p>

<div class="tip">
  <p><strong>Viktig</strong>: Sørg for at du <a href="$(HOME)/CD/verify">verifiserer nedlastingen din med sjekksummen</a>.</p>
</div>

<p>Debian ISO-er for installasjon er hybrid bildefiler, som betyr at de kan skrives
direkte til CD/DVD/BD-media eller til <a href="https://www.debian.org/CD/faq/#write-usb">USB-minnepinner</a>.</p>

<h2>Andre installasjonsprogram</h2>

<p>Andre installasjonsprogram og bildefiler, som live-system, offline-installasjonsprogram 
for system uten nettverksforbindelse, installasjonsprogram for andre CPU-arkitekturer, 
eller sky-instanser, fins på <a href="$(HOME)/distrib/">Få tak i Debian</a>.</p>

<p>Uoffisielle installasjonsprogram med <a href="https://wiki.debian.org/Firmware"><strong>ufri fastvare</strong></a>,
nyttig for noen adaptere for nettverk og video, kan lastes ned fra
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">Uoffisielle installasjonsbilder med fastvare inkludert</a>.</p>

<h2>Relaterte lenker</h2>

<p><a href="$(HOME)/releases/<current_release_name>/installmanual">Installasjonshåndbok</a></p>
<p><a href="$(HOME)/releases/<current_release_name>/releasenotes">Utgivelsesmerknader</a></p>
<p><a href="$(HOME)/CD/verify">Håndbok for verifikasjon av ISO-filer</a></p>
<p><a href="$(HOME)/CD/http-ftp/#mirrors">Alternative nettsteder for nedlasting</a></p>
<p><a href="$(HOME)/releases">Andre utgivelser</a></p>
