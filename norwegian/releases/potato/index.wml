#use wml::debian::template title="Debian GNU/Linux 2.2 &ldquo;potato&rdquo;-utgivelsesinformasjon" BARETITLE="true"
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/potato/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="552f9232b5d467c677963540a5a4236c824e87b9" maintainer="Hans F. Nordhaug"

<p>
  Debian GNU/Linux 2.2 (med kodenavn <em>potato</em>) ble først utgitt <:=spokendate('2000-08-14'):>.
  Siste punktutgave av Debian 2.2 er <current_release_potato>, utgitt
  <a href="$(HOME)/News/<current_release_newsurl_potato/>"><current_release_date_potato></a>.</p>
</p>

<p><strong>
 Debian GNU/Linux 2.2 er erstattet av <a href="../woody/">Debian GNU/Linux 3.0 (<q>woody</q>)</a>.
 Sikkerhetsoppdateringer opphørte ved utgangen av juni 2003.
</strong></p>

<p>For informasjon om hovedendringene i denne utgaven, les 
<a href="releasenotes">utgivelsesmerknadene</a> og den offisielle 
<a href="$(HOME)/News/2000/20000815">pressemeldingen</a>.</p>

<p>Debian GNU/Linux 2.2 er dedikert til minnet om Joel "Espy" Klecker, 
en Debian utvikler, ukjent for de fleste i Debian-prosjekt. Han var 
plaget av en sykdom kjent som Duchennes muskeldystrofi under mesteparten
av hans tid i prosjektet. I ettertid har prosjektet sett rekkevidden av hans 
dedikasjon til prosjektet, og derfor som et tegn på verdsettelse og til minne
om hans inspirerende liv, dedikert denne utgaven av Debian GNU/Linux til ham.</p>

<p>Følgende datamaskinarkitekturer var støttet i denne utgaven:</p>

<ul>
<: foreach $arch (@arches) {
      print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
   }
:>
</ul>

<p>
  I motsetning til våre ønsker kan det være problemer i denne utgave selvom
  den er erklært <em>stabil</em>. Vi har lagd <a href="errata">en list med de
  viktigste kjente problemene</a>, og du kan alltid 
  <a href="reportingbugs">rapportere andre problem</a> til oss.
</p>

<p>
  Sist, men ikke minst, har vi en liste med <a href="credits">folk som har
  sørget for at denne utgaven ble utgitt</a>.
</p>
