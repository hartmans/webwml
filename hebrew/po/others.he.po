msgid ""
msgstr ""
"Project-Id-Version: Debian webwml others\n"
"PO-Revision-Date: 2008-09-28 22:40+0200\n"
"Last-Translator: Oz Nahum <nahumoz@gmail.com>\n"
"Language-Team: Hebrew <he@li.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "צעד 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "צעד 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "צעד 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "צעד 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "צעד 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "צעד 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "צעד 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "רשימת הצעדים של המועמד"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
#, fuzzy
msgid "More information"
msgstr "מידע נוסף"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""

#: ../../english/distrib/pre-installed.defs:18
#, fuzzy
msgid "Phone"
msgstr "טלפון:"

#: ../../english/distrib/pre-installed.defs:19
#, fuzzy
msgid "Fax"
msgstr "פקס:"

#: ../../english/distrib/pre-installed.defs:21
#, fuzzy
msgid "Address"
msgstr "כתובת:"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "מוצרים"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "חולצות טי"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "כובעים"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "מדבקות"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "ספלים"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "פרטי לבוש אחרים"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "חולצות פולו"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "צלחות מעופפות"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "משטחים לעכבר"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "תגים"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr ""

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "עגילים"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "מזוודות"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr ""

#: ../../english/events/merchandise.def:56
#, fuzzy
msgid "pillowcases"
msgstr "מזוודות"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr ""

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr ""

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr ""

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr ""

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr ""

#: ../../english/events/merchandise.def:90
#, fuzzy
msgid "Available languages:"
msgstr "שפות:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr ""

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr ""

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr ""

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr ""

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr ""

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr ""

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr ""

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr ""

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr ""

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr ""

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr ""

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "כמו למעלה"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr ""

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr ""

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr ""

#~ msgid "Mailing List Subscription"
#~ msgstr "הרשמה לרשימת התפוצה"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription "
#~ "web form</a> is also available, for unsubscribing from mailing lists. "
#~ msgstr ""
#~ "בקרו בעמוד <a href=\"./#subunsub\">רשימות תפוצה</a>על מנת לקבל מידע נוסף "
#~ "כיצד יש להסיר את שמכם מרשימות התפוצה באמצעות שליחת דואר אלקטרוני. ניתן "
#~ "לעשות זאת גם על ידי מילוי טופס בדף האינטרט  <a href=\"unsubscribe\">טופס "
#~ "הסרה מרשימות תפוצה</a>. "

#~ msgid ""
#~ "Note that most Debian mailing lists are public forums. Any mails sent to "
#~ "them will be published in public mailing list archives and indexed by "
#~ "search engines. You should only subscribe to Debian mailing lists using "
#~ "an e-mail address that you do not mind being made public."
#~ msgstr ""
#~ "שימו לב לכך שמרבית רשימות התפוצה של דביאן הינן גלויות לציבור. כל דואר "
#~ "שיישלח אליהן יפורסם ברבים, יאוכסן בארכיב ויימצא על ידי מנועי חיפוש. עליכם "
#~ "להרשם לרשימות התפוצה של דביאן באמצעות כתובת דואר אלקטרוני אשר לא אכפת לכם "
#~ "שתהיה גלויה לציבור.  "

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "נא בחרו לאיזו רשימת תפוצה ברצונכם להרשם:"

#~ msgid "No description given"
#~ msgstr "לא ניתן תיאור"

#~ msgid "Moderated:"
#~ msgstr "מפוקח"

#~ msgid "Posting messages allowed only to subscribers."
#~ msgstr "רק משתמשים רשומים יכולים להשאיר הודעות"

#~ msgid ""
#~ "Only messages signed by a Debian developer will be accepted by this list."
#~ msgstr "רק הודעות חתומות על ידי מפתח דביאן יתקבלו לרשימה זו"

#~ msgid "Subscription:"
#~ msgstr "הרשמה"

#~ msgid "is a read-only, digestified version."
#~ msgstr "רשימת תפוצה במצב קריאה בלבד"

#~ msgid "Your E-Mail address:"
#~ msgstr "כתובת הדואר האלקטרוני שלך"

#~ msgid "Subscribe"
#~ msgstr "הרשמה"

#~ msgid "Clear"
#~ msgstr "נקה"

#~ msgid ""
#~ "Please respect the <a href=\"./#ads\">Debian mailing list advertising "
#~ "policy</a>."
#~ msgstr "נא לכבד את  <a href=\"./#ads\"> נהלי הפרסום ברשימות התפוצה של דביאן"

#~ msgid "Mailing List Unsubscription"
#~ msgstr "הסרה מרשימת התפוצה"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription "
#~ "web form</a> is also available, for subscribing to mailing lists. "
#~ msgstr ""
#~ "בקרו בעמוד <a href=\"./#subunsub\">רשימות תפוצה</a>על מנת לקבל מידע נוסף "
#~ "כיצד יש להסיר את שמכם מרשימות התפוצה באמצעות שליחת דואר אלקטרוני. ניתן "
#~ "לעשות זאת גם על ידי מילוי טופס בדף האינטרט  <a href=\"unsubscribe\">טופס "
#~ "הסרה מרשימות תפוצה</a>. "

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "נא בחרו מאיזו רשימת תפוצה להסיר את שמכם:"

#~ msgid "Unsubscribe"
#~ msgstr "הסרה מרשימת תפוצה"

#~ msgid "open"
#~ msgstr "פתוח"

#~ msgid "closed"
#~ msgstr "סגור"

#~ msgid "Name:"
#~ msgstr "שם:"

#~ msgid "Company:"
#~ msgstr "חברה:"

#~ msgid "or"
#~ msgstr "או"

#~ msgid "Email:"
#~ msgstr "דואר אלקטרוני:"

#~ msgid "Rates:"
#~ msgstr "קצבים:"

#~ msgid "Willing to Relocate"
#~ msgstr "מוכן לעבור מקום מגורים"

#~ msgid ""
#~ "<total_consultant> Debian consultants listed in <total_country> countries "
#~ "worldwide."
#~ msgstr ""
#~ "<total_consultant> יועצים לדביאן <total_country> מדינות ברחבי העולם."

#~ msgid "Previous Talks:"
#~ msgstr "שיחות קודמות:"

#~ msgid "Location:"
#~ msgstr "מיקום"

#~ msgid "Topics:"
#~ msgstr "נושאים:"

#~ msgid "Where:"
#~ msgstr "איפה:"

#~ msgid "Specifications:"
#~ msgstr "מפרט טכני:"

#~ msgid "Architecture:"
#~ msgstr "ארכיטקטורה:"

#~ msgid "Who:"
#~ msgstr "מי:"

#~ msgid "Wanted:"
#~ msgstr "דרושים:"

#~ msgid "Version"
#~ msgstr "גרסה"

#~ msgid "Status"
#~ msgstr "מצב"

#~ msgid "Package"
#~ msgstr "חבילה"

#~ msgid "ALL"
#~ msgstr "הכל"

#~ msgid "Unknown"
#~ msgstr "לא ידוע"

#~ msgid "BAD?"
#~ msgstr "לא טוב ?"

#~ msgid "OK?"
#~ msgstr "טוב ?"

#~ msgid "BAD"
#~ msgstr "לא טוב"

#~ msgid "OK"
#~ msgstr "טוב"

#~ msgid "Old banner ads"
#~ msgstr "מודעות ישנות"

#~ msgid "Download"
#~ msgstr "הורדה"

#~ msgid "Unavailable"
#~ msgstr "לא זמין"

#~ msgid "No kernel"
#~ msgstr "אין קרנל"

#~ msgid "Not yet"
#~ msgstr "עדיין לא"

#~ msgid "Building"
#~ msgstr "בונה"

#~ msgid "Booting"
#~ msgstr "מאתחל"

#~ msgid "sarge (broken)"
#~ msgstr "סרג' (שבור)"

#~ msgid "sarge"
#~ msgstr "סרג'"

#~ msgid "Working"
#~ msgstr "עובד"
