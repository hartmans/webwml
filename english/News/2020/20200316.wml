<define-tag pagetitle>Official communication channels for Debian</define-tag>
<define-tag release_date>2020-03-16</define-tag>
#use wml::debian::news

<p>
From time to time, we get questions in Debian about our official channels of
communication and questions about the Debian status of who may own similarly
named websites.
</p>

<p>
The main Debian website <a href="https://www.debian.org">www.debian.org</a>
is our primary medium of communication. Those seeking information about current
events and development progress in the community may be interested in the
<a href="https://www.debian.org/News/">Debian News</a> section of the Debian
website.

For less formal announcements, we have the official Debian blog
<a href="https://bits.debian.org">Bits from Debian</a>,
and the <a href="https://micronews.debian.org">Debian micronews</a>
service for shorter news items.
</p>

<p>
Our official newsletter
<a href="https://www.debian.org/News/weekly/">Debian Project News</a>
and all official announcements of news or project changes are dual posted on
our website and sent to our official mailing lists
<a href="https://lists.debian.org/debian-announce/">debian-announce</a> or
<a href="https://lists.debian.org/debian-news/">debian-news</a>.
Posting to those mailing lists is restricted.
</p>

<p>
We also want to take the opportunity to announce how the Debian Project,
or for short, Debian is structured.
</p>

<p>
Debian has a structure regulated by our
<a href="https://www.debian.org/devel/constitution">Constitution</a>.
Officers and delegated members are listed on our
<a href="https://www.debian.org/intro/organization">Organizational Structure</a> page.
Additional teams are listed on our <a href="https://wiki.debian.org/Teams">Teams</a> page.
</p>

<p>
The complete list of official Debian members can be found on our
<a href="https://nm.debian.org/members">New Members page</a>,
where our membership is managed. A broader list of Debian contributors can be
found on our <a href="https://contributors.debian.org">Contributors</a> page.
</p>

<p>
If you have questions, we invite you to reach the press team at
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>

<h2>About Debian</h2>
<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely free
operating system Debian.</p>

<h2>Contact Information</h2>
<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
