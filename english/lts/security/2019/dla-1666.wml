<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>For the FreeRDP version in Debian jessie LTS a security and functionality
update has recently been provided. FreeRDP is a free re-implementation
of the Microsoft RDP protocol (server and client side) with freerdp-x11
being the most common RDP client these days.</p>

<p>Functional improvements:</p>

     <p>With help from FreeRDP upstream (cudos to Bernhard Miklautz and
     Martin Fleisz) we are happy to announce that RDP proto v6 and CredSSP
     v3 support have been backported to the old FreeRDP 1.1 branch.</p>

     <p>Since Q2/2018, Microsoft Windows servers and clients received an
     update that defaulted their RDP server to proto version 6. Since this
     change, people have not been able anymore to connect to recently
     updated MS Windows machines using old the FreeRDP 1.1 branch as found
     in Debian jessie LTS and Debian stretch.</p>

     <p>With the recent FreeRDP upload to Debian jessie LTS, connecting to
     up-to-date MS Windows machines is now again possible.</p>

<p>Security issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8786">CVE-2018-8786</a>

     <p>FreeRDP contained an integer truncation that lead to a heap-based
     buffer overflow in function update_read_bitmap_update() and resulted
     in a memory corruption and probably even a remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8787">CVE-2018-8787</a>

     <p>FreeRDP contained an integer overflow that leads to a heap-based
     buffer overflow in function gdi_Bitmap_Decompress() and resulted in a
     memory corruption and probably even a remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8788">CVE-2018-8788</a>

     <p>FreeRDP contained an out-of-bounds write of up to 4 bytes in function
     nsc_rle_decode() that resulted in a memory corruption and possibly
     even a remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8789">CVE-2018-8789</a>

     <p>FreeRDP contained several out-of-bounds reads in the NTLM
     authentication module that resulted in a denial of service
     (segfault).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these security problems have been fixed in version
1.1.0~git20140921.1.440916e+dfsg1-13~deb8u3.</p>

<p>We recommend that you upgrade your freerdp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1666.data"
# $Id: $
