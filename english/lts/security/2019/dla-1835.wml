<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities were discovered in Python, an interactive
high-level object-oriented language, including</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14647">CVE-2018-14647</a>

    <p>Python's elementtree C accelerator failed to initialise Expat's hash
    salt during initialization. This could make it easy to conduct
    denial of service attacks against Expat by constructing an XML
    document that would cause pathological hash collisions in Expat's
    internal data structures, consuming large amounts CPU and RAM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a>

    <p>Improper Handling of Unicode Encoding (with an incorrect netloc)
    during NFKC normalization resulting in information disclosure
    (credentials, cookies, etc. that are cached against a given
    hostname).  A specially crafted URL could be incorrectly parsed to
    locate cookies or authentication data and send that information to
    a different host than when parsed correctly.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>

    <p>An issue was discovered in urllib where CRLF injection is possible
    if the attacker controls a url parameter, as demonstrated by the
    first argument to urllib.request.urlopen with \r\n (specifically in
    the query string after a ? character) followed by an HTTP header or
    a Redis command.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9947">CVE-2019-9947</a>

    <p>An issue was discovered in urllib where CRLF injection is possible
    if the attacker controls a url parameter, as demonstrated by the
    first argument to urllib.request.urlopen with \r\n (specifically in
    the path component of a URL that lacks a ? character) followed by an
    HTTP header or a Redis command. This is similar to the <a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>
    query string issue.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.4.2-1+deb8u3.</p>

<p>We recommend that you upgrade your python3.4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1835.data"
# $Id: $
