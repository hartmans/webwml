<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A memory disclosure vulnerability was discovered in OpenJDK, an
implementation of the Oracle Java platform, resulting in information
disclosure or bypass of sandbox restrictions.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
7u211-2.6.17-1~deb8u1.</p>

<p>We recommend that you upgrade your openjdk-7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1732.data"
# $Id: $
