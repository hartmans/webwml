<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An attacker who is logged into OTRS, a Ticket Request System, as an
agent with write permissions for statistics can inject arbitrary code
into the system. This can lead to serious problems like privilege
escalation, data loss, and denial of service. This issue is also known
as <a href="https://security-tracker.debian.org/tracker/CVE-2017-14635">CVE-2017-14635</a> 
and is resolved by upgrading to the latest upstream release of OTRS3.</p>

<p><strong>IMPORTANT UPGRADE NOTES</strong></p>

<p>This update requires manual intervention. We strongly recommend to
backup all files and databases before upgrading. If you use the MySQL
backend you should read Debian bug report #707075 and the included
README.Debian file which will provide further information.</p>

<p>If you discover that the maintenance mode is still activated after the
update, we recommend to remove /etc/otrs/maintenance.html and
/var/lib/otrs/httpd/htdocs/maintenance.html which will resolve the issue.</p>

<p>In addition the following security vulnerabilities were also addressed:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-1695">CVE-2014-1695</a>

    <p>Cross-site scripting (XSS) vulnerability in OTRS allows remote
    attackers to inject arbitrary web script or HTML via a crafted HTML
    email</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2553">CVE-2014-2553</a>

    <p>Cross-site scripting (XSS) vulnerability in OTRS allows remote
    authenticated users to inject arbitrary web script or HTML via
    vectors related to dynamic fields</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2554">CVE-2014-2554</a>

    <p>OTRS allows remote attackers to conduct clickjacking attacks via an
    IFRAME element</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.3.18-1~deb7u1.</p>

<p>We recommend that you upgrade your otrs2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1119.data"
# $Id: $
