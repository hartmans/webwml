<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12872">CVE-2017-12872</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-12868">CVE-2017-12868</a>

     <p>The (1) Htpasswd authentication source in the authcrypt module and (2)
     SimpleSAML_Session class in SimpleSAMLphp 1.14.11 and earlier allow
     remote attackers to conduct timing side-channel attacks by leveraging
     use of the standard comparison operator to compare secret material
     against user input.</p>

     <p><a href="https://security-tracker.debian.org/tracker/CVE-2017-12868">CVE-2017-12868</a> was a about an improper fix of <a href="https://security-tracker.debian.org/tracker/CVE-2017-12872">CVE-2017-12872</a> in the
     initial patch released by upstream. We have used the correct patch.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.13.1-2+deb8u2.</p>

<p>We recommend that you upgrade your simplesamlphp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1408.data"
# $Id: $
