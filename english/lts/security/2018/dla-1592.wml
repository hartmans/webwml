<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities were discovered in OTRS, a Ticket Request
System, that may lead to privilege escalation or arbitrary file write.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19141">CVE-2018-19141</a>

    <p>An attacker who is logged into OTRS as an admin user may manipulate
    the URL to cause execution of JavaScript in the context of OTRS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19143">CVE-2018-19143</a>

    <p>An attacker who is logged into OTRS as a user may manipulate the
    submission form to cause deletion of arbitrary files that the OTRS
    web server user has write access to.</p></li>

</ul>

<p>Please also read the upstream advisory for <a href="https://security-tracker.debian.org/tracker/CVE-2018-19141">CVE-2018-19141</a>. 
If you think you might be affected then you should consider to run the
mentioned clean-up SQL statements to remove possible affected records.</p>

<p><url "https://community.otrs.com/security-advisory-2018-09-security-update-for-otrs-framework/"></p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.3.18-1+deb8u7.</p>

<p>We recommend that you upgrade your otrs2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1592.data"
# $Id: $
