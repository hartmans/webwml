<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several heap-based buffer over-reads were found in discount, an
implementation of the Markdown markup language in C, that allowed
remote attackers to cause a denial-of-service via specially crafted
files.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.7-1+deb8u1.</p>

<p>We recommend that you upgrade your discount packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1499.data"
# $Id: $
