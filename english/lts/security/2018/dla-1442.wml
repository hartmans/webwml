<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two flaws were discovered in mailman, a web-based mailing list manager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0618">CVE-2018-0618</a>

    <p>Toshitsugu Yoneyama of Mitsui Bussan Secure Directions, Inc.
    discovered that mailman is prone to a cross-site scripting flaw
    allowing a malicious listowner to inject scripts into the listinfo
    page, due to not validated input in the host_name field.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13796">CVE-2018-13796</a>

    <p>Hammad Qureshi discovered a content spoofing vulnerability with
    invalid list name messages in the web UI.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.1.18-2+deb8u3.</p>

<p>We recommend that you upgrade your mailman packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1442.data"
# $Id: $
