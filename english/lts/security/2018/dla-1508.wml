<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2016-10728">CVE-2016-10728</a>
      If an ICMPv4 error packet is received as the first packet on a flow
      in the to_client direction, it can lead to missed TCP/UDP detection
      in packets arriving afterwards.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.0.7-2+deb8u1.</p>

<p>We recommend that you upgrade your suricata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1508.data"
# $Id: $
