<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in SPICE before version 0.14.1 where the
generated code used for demarshalling messages lacked sufficient bounds
checks. A malicious client or server, after authentication, could send
specially crafted messages to its peer which would result in a crash or,
potentially, other impacts.</p>

<p>The issue has been fixed by upstream by bailing out with an error if the
pointer to the start of some message data is strictly greater than the
pointer to the end of the  message data.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.12.5-1+deb8u6.</p>

<p>We recommend that you upgrade your spice packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1488.data"
# $Id: $
