<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Various security issues were discovered in the poppler PDF rendering
shared library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18267">CVE-2017-18267</a>

    <p>The FoFiType1C::cvtGlyph function in fofi/FoFiType1C.cc in Poppler
    through 0.64.0 allows remote attackers to cause a denial of service
    (infinite recursion) via a crafted PDF file, as demonstrated by
    pdftops.</p>

    <p>The applied fix in FoFiType1C::cvtGlyph prevents infinite recursion
    on such malformed documents.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10768">CVE-2018-10768</a>

    <p>A NULL pointer dereference in the AnnotPath::getCoordsLength function
    in Annot.h in Poppler 0.24.5 had been discovered. A crafted input
    will lead to a remote denial of service attack. Later versions of
    Poppler such as 0.41.0 are not affected.</p>

    <p>The applied patch fixes the crash on AnnotInk::draw for malformed
    documents.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13988">CVE-2018-13988</a>

    <p>Poppler through 0.62 contains an out of bounds read vulnerability due
    to an incorrect memory access that is not mapped in its memory space,
    as demonstrated by pdfunite. This can result in memory corruption and
    denial of service. This may be exploitable when a victim opens a
    specially crafted PDF file.</p>

    <p>The applied patch fixes crashes when Object has negative number.
    (Specs say, number has to be > 0 and gen >= 0).</p>

    <p>For Poppler in Debian jessie, the original upstream patch has been
    backported to Poppler's old Object API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16646">CVE-2018-16646</a>

    <p>In Poppler 0.68.0, the Parser::getObj() function in Parser.cc may
    cause infinite recursion via a crafted file. A remote attacker can
    leverage this for a DoS attack.</p>

    <p>A range of upstream patches has been applied to Poppler's XRef.cc in
    Debian jessie to consolidate a fix for this issue.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.26.5-2+deb8u5.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1562.data"
# $Id: $
