<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security problems have been fixed in the cron daemon.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9525">CVE-2017-9525</a>

    <p>Fix group crontab to root escalation via postinst.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9704">CVE-2019-9704</a>

    <p>A very large crontab created by a user could crash the daemon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9705">CVE-2019-9705</a>

    <p>Enforce maximum crontab line count of 10000 to prevent a malicious
    user from creating an excessivly large crontab.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9706">CVE-2019-9706</a>

    <p>Fix for possible DoS by use-after-free.</p></li>

<li><p>Additionally, a bypass of /etc/cron.{allow,deny} on failure to open has 
been fixed. If these files exist, then they must be readable by the user
executing crontab(1). Users will now be denied by default if they aren't.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.0pl1-128+deb9u2.</p>

<p>We recommend that you upgrade your cron packages.</p>

<p>For the detailed security status of cron please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cron">https://security-tracker.debian.org/tracker/cron</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2801.data"
# $Id: $
