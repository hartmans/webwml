<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in btrbk, a backup tool for btrfs subvolumes.
Due to mishandling of remote hosts filtering SSH commands using
ssh_filter_btrbk.sh in authorized_keys an arbitrary code execution would
have been allowed.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
0.24.0-1+deb9u1.</p>

<p>We recommend that you upgrade your btrbk packages.</p>

<p>For the detailed security status of btrbk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/btrbk">https://security-tracker.debian.org/tracker/btrbk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2755.data"
# $Id: $
