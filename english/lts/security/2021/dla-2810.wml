<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were a number of issues in redis, a popular
key-value database system:</p>

<ul>
    <li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41099">CVE-2021-41099</a>:
    Integer to heap buffer overflow handling certain string commands and network
    payloads, when proto-max-bulk-len is manually configured to a non-default, very
    large value.</li>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32762">CVE-2021-32762</a>:
    Integer to heap buffer overflow issue in redis-cli and redis-sentinel parsing
    large multi-bulk replies on some older and less common platforms.</li>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32687">CVE-2021-32687</a>:
    Integer to heap buffer overflow with intsets, when set-max-intset-entries is
    manually configured to a non-default, very large value.</li>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32675">CVE-2021-32675</a>:
    Denial Of Service when processing RESP request payloads with a large number of
    elements on many connections.</li>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32672">CVE-2021-32672</a>:
    Random heap reading issue with Lua Debugger.</li>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32626">CVE-2021-32626</a>:
    Specially crafted Lua scripts may result with Heap buffer overflow.</li>
</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
3:3.2.6-3+deb9u8.</p>

<p>We recommend that you upgrade your redis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2810.data"
# $Id: $
