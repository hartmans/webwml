<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been found in unrar-free, an unarchiver for .rar files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14120">CVE-2017-14120</a>

     <p>This CVE is related to a directory traversal vulnerability for
     RAR v2 archives.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14121">CVE-2017-14121</a>

     <p>This CVE  is related to NULL pointer dereference flaw triggered
     by a specially crafted RAR archive.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14122">CVE-2017-14122</a>

     <p>This CVE is related to stack-based buffer over-read.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1:0.0.1+cvs20140707-1+deb9u1.</p>

<p>We recommend that you upgrade your unrar-free packages.</p>

<p>For the detailed security status of unrar-free please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/unrar-free">https://security-tracker.debian.org/tracker/unrar-free</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2567.data"
# $Id: $
