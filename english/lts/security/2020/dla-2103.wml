<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>debian-security-support, the Debian security support coverage checker,
has been updated in jessie-security.</p>

<p>This marks the end of life of the libqb package in jessie.  A recently
reported vulnerability against libqb which allows users to overwrite
arbitrary files via a symlink attack cannot be adequately addressed in
libqb in jessie.  Upstream no longer supports this version and no
packages in jessie depend upon libqb.</p>

<p>We recommend that if your systems or applications depend upon the libqb
package provided from the Debian archive that you upgrade your systems
to a more recent Debian release or find an alternate and up to date
source of libqb packages.</p>

<p>Additionally, MySQL 5.5 is no longer supported.  Upstream has ended its
support and we are unable to backport fixes from newer versions due to
the lack of patch details. Options are to switch to MariaDB 10.0 in
jessie or to a newer version of MySQL in more recent Debian releases.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at:
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>https://wiki.debian.org/LTS</p>

<p>For Debian 8 <q>Lenny</q>, these issues have been fixed in
debian-security-support version 2019.12.12~deb8u2</p>
</define-tag>



# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2103.data"
# $Id: $
