<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>There were several vulnerabilites reported against wordpress,
as follows:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28032">CVE-2020-28032</a>

    <p>WordPress before 4.7.19 mishandles deserialization requests in
    wp-includes/Requests/Utility/FilteredIterator.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28033">CVE-2020-28033</a>

    <p>WordPress before 4.7.19 mishandles embeds from disabled sites
    on a multisite network, as demonstrated by allowing a spam
    embed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28034">CVE-2020-28034</a>

    <p>WordPress before 4.7.19 allows XSS associated with global
    variables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28035">CVE-2020-28035</a>

    <p>WordPress before 4.7.19 allows attackers to gain privileges via
    XML-RPC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28036">CVE-2020-28036</a>

    <p>wp-includes/class-wp-xmlrpc-server.php in WordPress before
    4.7.19 allows attackers to gain privileges by using XML-RPC to
    comment on a post.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28037">CVE-2020-28037</a>

    <p>is_blog_installed in wp-includes/functions.php in WordPress
    before 4.7.19 improperly determines whether WordPress is
    already installed, which might allow an attacker to perform
    a new installation, leading to remote code execution (as well
    as a denial of service for the old installation).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28038">CVE-2020-28038</a>

    <p>WordPress before 4.7.19 allows stored XSS via post slugs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28039">CVE-2020-28039</a>

    <p>is_protected_meta in wp-includes/meta.php in WordPress before
    4.7.19 allows arbitrary file deletion because it does not
    properly determine whether a meta key is considered protected.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28040">CVE-2020-28040</a>

    <p>WordPress before 4.7.19 allows CSRF attacks that change a
    theme's background image.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.7.19+dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>For the detailed security status of wordpress please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wordpress">https://security-tracker.debian.org/tracker/wordpress</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2429.data"
# $Id: $
