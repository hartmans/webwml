<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>libupnp, the portable SDK for UPnP Devices allows remote attackers to cause a denial of service (crash) via a crafted SSDP message due to a NULL pointer dereference in the functions FindServiceControlURLPath and FindServiceEventURLPath in genlib/service_table/service_table.c.  This crash can be triggered by sending a malformed SUBSCRIBE or UNSUBSCRIBE using any of the attached files.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.6.19+git20141001-1+deb8u2.</p>

<p>We recommend that you upgrade your libupnp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2238.data"
# $Id: $
