<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabiliites have been discovered in Unbound, a recursive-only
caching DNS server; a traffic amplification attack against third party
authoritative name servers (NXNSAttack) and insufficient sanitisation
of replies from upstream servers could result in denial of service via
an infinite loop.</p>

<p>The version of Unbound in the oldstable distribution (stretch) is
no longer supported. If these security issues affect your setup, you
should upgrade to the stable distribution (buster).</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1.9.0-2+deb10u2.</p>

<p>We recommend that you upgrade your unbound packages.</p>

<p>For the detailed security status of unbound please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/unbound">\
https://security-tracker.debian.org/tracker/unbound</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4694.data"
# $Id: $
