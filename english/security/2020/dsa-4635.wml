<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Antonio Morales discovered an use-after-free flaw in the memory pool
allocator in ProFTPD, a powerful modular FTP/SFTP/FTPS server.
Interrupting current data transfers can corrupt the ProFTPD memory pool,
leading to denial of service, or potentially the execution of arbitrary
code.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 1.3.5b-4+deb9u4.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.3.6-4+deb10u4.</p>

<p>We recommend that you upgrade your proftpd-dfsg packages.</p>

<p>For the detailed security status of proftpd-dfsg please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/proftpd-dfsg">https://security-tracker.debian.org/tracker/proftpd-dfsg</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4635.data"
# $Id: $
