# translation of stats.po to Ukrainian
# Volodymyr Bodenchuk <Bodenchuk@bigmir.net>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2017-11-14 23:35+0200\n"
"Last-Translator: Volodymyr Bodenchuk <Bodenchuk@bigmir.net>\n"
"Language-Team: Ukrainian <ukrainian>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Статистика перекладу веб-сайту Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Всього %d сторінок для перекладу."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Всього %d байт для перекладу."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Всього %d рядків для перекладу."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Невірна версія перекладу"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Цей переклад надто застарілий"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Оригінал новіший за цей переклад"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "Оригінал більше не існує"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "кількість звернень не відома"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "звернень"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Натисніть, щоб отримати дані diffstat"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr "Створено за допомогою <transstatslink>"

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Підсумок перекладу для"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Не перекладені"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Застарілі"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Перекладені"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Актуальні"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "файлів"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "байт"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Примітка: цей список сторінок відсортований за популярністю. Наведіть мишку "
"на назву сторінки, щоб побачити кількість звернень."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Застарілі переклади"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Файли"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Коментар"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Журнал"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Переклад"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Супроводжувач"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Статус"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Перекладач"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Дата"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Не перекладені сторінки загального характеру"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Не перекладені сторінки загального характеру"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Не перекладені сторінки новин"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Не перекладені сторінки новин"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Не перекладені сторінки про консультантів/користувачів"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Не перекладені сторінки про консультантів/користувачів"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Не перекладені міжнародні сторінки"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Не перекладені міжнародні сторінки"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Перекладені сторінки (актуальні)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Перекладені шаблони (файли PO)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "Статистика перекладу файлів PO"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Не точні"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Не перекладені"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Всього"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Всього:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Перекладені веб-сторінки"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Статистика перекладу за кількістю сторінок"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Мова"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Переклади"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Перекладені веб-сторінки (за розміром)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Статистика перекладу за розміром сторінок"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Кольоровий diff"

#~ msgid "Colored diff"
#~ msgstr "Кольоровий diff"

#~ msgid "Unified diff"
#~ msgstr "Уніфікований diff"
