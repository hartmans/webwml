#use wml::debian::template title="Въведение в Дебиан"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c"

<a id=community></a>
<h2>Дебиан е общност на съмишленици</h2>
<p>Хиляди доброволци от цял свят работят заедно,
  обръщайки специално внимание на свободния софтуер и
  нуждите на потребителите.</p>

<ul>
  <li>
    <a href="people">Хората:</a>
    Кои сме ние и какво правим
  </li>
  <li>
    <a href="philosophy">Идеята:</a>
    Защо и как го правим
  </li>
  <li>
    <a href="../devel/join/">Присъединяване:</a>
    Можете да станете част от нас!
  </li>
  <li>
    <a href="help">Как можете да помогнете?</a>
  </li>
  <li>
    <a href="../social_contract">Обществен договор:</a>
    Нашите цели и мотиви
  </li>
  <li>
    <a href="diversity">Декларация за многообразие</a>
  </li>
  <li>
    <a href="../code_of_conduct">Кодекс за поведение</a>
  </li>
  <li>
    <a href="../partners/">Партньори:</a>
    Организации, които помагат на проекта в дългосрочен план
  </li>
  <li>
    <a href="../donations">Дарения</a>
  </li>
  <li>
    <a href="../legal/">Правна информация</a>
  </li>
  <li>
    <a href="../legal/privacy">Лични данни</a>
  </li>
  <li>
    <a href="../contact">Контакт</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>Дебиан е свободна операционна система</h2>
<p>В основата е Линукс, с добавени хиляди приложения, които да посрещнат
  нуждите на потребителите.</p>

<ul>
  <li>
    <a href="../distrib">Изтегляне:</a>
    Варианти на носители с Дебиан
  </li>
  <li>
  <a href="why_debian">Защо Дебиан</a>
  </li>
  <li>
    <a href="../support">Поддръжка:</a>
    Помощ
  </li>
  <li>
    <a href="../security">Сигурност:</a>
    Последно обновяване <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages">Софтуерни пакети:</a>
    Търсене и разглеждане на списъка с достъпен софтуер
  </li>
  <li>
    <a href="../doc">Документация</a>
  </li>
  <li>
    <a href="https://wiki.debian.org">Уики</a>
  </li>
  <li>
    <a href="../Bugs">Доклади за проблеми</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Пощенски списъци</a>
  </li>
  <li>
    <a href="../blends">Дестилати:</a>
    Групи пакети за специфични нужди
  </li>
  <li>
    <a href="../devel">За сътрудници:</a>
    Информация, предназначена за сътрудниците на Дебиан
  </li>
  <li>
    <a href="../ports">Архитектури:</a>
    Поддържани процесорни архитектури
  </li>
  <li>
    <a href="search">Информация за използването на търсачката на Дебиан</a>.
  </li>
  <li>
    <a href="cn">Информация за разглеждане на сайта на различен език</a>.
  </li>
</ul>
