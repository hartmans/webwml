#use wml::debian::translation-check translation="70be9894f7f65be4520c817dfd389d2ee7c87f04" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i git, et hurtigt, skalerbart og distribueret 
versionsstyringssystem.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1348">CVE-2019-1348</a>

    <p>Der blev rapporteret af valgmuligheden --export-marks tilhørende git 
    fast-import, også blev udstilet gennem in-stream-kommandofunktionaliteten 
    export-marks=..., hvilket gjorde det muligt at overskrive vilkårlige 
    stier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1387">CVE-2019-1387</a>

    <p>Man opdagede at submodule-navne ikke blev valideret tilstrækkeligt 
    strikst, hvilket muliggjorde målrettede angreb gennem fjernudførelse af 
    kode, når der blev dannet rekursive kloner.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19604">CVE-2019-19604</a>

    <p>Joern Schneeweisz rapportered eom en sårbarhed, hvor en rekursiv klone 
    efterfulgt af opdatering af et submodule, kunne udføre kode indeholdt i 
    arkivet, uden at brugeren eksplicit havde bedt om det.  Det er nu ikke 
    længere tilladt at <q>.gitmodules</q> indeholder registreringer som 
    opsætter <q>submodule.&lt;name&gt;.update=!command</q>.</p></li>

</ul>

<p>Desuden løser denne opdatering et antal sikkerhedsproblemer, som kun er et 
problem hvis git afvikles på et NTFS-filsystem 
(<a href="https://security-tracker.debian.org/tracker/CVE-2019-1349">CVE-2019-1349</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1352">CVE-2019-1352</a> og 
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1353">CVE-2019-1353</a>).</p>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 1:2.11.0-3+deb9u5.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 1:2.20.1-2+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine git-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende git, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/git">\
https://security-tracker.debian.org/tracker/git</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4581.data"
