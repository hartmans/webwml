#use wml::debian::template title="Introduktion til Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c"

<a id=community></a>
<h1>Debian er et fællesskab</h1>
<p>Tusinder af frivillige over hele verden samarbejder om prioritering af fri 
software og brugernes behov.</p>

<ul>
  <li>
    <a href="people">Personer:</a>
    Hvem vi er og hvad vi laver
  </li>
  <li>
    <a href="philosophy">Filosofi:</a>
    Hvorfor vi gør det og hvordan vi gør det
  </li>
  <li>
    <a href="../devel/join/">Bliv involveret:</a>
    Du kan også deltage!
  </li>
  <li>
    <a href="help">Hvordan du kan hjælpe Debian</a>
  </li>
  <li>
    <a href="../social_contract">Social kontrakt:</a>
    Vores moralske agenda
  </li>
  <li>
    <a href="diversity">Mangfoldighedserklæring</a>
  </li>
  <li>
    <a href="../code_of_conduct">Etiske regler</a>
  </li>
  <li>
    <a href="../partners/">Partnere:</a>
    Virksomheder og organisationer der løbende hjælper Debian-projektet
  </li>
  <li>
    <a href="../donations">Donationer</a>
  </li>
  <li>
    <a href="../legal/">Juridiske oplysninger</a>
  </li>
  <li>
    <a href="../legal/privacy">Privatlivsdata</a>
  </li>
  <li>
    <a href="../contact">Kontakt os</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h1>Debian er et frit styresystem</h1>
<p>Vi begynder med Linux og tilføjer mange tusinder af applikationer, for at 
opfylde brugernes behov.</p>

<ul>
  <li>
    <a href="../distrib">Download:</a>
    Flere varianter af filaftryk med Debian
  </li>
  <li>
    <a href="why_debian">Hvorfor Debian</a>
  </li>
  <li>
    <a href="../support">Support:</a>
    Få hjælp
  </li>
  <li>
    <a href="../security">Sikkerhed:</a>
    Seneste opdatering<br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages">Softwarepakker:</a>
    Søg i og gennemse vores lange softwareliste
  </li>
  <li>
    <a href="../doc">Dokumentation</a>
  </li>
  <li>
    <a href="https://wiki.debian.org">Debians wiki</a>
  </li>
  <li>
    <a href="../Bugs">Fejlrapporter</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">Postlister</a>
  </li>
  <li>
    <a href="../blends">Pure Blends:</a>
    Metapakker til specifikke behov
  </li>
  <li>
    <a href="../devel">Udviklerhjørnet:</a>
    Oplysninger primært rettet mod Debian-udviklere
  </li>
  <li>
    <a href="../ports">Tilpasninger/arkitekturer:</a>
    CPU-arkitekturer vi understøtter
  </li>
  <li>
    <a href="search">Oplysninger om hvordan Debians søgemaskine benyttes</a>.
  </li>
  <li>
    <a href="cn">Oplysninger om sider der er tilgængelige på flere sprog</a>.
  </li>
</ul>
