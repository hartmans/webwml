#use wml::debian::template title="www.debian.org 是如何被制作出来的"
#use wml::debian::toc
#use wml::debian::translation-check translation="6d9ec7201dcfa229e0cdf7f93c34b3d4a8509ebe"

<p>Debian <em>&ldquo;webtree &rdquo;</em>是构成我们网站的目录和文件的集合，它位于 \
www-master.debian.org 上的 <tt>/org/www.debian.org/www</tt> 目录。页面的大部分内容\
都是普通的静态 HTML 文件（即不是用诸如 CGI 或 PHP 脚本之类的技术动态生成的），因为\
网站需要能被镜像。

<p>网站是通过以下三种方式之一生成的：
<ul>
  <li>大部分内容是从<a href="$(DEVEL)/website/using_git"><q> webwml</q> git 存储库</a>\
      使用 WML 生成的
  <li>从<a href="$(DOC)/vcs"><q> ddp</q> git 存储库</a>使用 DocBook XML（DebianDoc SGML \
      正被逐步淘汰）生成文档，也会使用<a href="#scripts"> cron 脚本</a>来生成（来自相应\
      的 Debian 软件包）
  <li>网站的部分内容是使用其他来源的脚本生成的，例如，邮件列表订阅/取消订阅页面
</ul>

<p>每天运行六次自动更新（从 git 存储库和其他来源的脚本到 webtree）。

<p>如果您想参与网站开发，开始时请<strong>不要</strong>简单地添加或编辑在<code> www/ </code>\
目录中的东西。首先请与<a href="mailto:webmaster@debian.org">webmaster 团队</a>联系。

<p>所有的文件与目录均属于 debwww 组，而且该组具有写入权限，因此 web 团队可以修改 web \
目录中的文件。目录的2775模式意味着在该目录下创建的任何文件都将继承该组（在这里是 debwww）。\
所有在 debwww 组中的人都应设置“<code>umask 002</code>”，以便使用组写入权限创建文件。

<toc-display />

<hrline />


<toc-add-entry name="look">外观</toc-add-entry>

<p>通过让<a href="https://packages.debian.org/unstable/web/wml"> WML </a>进行将页眉\
和页脚添加到页面的所有繁杂工作，我们令页面具有相同的外观。尽管一个 .wml 页面文件乍看\
起来像是一个 HTML 文件，但是 HTML 只是 .wml 中可以使用的其中一种额外信息。在 WML 结束\
在一个文件上运行其各种筛选器后，得到的最终产品就是真正的 HTML 文件。为了让您了解到 \
WML 的强大功能的妙用，这里举一个例子，您可以尝试将 Perl 代码包含在一个页面文件中，这\
将允许您实现几乎所有操作。

<p>但是请注意，WML 仅检查（有时能自动纠正）您的 HTML 代码的最基础的正确性。您应该安装\
<a href="https://packages.debian.org/unstable/web/weblint"> weblint </a>和/或\
<a href="https://packages.debian.org/unstable/web/tidy"> tidy </a>来验证您的 HTML 代码\
是否正确。

<p>我们的网页目前遵守<a href="http://www.w3.org/TR/html4/"> HTML 4.01 Strict </a>标准。

<p>任何在大量页面上进行工作的人都应该安装 WML，以便可以进行测试以确保生成结果符合要求。\
如果您正在使用 Debian，则您可以轻松地安装<code> wml </code>软件包。阅读\
<a href="using_wml"> using WML </a>页面以获取更多信息。


<toc-add-entry name="sources">网页文件的来源</toc-add-entry>

<p>网页的源代码存储在 git 存储库中。git 是一个版本控制系统，它使我们可以记录关于更改了\
什么、是谁更改的、什么时候更改的，以及更改原因等日志。这是一个在多个作者并行编辑源文件时\
令情况能得以控制的安全的方法，由于 Debian 网络团队规模很大，所以这一点对我们来说至关重要。

<p>如果您不熟悉 git，您可能想要阅读有关<a href="using_git">使用 git </a>的页面。

<p>git 存储库中最顶层的 webwml 目录包含以网站所使用的语言命名的目录、两个 makefile 文件\
以及几个脚本。翻译目录名称应使用英文小写字母（例如使用“german”而不是“Deutsch”）。

<p>这两个 makefile 文件中最重要的是 Makefile.common，顾名思义，该文件包含一些通用规则，\
通过在其它 makefile 文件中包含该文件，这些规则得以应用。

<p>每种语言所在的目录都包含 makefile、WML 源文件和子目录。不同语言的目录中包含的文件名与\
目录名没有任何区别，以便令链接适用于所有语言。这些目录还可能包含 .wmlrc 文件，其中包含\
一些对 WML 有用的信息。

<p>webwml/english/template 目录包含被我们称之为模板的特殊的 WML 文件，之所以这样命名是因为\
它们能被所有其它文件通过使用<code> #use </code>指令来引用。

<p>为了使对模板的更改传播到使用它们的文件，所以其它文件在 makefile 上依赖它们。由于绝大多数\
文件都通过在顶部添加“<code>#use wml::debian::template</code>”这一行以使用“template”这一\
模板，所以通用依赖项（对所有文件适用的一个东西）非常像是模板。当然，这条规则也有例外。


<toc-add-entry name="scripts">脚本</toc-add-entry>

<p>用到的脚本主要用 shell 或 Perl 语言编写。其中一些是独立的，还有一些已集成到 WML 源文件中。</p>

<p>重新构建主要的 www-master 的脚本代码位于<a href="https://salsa.debian.org/webmaster-team/cron.git">
webmaster-team/ cron Git 存储库</a>中。
</p>

<p>重新构建 packages.debian.org 网站的脚本代码位于\
<a href="https://salsa.debian.org/webmaster-team/packages"> webmaster-team/packages Git 存储库</a>中。</p>


<toc-add-entry name="help">怎样帮忙</toc-add-entry>

<p>我们邀请所有对此有兴趣的人帮助我们使 Debian 网站变得尽可能的出色。如果您认为我们的页面上\
缺失了一些有关于 Debian 的重要信息，请<a href="mailto:debian-www@lists.debian.org">告诉我们
</a>，我们会查看您指出的问题。

<p>我们总是采纳关于网页设计（有关图像和布局）以及令 HTML 代码保持整洁的帮助与建议。我们定期\
在整个网站上运行以下检查：</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">URL check</a>
  <li><a href="https://www-master.debian.org/build-logs/validate/">wdg-html-validator</a>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a>
</ul>

<p>我们始终欢迎在阅读了以上日志后解决问题的帮助。</p>

<p>网站目前的构建日志可以在 <url "https://www-master.debian.org/build-logs/"> 中找到。</p>

<p>如果您熟悉英语，我们希望您能对我们的页面进行校对并向\
<a href="mailto:debian-www@lists.debian.org">我们</a>报告所有错误。

<p>如果您使用其他语言，您可能想帮助我们将网页翻译成您使用的语言。如果某个页面已经被翻译了\
但存在问题，请查看<a href="translation_coordinators">翻译协调员</a>列表，然后告诉负责翻译您\
使用的语言的人员解决该问题。如果您想自己翻译网页，请参阅<a href="translating">该主题</a>的\
页面以获取更多信息。

<p>这还有一个<a href="todo">待办清单</a>，您可以查看其中的待办事项。</p>


<toc-add-entry name="nohelp">怎样帮<strong>不</strong>上忙</toc-add-entry>

<p><em>[问] 我想给 www.debian.org 添加<var>精美的网页效果</var>，可以吗？</em>

<p>[答] 请不要这样做。我们希望 www.debian.org 尽可能易于访问，因此
<ul>
    <li>不要使用浏览器特定的“扩展名”。
    <li>不要仅依靠图像。图像有时会被用来更好的解释某一项内容，但 www.debian.org 上的信息\
        必须保证能被诸如 lynx 等纯文本网页浏览器访问。
</ul>

<p><em>[问] 我有个好主意，您可以在 www.debian.org 的 HTTPD 启用 FOO 吗？</em>

<p>[答] 请不要这样做。我们希望管理员可以轻松地镜像 www.debian.org，所以请不要使用特殊的 \
HTTPD 功能，为此我们甚至不使用 SSI。内容协商已作为例外进行处理，因为它是提供多种语言服务的\
唯一的一种可靠方法。
