#use wml::debian::template title="تثبيت دبيان من خلال الإنترنت" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="457bcb4118a5a79a3dc0e676d42418e45a8d65f3"

<p>
تتطلب هذه الطريقة توفر إتصال إنترنت <em>خلال</em> عملية التثبيت. 
خلافا للطرائق الأخرى ستنزِّل بيانات أقل وفقا لحتياجاتك. كل من الإتصال 
السلكي واللاسلكي مدعومان. بطاقات ISDN الداخلية <em>ليست</em> مدعومة للأسف.
</p>
<p>هناك ثلاث خيارات للتثبيت من الشّبكة:</p>

<toc-display />
<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">
الأقراص الصغيرة CD أو مفاتيح الـ USB
</toc-add-entry>

<p>
الملفات التالية هي صور أقراص.
اختر البُنية المناسبة لمعالجك أدناه.
</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>لمزيد من التفاصيل، يرجى مراجعة: <a href="../CD/netinst/">التثبيت الشّبَكي من القرص القياسي</a></p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">
الأقراص الصغيرة CD، و مفاتيح الـ USB المرنة، إلخ.
</toc-add-entry>

<p>
يمكنك تنزيل بعض ملفات الصور من الحجم الصغير المخصصة 
لمفاتيح الـ USB والأجهزة المماثلة، يجب حرقها على الوسيط 
والإقلاع من خلاله لبدء التثبيت.
</p>

<p>
هناك بعض الاختلافات بين البنُى فيما يخص دعم التثبيت من الأقراص الصغيرة.
</p>

<p>
لمزيد من التفاصيل، يُرجى الرجوع إلى
<a href="$(HOME)/releases/stable/installmanual">كُتيّب التثبيت الخاص بالبُنية</a>،
وخاصة الفصل
<q>الحصول على وسائط تثبيت النظام</q>.
</p>

<p>
هذه روابط ملفات الصور المتوفرة (لمزيد من المعلومات، يرجى قراءة الملف MANIFEST).
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">الإقلاع من الشّبكة</toc-add-entry>

<p>
أعِدّ خادوم TFTP و DHCP (أو BOOTP، أو RARP) على شبكتك المحلية 
ليعمل كوسيط للتثبيت. إذا كان بيوس "BIOS" الأجهزة العميلة يدعم هذه الميزة، 
يمكنك إذن إقلاع نظام تثبيت دبيان من خلال الشّبكة
(باستخدام PXE و TFTP) ثم المضي قُدما في تثبيت ما تبقى من دبيان.
</p>

<p>
ليست كل الأجهزة تدعم الإقلاع من الشّبكة.
لا يوصى بهذه الطريقة للمستخدمين المبتدئين بسبب الإعدادات الإضافية التي تتطلبها.
</p>

<p>
لمزيد من التفاصيل، يرجى الرجوع إلى
<a href="$(HOME)/releases/stable/installmanual">كُتيّب التثبيت الخاص بالبُنية</a>،
وخاصة الفصل
<q>إعداد الملفات للإقلاع من خلال الشّبكة باستخدام TFTP</q>.
</p>
<p>
هذه روابط لملفات الصور (لمزيد من المعلومات، يرجى قراءة الملف MANIFEST):
</p>

<stable-netboot-images />
</div>
</div>
