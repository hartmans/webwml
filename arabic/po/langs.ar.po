# translation of langs.po to Arabic
# Isam Bayazidi <isam@bayazidi.net>, 2004.
# Mohammed Adnene Trojette <adn+deb@diwi.org>, 2005.
# Ossama M. Khayat <okhayat@yahoo.com>, 2005.
# Med Amine <medeb@protonmail.com>, 2013-20.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-12-25 07:29+0000\n"
"Last-Translator: Med Amine <medeb@protonmail.com>\n"
"Language-Team: Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/template/debian/language_names.wml:8
msgid "Arabic"
msgstr "عربية"

#: ../../english/template/debian/language_names.wml:9
msgid "Armenian"
msgstr "أرمنية"

#: ../../english/template/debian/language_names.wml:10
msgid "Finnish"
msgstr "فنلندية"

#: ../../english/template/debian/language_names.wml:11
msgid "Croatian"
msgstr "كرواتية"

#: ../../english/template/debian/language_names.wml:12
msgid "Danish"
msgstr "دنماركية"

#: ../../english/template/debian/language_names.wml:13
msgid "Dutch"
msgstr "هولندية"

#: ../../english/template/debian/language_names.wml:14
msgid "English"
msgstr "إنجليزية"

#: ../../english/template/debian/language_names.wml:15
msgid "French"
msgstr "فرنسية"

#: ../../english/template/debian/language_names.wml:16
msgid "Galician"
msgstr "جليقية"

#: ../../english/template/debian/language_names.wml:17
msgid "German"
msgstr "ألمانية"

#: ../../english/template/debian/language_names.wml:18
msgid "Italian"
msgstr "إيطالية"

#: ../../english/template/debian/language_names.wml:19
msgid "Japanese"
msgstr "يابانية"

#: ../../english/template/debian/language_names.wml:20
msgid "Korean"
msgstr "كورية"

#: ../../english/template/debian/language_names.wml:21
msgid "Spanish"
msgstr "إسبانية"

#: ../../english/template/debian/language_names.wml:22
msgid "Portuguese"
msgstr "برتغالية"

#: ../../english/template/debian/language_names.wml:23
msgid "Portuguese (Brazilian)"
msgstr "برتغالية (برازيلة)"

#: ../../english/template/debian/language_names.wml:24
msgid "Chinese"
msgstr "صينية"

#: ../../english/template/debian/language_names.wml:25
msgid "Chinese (China)"
msgstr "صينية (الصين)"

#: ../../english/template/debian/language_names.wml:26
msgid "Chinese (Hong Kong)"
msgstr "صينية (هونغ كونغ)"

#: ../../english/template/debian/language_names.wml:27
msgid "Chinese (Taiwan)"
msgstr "صينية (تايوان)"

#: ../../english/template/debian/language_names.wml:28
msgid "Chinese (Traditional)"
msgstr "صينية (تقليدية)"

#: ../../english/template/debian/language_names.wml:29
msgid "Chinese (Simplified)"
msgstr "صينية (مبسطة)"

#: ../../english/template/debian/language_names.wml:30
msgid "Swedish"
msgstr "سويدية"

#: ../../english/template/debian/language_names.wml:31
msgid "Polish"
msgstr "بولونية"

#: ../../english/template/debian/language_names.wml:32
msgid "Norwegian"
msgstr "نرويجية"

#: ../../english/template/debian/language_names.wml:33
msgid "Turkish"
msgstr "تركية"

#: ../../english/template/debian/language_names.wml:34
msgid "Russian"
msgstr "روسية"

#: ../../english/template/debian/language_names.wml:35
msgid "Czech"
msgstr "تشيكية"

#: ../../english/template/debian/language_names.wml:36
msgid "Esperanto"
msgstr "إسبرانتو"

#: ../../english/template/debian/language_names.wml:37
msgid "Hungarian"
msgstr "هنغارية"

#: ../../english/template/debian/language_names.wml:38
msgid "Romanian"
msgstr "رومانية"

#: ../../english/template/debian/language_names.wml:39
msgid "Slovak"
msgstr "سلوفاكية"

#: ../../english/template/debian/language_names.wml:40
msgid "Greek"
msgstr "يونانية"

#: ../../english/template/debian/language_names.wml:41
msgid "Catalan"
msgstr "كتالانية"

#: ../../english/template/debian/language_names.wml:42
msgid "Indonesian"
msgstr "إندونيسية"

#: ../../english/template/debian/language_names.wml:43
msgid "Lithuanian"
msgstr "ليتوانية"

#: ../../english/template/debian/language_names.wml:44
msgid "Slovene"
msgstr "سلوفانية"

#: ../../english/template/debian/language_names.wml:45
msgid "Bulgarian"
msgstr "بلغارية"

#: ../../english/template/debian/language_names.wml:46
msgid "Tamil"
msgstr "تاميلية"

#. for now, the following are only needed if you intend to translate intl/l10n
#: ../../english/template/debian/language_names.wml:48
msgid "Afrikaans"
msgstr "أفريكانية"

#: ../../english/template/debian/language_names.wml:49
msgid "Albanian"
msgstr "ألبانية"

#: ../../english/template/debian/language_names.wml:50
msgid "Asturian"
msgstr "أستورية"

#: ../../english/template/debian/language_names.wml:51
msgid "Amharic"
msgstr "أمهرية"

#: ../../english/template/debian/language_names.wml:52
msgid "Azerbaijani"
msgstr "أذربيجانية"

#: ../../english/template/debian/language_names.wml:53
msgid "Basque"
msgstr "باسكية"

#: ../../english/template/debian/language_names.wml:54
msgid "Belarusian"
msgstr "بلاروسية"

#: ../../english/template/debian/language_names.wml:55
msgid "Bengali"
msgstr "بنغالية"

#: ../../english/template/debian/language_names.wml:56
msgid "Bosnian"
msgstr "بوسنية"

#: ../../english/template/debian/language_names.wml:57
msgid "Breton"
msgstr "بريتانية"

#: ../../english/template/debian/language_names.wml:58
msgid "Cornish"
msgstr "كورنية"

#: ../../english/template/debian/language_names.wml:59
msgid "Estonian"
msgstr "إستونية"

#: ../../english/template/debian/language_names.wml:60
msgid "Faeroese"
msgstr "فاروية"

#: ../../english/template/debian/language_names.wml:61
msgid "Gaelic (Scots)"
msgstr "غيلية (أسكتلندية)"

#: ../../english/template/debian/language_names.wml:62
msgid "Georgian"
msgstr "جورجية"

#: ../../english/template/debian/language_names.wml:63
msgid "Hebrew"
msgstr "عبرية"

#: ../../english/template/debian/language_names.wml:64
msgid "Hindi"
msgstr "هندية"

#: ../../english/template/debian/language_names.wml:65
msgid "Icelandic"
msgstr "آيسلندية"

#: ../../english/template/debian/language_names.wml:66
msgid "Interlingua"
msgstr "إنترلنغوا"

#: ../../english/template/debian/language_names.wml:67
msgid "Irish"
msgstr "أيرلندية"

#: ../../english/template/debian/language_names.wml:68
msgid "Kalaallisut"
msgstr "جرينلاندية"

#: ../../english/template/debian/language_names.wml:69
msgid "Kannada"
msgstr "كنادية"

#: ../../english/template/debian/language_names.wml:70
msgid "Kurdish"
msgstr "كردية"

#: ../../english/template/debian/language_names.wml:71
msgid "Latvian"
msgstr "لاتفية"

#: ../../english/template/debian/language_names.wml:72
msgid "Macedonian"
msgstr "مقدونية"

#: ../../english/template/debian/language_names.wml:73
msgid "Malay"
msgstr "ملايوية"

#: ../../english/template/debian/language_names.wml:74
msgid "Malayalam"
msgstr "ماليالامية"

#: ../../english/template/debian/language_names.wml:75
msgid "Maltese"
msgstr "مالطية"

#: ../../english/template/debian/language_names.wml:76
msgid "Manx"
msgstr "منكية"

#: ../../english/template/debian/language_names.wml:77
msgid "Maori"
msgstr "ماورية"

#: ../../english/template/debian/language_names.wml:78
msgid "Mongolian"
msgstr "منغولية"

#: ../../english/template/debian/language_names.wml:80
msgid "Norwegian Bokm&aring;l"
msgstr "بوكمول"

#: ../../english/template/debian/language_names.wml:82
msgid "Norwegian Nynorsk"
msgstr "نينوشك"

#: ../../english/template/debian/language_names.wml:83
msgid "Occitan (post 1500)"
msgstr "قسطانية"

#: ../../english/template/debian/language_names.wml:84
msgid "Persian"
msgstr "فارسية"

#: ../../english/template/debian/language_names.wml:85
msgid "Serbian"
msgstr "صربية"

#: ../../english/template/debian/language_names.wml:86
msgid "Slovenian"
msgstr "سلوفينية"

#: ../../english/template/debian/language_names.wml:87
msgid "Tajik"
msgstr "طاجيكية"

#: ../../english/template/debian/language_names.wml:88
msgid "Thai"
msgstr "تايلندية"

#: ../../english/template/debian/language_names.wml:89
msgid "Tonga"
msgstr "تونغية"

#: ../../english/template/debian/language_names.wml:90
msgid "Twi"
msgstr "أكانية"

#: ../../english/template/debian/language_names.wml:91
msgid "Ukrainian"
msgstr "أوكرانية"

#: ../../english/template/debian/language_names.wml:92
msgid "Vietnamese"
msgstr "فتنامية"

#: ../../english/template/debian/language_names.wml:93
msgid "Welsh"
msgstr "ويلزية"

#: ../../english/template/debian/language_names.wml:94
msgid "Xhosa"
msgstr "زوشاوية"

#: ../../english/template/debian/language_names.wml:95
msgid "Yiddish"
msgstr "يديشية"

#: ../../english/template/debian/language_names.wml:96
msgid "Zulu"
msgstr "زولوية"
