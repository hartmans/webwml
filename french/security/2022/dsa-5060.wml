#use wml::debian::translation-check translation="341e6852d23c334b780a3b6f9546f14ecc590346" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
webkit2gtk :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30934">CVE-2021-30934</a>

<p>Dani Biro a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30936">CVE-2021-30936</a>

<p>Chijin Zhou a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30951">CVE-2021-30951</a>

<p>Pangu a découvert que le traitement d'un contenu web contrefait pourrait
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30952">CVE-2021-30952</a>

<p>WeBin a découvert que le traitement d'un contenu web contrefait pourrait
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30953">CVE-2021-30953</a>

<p>VRIJ a découvert que le traitement d'un contenu web contrefait pourrait
conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30954">CVE-2021-30954</a>

<p>Kunlun Lab a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30984">CVE-2021-30984</a>

<p>Kunlun Lab a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 2.34.4-1~deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.34.4-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5060.data"
# $Id: $
