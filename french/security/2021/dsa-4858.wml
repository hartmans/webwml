#use wml::debian::translation-check translation="0f4f3e8210b7403850a5550da354ea08036f8c2f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le navigateur web Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21148">CVE-2021-21148</a>

<p>Mattias Buelens a découvert un problème de dépassement de tampon dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21149">CVE-2021-21149</a>

<p>Ryoya Tsukasaki a découvert un problème de dépassement de pile dans
l'implémentation du transfert de données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21150">CVE-2021-21150</a>

<p>Woojin Oh a découvert un problème d'utilisation de mémoire après libération
dans le téléchargement de fichiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21151">CVE-2021-21151</a>

<p>Khalil Zhani a découvert un problème d'utilisation de mémoire après
libération dans le système de paiement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21152">CVE-2021-21152</a>

<p>Un dépassement de tampon a été découvert dans le traitement des médias.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21153">CVE-2021-21153</a>

<p>Jan Ruge a découvert un problème de dépassement de pile dans le traitement du
processeur graphique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21154">CVE-2021-21154</a>

<p>Abdulrahman Alqabandi a découvert un problème de dépassement de tampon dans
l'implémentation de Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21155">CVE-2021-21155</a>

<p>Khalil Zhani a découvert un problème de dépassement de tampon dans 
l'implémentation de Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21156">CVE-2021-21156</a>

<p>Sergei Glazunov a découvert un problème de dépassement de tampon dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21157">CVE-2021-21157</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert dans
l'implémentation de WebSocket.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés dans la
version 88.0.4324.182-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chromium.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de chromium, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4858.data"
# $Id: $
