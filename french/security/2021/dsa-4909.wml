#use wml::debian::translation-check translation="022e55922f28ad2d0ab4d75ed4d28a027bafddfa" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans BIND, une
implémentation de serveur DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25214">CVE-2021-25214</a>

<p>Greg Kuechle a découvert qu'un transfert entrant IXFR mal formé pourrait
déclencher un échec d'assertion dans named, avec pour conséquence un déni
de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25215">CVE-2021-25215</a>

<p>Siva Kakarla a découvert que named pourrait planter quand un
enregistrement de DNAME placé dans la section ANSWER durant la poursuite de
DNAME se révélait être la réponse finale à une requête d'un client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25216">CVE-2021-25216</a>

<p>L'implémentation de SPNEGO utilisée par BIND est prédisposée à une
vulnérabilité de dépassement de tampon. Cette mise à jour bascule vers
l'implémentation de SPNEGO provenant des bibliothèques Kerberos.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1:9.11.5.P4+dfsg-5.1+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4909.data"
# $Id: $
