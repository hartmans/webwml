#use wml::debian::translation-check translation="158226aad5fbf7d8645b13853a75c3562078bbc7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation de privilèges, à un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36311">CVE-2020-36311</a>

<p>Un défaut a été découvert dans le sous-système KVM pour les processeurs
AMD, permettant à un attaquant de provoquer un déni de service en
déclenchant la destruction d'une grande machine virtuelle SEV.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3609">CVE-2021-3609</a>

<p>Norbert Slusarek a signalé une vulnérabilité de situation de compétition
dans le protocole réseau BCM du bus CAN, permettant à un attaquant local
d'élever ses privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33909">CVE-2021-33909</a>

<p>Qualys Research Labs a découvert une vulnérabilité de conversion
<q>size_t-to-int</q> dans la couche système de fichiers du noyau Linux. Un
attaquant local non privilégié capable de créer, monter puis supprimer une
structure profonde de répertoires dont la longueur totale du chemin dépasse
1 Go, peut tirer avantage de ce défaut pour une élévation de privilèges.</p>

<p>Vous trouverez plus de détails dans l'annonce de Qualys à l'adresse
<a href="https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt">\
https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34693">CVE-2021-34693</a>

<p>Norbert Slusarek a découvert une fuite d'informations dans le protocole
réseau BCM du bus CAN. Un attaquant local peut tirer avantage de ce défaut
pour obtenir des informations sensibles à partir de la mémoire de pile du
noyau.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés dans
la version 4.19.194-3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4941.data"
# $Id: $
