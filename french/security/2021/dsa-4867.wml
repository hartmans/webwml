#use wml::debian::translation-check translation="938f7bfa52fa35f75b35de70bbd10bfc81e42e8d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le chargeur d'amorçage
GRUB2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14372">CVE-2020-14372</a>

<p>La commande acpi permet à un utilisateur privilégié de charger des
tables ACPI contrefaites lorsque l'amorçage sécurisé (« Secure Boot ») est
activé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25632">CVE-2020-25632</a>

<p>Une vulnérabilité d'utilisation de mémoire après libération a été
découverte dans la commande rmmod.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25647">CVE-2020-25647</a>

<p>Une vulnérabilité d'écriture hors limites a été découverte dans la
fonction grub_usb_device_initialize() qui est appelée pour gérer
l'initialisation de périphériques USB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27749">CVE-2020-27749</a>

<p>Un défaut de dépassement de pile a été découvert dans
grub_parser_split_cmdline.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27779">CVE-2020-27779</a>

<p>La commande cutmem permet à un utilisateur privilégié de supprimer des
régions de mémoires lorsque l'amorçage sécurisé est activé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20225">CVE-2021-20225</a>

<p>Une vulnérabilité d'écriture de tas hors limites a été découverte dans
l'analyseur d'options de forme courte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20233">CVE-2021-20233</a>

<p>Un défaut d'écriture de tas hors limites, provoqué par un calcul inexact
de l'espace requis pour les guillemets dans le rendu du menu, a été
découvert.</p></li>

</ul>

<p>Vous trouverez davantage d'informations détaillées à l'adresse
<a href="https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot">\
https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot</a>.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.02+dfsg1-20+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets grub2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de grub2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/grub2">\
https://security-tracker.debian.org/tracker/grub2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4867.data"
# $Id: $
