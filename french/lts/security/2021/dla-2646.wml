#use wml::debian::translation-check translation="ff0790ce02fa409f0bd3c9ae3d8deb8dbe9d7133" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité a été découvert dans subversion.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17525">CVE-2020-17525</a>:

<p>Le module mod_authz_svn de Subversion pourrait planter si le serveur utilise
des règles authz du dépôt avec l’option AuthzSVNReposRelativeAccessFile et qu’un
client envoie une requête pour un localisateur de dépôt non existant. Cela peut
conduire à une perturbation de service pour les utilisateurs.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.9.5-1+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets subversion.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de subversion, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/subversion">\
https://security-tracker.debian.org/tracker/subversion</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2646.data"
# $Id: $
