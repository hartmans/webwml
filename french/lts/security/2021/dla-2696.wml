#use wml::debian::translation-check translation="47b03412d46e7e48fdb1198dbf454fde5bbf6b8b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème d’entité externe XML (XXE)
dans libjdom2-java, une bibliothèque pour lire et manipuler des documents XML.
Des attaquants pourraient déclencher une attaque par déni de service
à l'aide d'une requête HTTP contrefaite pour l'occasion.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33813">CVE-2021-33813</a>

<p>Un problème de XXE dans SAXBuilder dans JDOM jusqu’à la version 2.0.6 permet
à des attaquants de provoquer un déni de service à l'aide d'une requête HTTP
contrefaite.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.0.6-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libjdom2-java.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2696.data"
# $Id: $
