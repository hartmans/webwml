#use wml::debian::translation-check translation="d669aca24d6d8285c31192a9984f839cac40e888" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans GlusterFS,
un système de fichiers pour grappe. Des dépassements de tampon et des
problèmes de traversée de répertoires pourraient conduire à la divulgation
d'informations, à un déni de service ou à l'exécution de code arbitraire.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 3.8.8-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets glusterfs.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de glusterfs, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/glusterfs">\
https://security-tracker.debian.org/tracker/glusterfs</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2806.data"
# $Id: $
