#use wml::debian::translation-check translation="f94365ece02cccd1d0ae27ba5ceb846147aaf09c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22885">CVE-2021-22885</a>

<p>Une vulnérabilité de divulgation d'informations ou d’exécution de méthode
inattendue est possible dans Action Pack lors de l’utilisation des assistants
<q>redirect_to</q> ou <q>polymorphic_url</q> avec une entrée d’utilisateur
non fiable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22904">CVE-2021-22904</a>

<p>Une vulnérabilité de déni de service est possible dans la logique
d’authentification de jeton dans Action Controller. Le code impacté utilise
<q>authenticate_or_request_with_http_token</q> ou
<q>authenticate_with_http_token</q> pour l’authentification de requête.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2:4.2.7.1-1+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rails.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rails, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/rails">\
https://security-tracker.debian.org/tracker/rails</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2655.data"
# $Id: $
