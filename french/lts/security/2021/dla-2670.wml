#use wml::debian::translation-check translation="fc2535a53c1d4f228a8af287345c789366a3c972" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Luis Merino, Markus Vervier et Eric Sesterhenn ont découvert une
vulnérabilité due à un décalage d'entier dans Nginx, un serveur web et
mandataire inverse à haute performance, qui pourrait avoir pour conséquences
un déni de service et éventuellement l'exécution de code arbitraire.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.10.3-1+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nginx.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nginx, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nginx">\
https://security-tracker.debian.org/tracker/nginx</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2670.data"
# $Id: $
