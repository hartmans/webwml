#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été découvertes dans la bibliothèque libtiff et les
outils inclus. Cela pourrait permettre une exécution de code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9936">CVE-2017-9936</a>

<p>Un document TIFF contrefait peut conduire à une fuite de mémoire aboutissant
à une attaque distante par déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10688">CVE-2017-10688</a>

<p>Une entrée contrefaite conduit à une attaque distante par déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.0.2-6+deb7u15.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1022.data"
# $Id: $
