#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Libtiff est vulnérable à plusieurs dépassements de tampon et dépassements
d'entier pouvant conduire à un plantage d'application (déni de service) ou pire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10266">CVE-2016-10266</a>

<p>Dépassement d'entier pouvant conduire à une division par zéro dans
TIFFReadEncodedStrip (tif_read.c).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10267">CVE-2016-10267</a>

<p>Erreur de division par zéro dans OJPEGDecodeRaw (tif_ojpeg.c).

<a href="https://security-tracker.debian.org/tracker/CVE-2016-10268">CVE-2016-10268</a></p>

<p>Dépassement de tampon de tas dans TIFFReverseBits (tif_swab.c).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10269">CVE-2016-10269</a>

<p>Dépassement de tampon de tas dans _TIFFmemcpy (tif_unix.c).</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.0.2-6+deb7u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-877.data"
# $Id: $
