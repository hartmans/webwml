#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans potrace.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-7437">CVE-2013-7437</a>

<p>Plusieurs dépassements d'entiers dans potrace 1.11 permettent à des
attaquants distants de provoquer un déni de service (plantage) au moyen de
trop grandes dimensions dans une image BMP qui déclenche un dépassement de
tampon. Ce bogue a été signalé par Murray McAllister de l'équipe de
sécurité de Red Hat.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8694">CVE-2016-8694</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8695">CVE-2016-8695</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8696">CVE-2016-8696</a>

<p>Plusieurs déréférencements de pointeur NULL dans bm_readbody_bmp. Ce
bogue a été découvert par Agostino Sarubbo de Gentoo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8697">CVE-2016-8697</a>

<p>Division par zéro dans bm_new. Ce bogue a été découvert par Agostino
Sarubbo de Gentoo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8698">CVE-2016-8698</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8699">CVE-2016-8699</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8700">CVE-2016-8700</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8701">CVE-2016-8701</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8702">CVE-2016-8702</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8703">CVE-2016-8703</a>

<p>Plusieurs dépassements de tas dans bm_readbody_bmp. Ce bogue a été
découvert par Agostino Sarubbo de Gentoo.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.10-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets potrace.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-675.data"
# $Id: $
