#use wml::debian::translation-check translation="d5e4a02feb384a1953c4af1ba19e832c8a99a614" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour fournit un microcode du processeur mis à jour pour la
plupart des processeurs Intel. Il fournit des moyens pour réduire les
vulnérabilités matérielles MSBDS, MFBDS, MLPDS et MDSUM.</p>

<p>Pour corriger complètement ces vulnérabilités, il est aussi nécessaire de
mettre à jour les paquets du noyau Linux publiés dans DLA-1787-1.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.20190514.1~deb8u1 du paquet intel-microcode, et aussi par les
mises à jour du noyau Linux décrites dans DLA-1787-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode et les
paquets du noyau Linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de intel-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1789.data"
# $Id: $
