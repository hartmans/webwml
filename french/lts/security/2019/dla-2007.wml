#use wml::debian::translation-check translation="c5e396f6a44aeee6df42317464c4585179832f55" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs défauts ont été découverts dans ruby2.1, un interpréteur de
langage de script orienté objet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15845">CVE-2019-15845</a>

<p>La correspondance de chemin pouvait réussir dans File.fnmatch et File.fnmatch?
à cause d’une injection de caractère NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16201">CVE-2019-16201</a>

<p>Une boucle causée par une mauvaise expression rationnelle pourrait conduire
à un déni de service du service WEBrick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16254">CVE-2019-16254</a>

<p>Cela est le même problème que
<a href="https://security-tracker.debian.org/tracker/CVE-2017-17742">CVE-2017-17742</a>
dont le correctif n’était pas complet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16255">CVE-2019-16255</a>

<p>La fourniture de données non fiables au premier argument de Shell#[] et
Shell#test pouvait conduire à une vulnérabilité d’injection de code.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.1.5-2+deb8u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby2.1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2007.data"
# $Id: $
