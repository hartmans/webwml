#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans wordpress, un outil de blog.
Le projet « Common Vulnerabilities and Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5836">CVE-2016-5836</a>

<p>L’implémentation du protocole oEmbed dans WordPress avant 4.5.3 permet à des
attaquants distants de provoquer un déni de service à l’aide de vecteurs non
précisés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12895">CVE-2018-12895</a>

<p>Une vulnérabilité a été découverte dans Wordpress, un outil de blog. Elle
permettait à  des attaquants distants avec des rôles spécifiques d’exécuter du
code arbitraire.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.1+dfsg-1+deb8u18.</p>
<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1452.data"
# $Id: $
