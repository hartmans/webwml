#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans openjpeg2, le codec au code
source ouvert JPEG 2000.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17480">CVE-2017-17480</a>

<p>Dépassement de tampon en écriture dû à un formatage manquant de longueur de
tampon dans l’appel fscanf (codecs jp3d et jpwl). Cette vulnérabilité peut être
exploitée par des attaquants distants utilisant des fichiers jp3d et jpwl
contrefaits pour provoquer un déni de service ou éventuellement une exécution de
code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18088">CVE-2018-18088</a>

<p>Déréférencement de pointeur NULL causé par des composants d’image non valables
dans imagetopnm. Cette vulnérabilité peut être exploitée par des attaquants
distants utilisant des fichiers BMP contrefaits pour provoquer un déni de
service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 2.1.0-2+deb8u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openjpeg2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1579.data"
# $Id: $
