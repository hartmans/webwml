#use wml::debian::template title="Comment utiliser le moteur de recherche de Debian ?"

#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="Jean-Paul Guillonneau"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<P>Le projet Debian propose son propre moteur de recherche à l'adresse <a
href="https://search.debian.org/">https://search.debian.org/</a>. Voici
quelques astuces sur son utilisation et faire quelques recherches simples
ou plus complexes en utilisant des opérateurs booléens.

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">

<h3>Recherche simple</h3>

<p>La façon la plus simple est de taper un seul mot dans le champ de
saisie et de taper sur la touche entrée (ou de cliquer sur le bouton
<em>Recherche</em>). Le moteur de recherche vous indiquera alors toutes
les pages du site qui contiennent ce mot. Cela donne assez souvent de
bons résultats.

<p>Sinon, vous pouvez rechercher plus d’un mot. Toutes les pages du site web
de Debian contenant les mots que vous avez saisis apparaîtront. Pour rechercher
des phrases, mettez-les entre guillemets ("). Remarquez que le moteur de
recherche n’est pas sensible à la casse, aussi la recherche de <code>gcc</code>
trouve « gcc » ainsi que « GCC ».</p>

<p>En dessous du champ de recherche il est possible de décider combien de
résultats par page sont souhaités. Il est aussi possible de choisir une autre
langue. La recherche dans le site web de Debian gère environ 40 langues
différentes.</p>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">

<h3>Recherche booléenne</h3>

<p>Si une recherche simple n'est pas suffisante, alors l'utilisation d'un
booléen peut vous satisfaire. Vous avez le choix entre <em>AND</em>, <em>OR</em>,
<em>NOT</em> et toute combinaison de ces trois opérateurs. Attention, ils
doivent être complètement écrits en capitales pour être reconnus.</p>

 <ul>
   <li><b>AND</b> combine deux expressions et donnera les résultats
pour lesquels deux mots sont dans la page. Par exemple, <code> gcc AND
patch</code> » trouvera toutes les pages qui contiennent à la fois « gcc » et
« patch ». Cet exemple donne les mêmes résultats que <code>gcc patch</code>,
mais le <code>AND</code> explicite peut être utile en combinaison avec
d'autres opérateurs.</li>

   <li><b>OR</b> donnera les résultats pour lesquels un des mots est dans
la page. Par exemple <code>gcc OR patch</code> trouvera toutes les pages qui
contiennent « gcc » ou « patch ».</li>

   <li><b>NOT</b> exclut un mot des résultats. Par exemple <code>gcc NOT
patch</code> trouvera toutes les pages qui contiennent
« gcc » mais qui ne contiennent pas aussi « patch ». Vous pouvez aussi
écrire <code>gcc AND NOT patch</code> pour le même résultat, mais une
recherche avec uniquement <code>NOT patch</code> n'est pas admise.</li>

    <li><b>(...)</b> peut être utilisé pour regrouper des sous-expressions.
Par exemple <code>(gcc OR make) NOT patch</code> trouvera toutes les pages
qui contiennent « gcc » ou « make » mais qui ne contiennent pas « patch ».
      </ul>
    </div>
  </div>
</div>
