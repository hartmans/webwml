#use wml::debian::template title="Assistance"
#use wml::debian::translation-check translation="9c90f0ed4b82c5e7504045fff4274d38f9b9997b" maintainer="Jean-Paul Guillonneau"
#use wml::debian::toc

# Translators:
# Christophe Le Bars, 1998
# Pierre Machard, 2002
# Denis Barbier, 2002
# Norbert Bottlaender-Prier, 2003-2005
# Thomas Huriaux, 2005
# Mohammed Adnène Trojette, 2005
# Frédéric Bothamy, 2006
# Thomas Peteul, 2009
# David Prévot, 2011
# Jean-Paul Guillonneau, 2016-2021

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

Debian est dirigée et prise en charge par une communauté de bénévoles.

Si l’assistance fournie par cette communauté ne satisfait pas vos besoins, vous
pouvez lire notre <a href="doc/">documentation</a> ou embaucher un
<a href="consultants/">consultant</a>.

<toc-display />

<toc-add-entry name="irc">Aide en ligne en temps réel à l’aide d’IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a> est
une manière de discuter en temps réel avec des personnes du monde
entier.
Des canaux IRC dédiés à Debian sont disponibles sur
<a href="https://www.oftc.net/">OFTC</a>.</p>
<p>Pour vous y connecter, vous avez besoin d'un client IRC.
Parmi les clients les plus populaires, citons&nbsp;:
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> et
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>.
Ils sont tous empaquetés pour Debian. OFTC propose aussi une interface
web <a href="https://www.oftc.net/WebChat/">WebChat</a> permettant une
connexion IRC à l’aide d’un navigateur sans nécessité d’installer localement
un client.</p>

<p>Avec la plupart des clients, vous devez indiquer notre serveur en tapant&nbsp;:</p>

<pre>
/server irc.debian.org
</pre>

<p>Avec quelques clients (tels que irssi) vous devrez plutôt saisir ceci :</p>

<pre>
/connect irc.debian.org
</pre>

<p>Une fois connecté, rejoignez le canal <code>#debian</code> (anglophone)
en tapant&nbsp;:</p>

<pre>
/join #debian
</pre>

<p>ou le canal <code>#debian-fr</code> (francophone) en tapant&nbsp;:</p>

<pre>
/join #debian-fr
</pre>

<p>Note&nbsp;: les clients comme HexChat possèdent généralement une interface
graphique pour rejoindre les serveurs et les salons de discussion.</p>

<p>Dès cet instant, vous vous trouverez au milieu de la foule amicale des
occupants de <code>#debian</code> ou de <code>#debian-fr</code>. Vous êtes
invités à poser vos questions au sujet de Debian. La foire aux questions
du canal <code>#debian</code> est consultable à l'adresse
<url "https://wiki.debian.org/DebianIRC">.</p>

<p>Il existe aussi de nombreux autres réseaux IRC sur lesquels vous pouvez
discuter de Debian.</p>


<toc-add-entry name="mail_lists" href="MailingLists/">Listes de diffusion</toc-add-entry>

<p>Le développement de Debian est l'&oelig;uvre de personnes disséminées
dans le monde entier. C'est pourquoi le courrier électronique est le
moyen préféré pour discuter de différents sujets. Une bonne partie des
conversations entre les développeurs et les utilisateurs de Debian se
déroule à travers les différentes listes de diffusion.</p>
<p>Plusieurs listes de diffusion sont ouvertes au public. Pour plus
d'informations, voir les <a href="MailingLists/">listes de diffusion
Debian</a>.</p>

<p>
Pour l'assistance aux utilisateurs en français, veuillez contacter
la <a href="https://lists.debian.org/debian-user-french/">liste de
diffusion debian-user-french</a>.
</p>

<p>
Pour l'assistance aux utilisateurs dans d'autres langues, veuillez consultez
l'<a href="https://lists.debian.org/users.html">index des listes de
diffusion pour les utilisateurs</a>.
</p>

<p>Il existe aussi beaucoup d'autres listes de diffusion dédiées à quelque
aspect de l'écosystème Linux, non axées spécifiquement sur Debian.
Veuillez utiliser votre moteur de recherche favori pour trouver la liste qui
conviendra le mieux à votre requête.</p>


<toc-add-entry name="usenet">Groupes de discussion Usenet</toc-add-entry>

<p>Une grande majorité de nos <a href="#mail_lists">listes de diffusion</a>
peuvent aussi être lues sous la forme de groupes de discussion, dans la hiérarchie
<kbd>linux.debian.*</kbd>. Ces groupes peuvent également être lus à l’aide
d’interfaces web telles que celle de <a href="https://groups.google.com/forum/">Google
Groups</a>.</p>


<toc-add-entry name="web">Sites web</toc-add-entry>


<h3>En langue anglaise</h3>

<h3 id="forums">Forums</h3>

<p><a href="http://forums.debian.net">Debian User Forums</a> est un portail sur
le web où vous pouvez aborder des sujets relatifs à Debian, poser vos questions
à propos de Debian et obtenir les réponses d'autres utilisateurs.</p>


# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
<h3>En langue française</h3>

<p>
<a href="https://www.debian-fr.org/">debian-fr.org</a> et <a
href="https://debian-fr.xyz">debian-fr.xyz</a> sont les
équivalents francophones de debianHELP et Debian User Forums.
Ce sont des portails où vous pouvez poser vos questions à propos
de Debian et obtenir les réponses des autres utilisateurs.
<a href="https://debian-facile.org">debian-facile.org</a>
est un site comportant de la documentation en français sous forme de wiki
et un forum.
</p>

<p>Liens utiles au sujet de Linux :</p>

<ul>
  <li><a href="http://www.lea-linux.org/">Linux Entre Amis</a></li>
 # <li><a href="http://www.linux-france.org/">Linux-France</a></li>
 # <li><a href="http://www.linux-france.org/article/fcol-faq/">\
#FAQ fr.comp.os.linux.*</a></li>
#  <li><a href="http://www.andesi.org/">ANDESI</a></li>
  <li><a href="https://linuxfr.org/">linuxfr.org</a></li>
</ul>


<toc-add-entry name="maintainers">Contacter les responsables des paquets Debian</toc-add-entry>

<p>Vous pouvez contacter les responsables de paquet de deux manières.
Si vous avez besoin de contacter le responsable à cause d'un bogue,
envoyez simplement un rapport de bogue (voyez ci-dessous le système de
suivi des bogues). Le responsable recevra une copie de votre rapport.</p>
<p>Si vous désirez simplement communiquer avec le responsable, vous
pouvez utiliser les alias spéciaux mis en place pour chaque paquet. Tout
courrier expédié à &lt;nom du paquet&gt;@packages.debian.org
sera retransmis au responsable
de ce paquet.</p>


<toc-add-entry name="bts" href="Bugs/">Le système de suivi des bogues</toc-add-entry>

<p>La distribution Debian possède un système de suivi des
bogues qui détaille les bogues rapportés par les utilisateurs et les
développeurs. Chaque bogue reçoit un numéro, et il reste fiché jusqu'à ce
qu'il soit marqué comme résolu.</p>
<p>Si vous voulez rapporter un bogue, vous pouvez lire les pages référencées
ci-après à propos des bogues. Nous recommandons d’utiliser le paquet <q>reportbug</q>
de Debian qui permet de le faire automatiquement.</p>
<p>Vous trouverez des informations sur le rapport de bogues, sur la
consultation de la liste des bogues actuellement observés et sur le
système de suivi des bogues en général, sur <a href="Bugs/">les pages
web du système de suivi des bogues</a>.</p>

<toc-add-entry name="doc" href="doc/">La documentation</toc-add-entry>

<p>Dans tous les systèmes exploitation, la documentation, les manuels
techniques qui décrivent les fonctionnement et l'utilisation de programmes
tiennent une place importante. Dans le cadre de ses efforts pour créer un
système d'exploitation libre de qualité élevé, le projet Debian fait tout
son possible pour fournir à tous ses utilisateurs une bonne documentation
dans une forme facilement accessible.</p>

<p>Consultez la <a href="doc/">page sur la documentation</a> pour voir la 
liste des manuels de Debian et d'autres documentations, y compris le Manuel
d'installation, la FAQ Debian et d'autres manuels pour les utilisateurs et
les développeurs.</p>


<toc-add-entry name="consultants" href="consultants/">Consultants</toc-add-entry>

<p>Debian est constitué de logiciels libres et une aide gratuite est offerte
àtravers des listes de discussion. Néanmoins, certains
n'ont pas de temps à y consacrer ou ont des besoins
spécifiques. Aussi souhaitent-ils louer les services de quelqu'un afin
de faire la maintenance de leur système ou d'ajouter des
fonctionnalités supplémentaires. Vous trouverez sur <a
href="consultants/">la page des consultants</a> une liste de
personnes ou sociétés susceptibles de pouvoir répondre à cette demande.</p>


<toc-add-entry name="release" href="releases/stable/">Problèmes connus</toc-add-entry>

<p>Les limitations et, le cas échéant, les problèmes graves de la
distribution stable actuelle se trouvent décrits dans les pages de <a
href="releases/stable/">la version publiée</a>.

<p> Référez-vous particulièrement aux
<a href="releases/stable/releasenotes">notes de publication</a> pour votre architecture
et aux <a href="releases/stable/errata">errata connus</a>.</p>
