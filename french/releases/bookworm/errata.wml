#use wml::debian::template title="Debian 12 – Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="571f37332bde9e6cd25f3ea49eea7661b8cf7d70" maintainer="Baptiste Jammet"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problèmes connus</toc-add-entry>
<toc-add-entry name="security">Problèmes de sécurité</toc-add-entry>

<p>
L'équipe de sécurité Debian produit des mises à jour de paquets de la
version stable dans lesquels elle a identifié des problèmes de
sécurité. Merci de consulter les
<a href="$(HOME)/security/">pages concernant la sécurité</a> pour plus
d'informations concernant les problèmes de ce type identifiés dans 
<q>Bookworm</q>.
</p>

<p>
Si vous utilisez APT, ajoutez la ligne suivante à votre fichier
<tt>/etc/apt/sources.list</tt> pour récupérer les dernières mises à jour de
sécurité&nbsp;:
</p>

<pre>
  deb http://security.debian.org/ bookworm-security main contrib non-free
</pre>

<p>
Ensuite, lancez <kbd>apt update</kbd> suivi de <kbd>apt upgrade</kbd>.
</p>


<toc-add-entry name="pointrelease">Les versions intermédiaires</toc-add-entry>

<p>
Dans le cas où il y a plusieurs problèmes critiques ou plusieurs mises
à jour de sécurité, la version de la distribution est parfois mise à jour.
Généralement, ces mises à jour sont indiquées comme étant des versions
intermédiaires.</p>

<!-- <ul>
  <li>La première version intermédiaire, 12.1, a été publiée le
      <a href="$(HOME)/News/2017/XXXXX">XXXXX</a>.</li>
</ul> -->

<ifeq <current_release_bookworm> 12.0 "

<p>
À l'heure actuelle, il n'y a pas de mise à jour pour Debian 12.
</p>" "

<p>
Veuillez consulter le <a
href=http://http.us.debian.org/debian/dists/bookworm/ChangeLog>journal des
modifications</a> pour obtenir le détail des modifications entre la
version&nbsp;12.0 et la version&nbsp;<current_release_bookworm/>.
</p>"/>


<p>
Les corrections apportées à la version stable de la distribution passent
souvent par une période de test étendue avant d'être acceptées dans l'archive.
Cependant, ces corrections sont disponibles dans le répertoire <a
href="http://ftp.debian.org/debian/dists/bookworm-proposed-updates/">\
dists/bookworm-proposed-updates</a> de tout miroir de l'archive Debian.
</p>

<p>
Si vous utilisez APT pour mettre à jour vos paquets, vous pouvez installer les
mises à jour proposées en ajoutant la ligne suivante dans votre fichier
<tt>/etc/apt/sources.list</tt>&nbsp;:
</p>

<pre>
  \# Ajouts proposés pour une version intermédiaire 12
  deb http://deb.debian.org/debian bookworm-proposed-updates main contrib non-free
</pre>

<p>Ensuite, exécutez <kbd>apt update</kbd> suivi de
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Système d'installation</toc-add-entry>

<p>
Pour plus d'informations à propos des errata et des mises à jour du système
d'installation, consultez la page de <a href="debian-installer/">l'installateur</a>.
</p>
