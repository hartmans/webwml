#use wml::debian::translation-check translation="43ee4d624248d2eac95a5997ba86398be35914ec" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 11.2</define-tag>
<define-tag release_date>2021-12-18</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la deuxième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction authheaders "Nouvelle version amont de correction de bogues">
<correction base-files "Mise à jour de /etc/debian_version pour cette version">
<correction bpftrace "Correction d'indexation de tableau">
<correction brltty "Correction d'opération sous X quand sysvinit est utilisé">
<correction btrbk "Correction d'une régression dans la mise à jour pour le CVE-2021-38173">
<correction calibre "Correction d'erreur de syntaxe">
<correction chrony "Correction du lien d'un socket à un périphérique réseau avec un nom de plus de 3 caractères quand le filtre d'appel système est activé">
<correction cmake "Ajout de PostgreSQL 13 aux versions connues">
<correction containerd "Nouvelle version amont stable ; gestion de l'analyse de manifeste OCI ambigu [CVE-2021-41190] ; prise en charge de <q>clone3</q> dans le profil de seccomp par défaut">
<correction curl "Retrait de -ffile-prefix-map de curl-config, corrigeant la co-installabilité de libcurl4-gnutls-dev sous multiarchitecture">
<correction datatables.js "Correction d'une protection insuffisante des tableaux passés à la fonction « escape entities » d'HTML [CVE-2021-23445]">
<correction debian-edu-config "pxe-addfirmware : correction du chemin du serveur TFTP ; prise en charge améliorée de la configuration et de l'entretien du chroot LTSP">
<correction debian-edu-doc "Mise à jour du manuel de Debian Edu Bullseye à partir du wiki ; mise à jour des traductions">
<correction debian-installer "Reconstruction avec proposed-updates ; mise à jour de l'ABI de Linux vers la version -10">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction distro-info-data "Mise à jour des données incluses pour Ubuntu 14.04 et 16.04 ESM ; ajout d'Ubuntu 22.04 LTS">
<correction docker.io "Correction d'une possible modification des permissions du système de fichiers hôte [CVE-2021-41089] ; verrouillage des permissions de fichier dans /var/lib/docker [CVE-2021-41091] ; envoi évité des identifiants au registre par défaut [CVE-2021-41092] ; ajout de la prise en charge de l'appel système <q>clone3</q> dans la politique par défaut de seccomp">
<correction edk2 "Traitement de la vulnérabilité TOCTOU de Boot Guard [CVE-2019-11098]">
<correction freeipmi "Installation des fichiers pkgconfig à leur emplacement correct">
<correction gdal "Correction de la prise en charge de l'extraction de BAG 2.0 dans le pilote LVBAG">
<correction gerbv "Correction d'un problème d'écriture hors limites [CVE-2021-40391]">
<correction gmp "Correction d'un problème de dépassement d'entier et de tampon [CVE-2021-43618]">
<correction golang-1.15 "Nouvelle version amont stable ; correction de <q>net/http : panique due à une lecture en situation de compétition de persistConn après une handler panic </q> [CVE-2021-36221] ; correction de <q>archive/zip : un dépassement dans la vérification de pré-allocation peut provoquer un « Out of Memory panic» </q> [CVE-2021-39293] ; correction d'un problème de débordement de tampon [CVE-2021-38297], d'un problème de lecture hors limites [CVE-2021-41771], de problèmes de déni de service [CVE-2021-44716 CVE-2021-44717]">
<correction grass "Correction des formats GDAL quand la description contient le caractère deux points">
<correction horizon "Réactivation des traductions">
<correction htmldoc "Correction de problèmes de dépassement de tampon [CVE-2021-40985 CVE-2021-43579]">
<correction im-config "Fcitx5 préféré à Fcitx4">
<correction isync "Correction de plusieurs problèmes de dépassement de tampon [CVE-2021-3657]">
<correction jqueryui "Correction de problèmes d'exécution de code non fiable [CVE-2021-41182 CVE-2021-41183 CVE-2021-41184]">
<correction jwm "Correction de plantage lors de l'utilisation de l'item de menu <q>Move</q>">
<correction keepalived "Correction de la politique de DBus trop ouverte [CVE-2021-44225]">
<correction keystone "Résolution d'une fuite d'information permettant de déterminer l'existence d'un utilisateur [CVE-2021-38155] ; application de quelques améliorations de performance au fichier keystone-uwsgi.ini par défaut">
<correction kodi "Correction de dépassement de tampon dans les listes de lecture de PLS [CVE-2021-42917]">
<correction libayatana-indicator "Mise à l'échelle des icônes chargées à partir d'un fichier ; plantages réguliers évités dans les applets d'indicateur">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libencode-perl "Correction d'une fuite de mémoire dans Encode.xs">
<correction libseccomp "Ajout de la prise en charge des appels système jusqu'à Linux 5.15">
<correction linux "Nouvelle version amont ; passage de l'ABI à la version 10 ; RT : mise à jour vers la version 5.10.83-rt58">
<correction linux-signed-amd64 "Nouvelle version amont ; passage de l'ABI à la version 10 ; RT : mise à jour vers la version 5.10.83-rt58">
<correction linux-signed-arm64 "Nouvelle version amont ; passage de l'ABI à la version 10 ; RT : mise à jour vers la version 5.10.83-rt58">
<correction linux-signed-i386 "Nouvelle version amont ; passage de l'ABI à la version 10 ; RT : mise à jour vers la version 5.10.83-rt58">
<correction lldpd "Correction d'un problème de dépassement de tas [CVE-2021-43612] ; pas de réglage de l'étiquette VLAN si le client ne l'a pas défini">
<correction mrtg "Correction d'erreurs dans les noms de variable">
<correction node-getobject "Résolution d'un problème de pollution de prototype [CVE-2020-28282]">
<correction node-json-schema "Résolution d'un problème de pollution de prototype [CVE-2021-3918]">
<correction open3d "Dépendance de python3-open3d à python3-numpy assurée">
<correction opendmarc "Correction d'opendmarc-import ; augmentation de la longueur maximale gérée des jetons dans les en-têtes d'ARC_Seal, résolvant des plantages">
<correction plib "Correction d'un problème de dépassement d'entier [CVE-2021-38714]">
<correction plocate "Correction d'un problème où les caractères non ASCII seraient incorrectement protégés">
<correction poco "Correction de l'installation des fichiers de CMake">
<correction privoxy "Correction de fuites de mémoire [CVE-2021-44540 CVE-2021-44541 CVE-2021-44542] ; correction d'un problème de script intersite [CVE-2021-44543]">
<correction publicsuffix "Mise à jour des données incluses">
<correction python-django "Nouvelle version de sécurité amont : correction d'un possible contournement d'un contrôle d'accès amont basé sur des chemins d'URL [CVE-2021-44420]">
<correction python-eventlet "Correction de la compatibilité avec dnspython 2">
<correction python-virtualenv "Correction de plantage lors de l'utilisation de --no-setuptools">
<correction ros-ros-comm "Correction d'un problème de déni de service [CVE-2021-37146]">
<correction ruby-httpclient "Utilisation du magasin de certifications du système">
<correction rustc-mozilla "Nouveau paquet source pour prendre en charge la construction des dernières versions de firefox-esr et de thunderbird">
<correction supysonic "Création d'un lien symbolique vers jquery plutôt que de le charger directement ; création des liens symboliques corrects vers les fichiers CSS minimisés de bootstrap">
<correction tzdata "Mise à jour des données pour les Fidji et la Palestine">
<correction udisks2 "Options de montage : utilisation toujours de errors=remount-ro pour les systèmes de fichiers ext [CVE-2021-3802] ; utilisation de la commande mkfs pour formater les partitions exfat ; ajout de Recommends exfatprogs comme alternative préférée">
<correction ulfius "Correction de l'utilisation d'allocateurs personnalisés avec ulfius_url_decode et ulfius_url_encode">
<correction vim "Correction de dépassement de tas [CVE-2021-3770 CVE-2021-3778], d'un problème d'utilisation de mémoire après libération [CVE-2021-3796] ; retrait des alternatives à vim-gtk pendant la transition de vim-gtk à vim-gtk3, favorisant les mises à niveau à partir de Buster">
<correction wget "Correction des téléchargements de plus de 2 Go sur les systèmes 32 bits">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2021 4980 qemu>
<dsa 2021 4981 firefox-esr>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4984 flatpak>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4988 libreoffice>
<dsa 2021 4989 strongswan>
<dsa 2021 4992 php7.4>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4996 wpewebkit>
<dsa 2021 4998 ffmpeg>
<dsa 2021 5002 containerd>
<dsa 2021 5003 ldb>
<dsa 2021 5003 samba>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5007 postgresql-13>
<dsa 2021 5008 node-tar>
<dsa 2021 5009 tomcat9>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5016 nss>
<dsa 2021 5017 xen>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5022 apache-log4j2>
</table>



<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>


<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
