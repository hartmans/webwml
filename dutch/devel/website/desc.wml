#use wml::debian::template title="Hoe www.debian.org gemaakt wordt"
#use wml::debian::toc
#use wml::debian::translation-check translation="6d9ec7201dcfa229e0cdf7f93c34b3d4a8509ebe"

<p>De <em>&ldquo;webboom&rdquo;</em> van Debian, een verzameling mappen en bestanden waaruit onze website bestaat, is te vinden op www-master.debian.org in de map <tt>/org/www.debian.org/www</tt>. Het grootste deel van de pagina's bestaat uit gewone HTML-bestanden (d.w.z. niet geproduceerd met iets dynamisch als een CGI- of een PHP-script), omdat de website gespiegeld wordt.

<p>De site wordt gegenereerd op een van de volgende drie manieren:
<ul>
  <li>het grootste deel wordt gegenereerd met behulp van WML uit de
      <a href="$(DEVEL)/website/using_git">git-opslagruimte <q>webwml</q></a>
  <li>de documentatie wordt gegenereerd, ofwel met behulp van DocBook XML (het
      gebruik van DebianDoc SGML wordt afgebouwd) uit de
      <a href="$(DOC)/vcs">git-opslagruimte <q>ddp</q></a>, ofwel
      met <a href="#scripts">cron-scripts</a> uit de overeenkomstige
      Debian-pakketten
  <li>delen van de site worden gegenereerd met scripts die van andere bronnen
      gebruik maken, bijvoorbeeld de pagina's om op mailinglijsten in te
      tekenen of er zich op uit te schrijven
</ul>

<p>Een automatische update (vanuit de git-opslagruimte en andere bronnen
   van de webboom) wordt zesmaal per dag uitgevoerd.

<p>Indien u wilt bijdragen aan de site, mag u <strong>niet</strong> gewoon
   items toevoegen aan de map <code>www/</code> of er items in bewerken. U moet
   eerst <a href="mailto:webmaster@debian.org">de webmasters</a> contacteren.

<p>Al de bestanden en mappen zijn eigendom van de groep debwww en kunnen door
die groep beschreven worden, zodat het webteam de bestanden in de web-map kan
wijzigen. De modus 2775 voor de mappen betekent dat alle bestanden die in die
mappen aangemaakt worden de groep - debwww in dit geval - overerven.
Iedereen uit de groep debwww wordt verondersteld '<code>umask 002</code>' in te
stellen, zodat de bestanden gecreëerd worden met schrijfrechten voor de groep.

<toc-display />

<hrline />


<toc-add-entry name="look">Uitzicht &amp; aanvoelen</toc-add-entry>

<p>We geven de pagina's een identieke uitstraling door al het detailwerk in
verband met het toevoegen aan pagina's van kop- en voetteksten over te laten aan
<a href="https://packages.debian.org/unstable/web/wml">WML</a>. Hoewel een
.wml-pagina er op het eerste gezicht uitziet als HTML, is HTML slechts één
van de types extra informatie die in .wml gebruikt kunnen worden. Nadat WML
klaar is met het toepassen van zijn verschillende filters op een bestand, is
het eindproduct echte HTML. Om u een idee te geven van de kracht van WML: u
kunt Perl-code toevoegen aan een pagina, zodat u ermee, nou ja, bijna alles
kunt gedaan krijgen.

<p>Merk evenwel op dat WML enkel op een heel basaal niveau de geldigheid van uw
HTML-code controleert (en deze soms automatisch corrigeert). U moet
<a href="https://packages.debian.org/unstable/web/weblint">weblint</a>
en/of
<a href="https://packages.debian.org/unstable/web/tidy">tidy</a>
installeren om uw HTML-code te valideren.

<p>Onze webpagina's voldoen momenteel aan de standaard
<a href="http://www.w3.org/TR/html4/">HTML 4.01 Strict</a>.

<p>Iedereen die aan veel pagina's werkt, zou WML moeten installeren, zodat
testen uitgevoerd kunnen worden om er zeker van te zijn dat het resultaat
datgene is wat gewenst wordt. Indien u op de computer met Debian werkt, kunt u
makkelijk het pakket <code>wml</code> installeren. Lees voor meer informatie de
pagina's over
<a href="using_wml">het gebruik van WML</a>.


<toc-add-entry name="sources">Broncode</toc-add-entry>

<p>De broncode voor de webpagina's wordt onder git bewaard. Git is een
versiecontrolesysteem dat u toelaat een logboek bij te houden van wie wanneer
waarom welke verandering aanbracht, enz. Het is een veilige manier om de
gelijktijdige bewerking van bronbestanden door meerdere auteurs te beheren, en
voor ons is dat cruciaal omdat het Debian webteam nogal uitgebreid is.

<p>Indien u niet vertrouwd bent met dit programma, zult u wellicht de pagina's
over <a href="using_git">het gebruik van git</a> willen lezen.

<p>De webwml-basismap in de git-opslagruimte bevat mappen die vernoemd zijn
naar de talen waarin de website vertaald is, twee make-bestanden en
verschillende scripts. De namen van de mappen die de vertalingen bevatten
moeten in het Engels (bijvoorbeeld "dutch" en niet "Nederlands") zijn en uit
kleine letters bestaan.

<p>Het belangrijkste van de twee make-bestanden is Makefile.common en dit
bevat, zoals de naam het zegt, een aantal gemeenschappelijke regels welke
toegepast worden door het opnemen van dit bestand in de andere make-bestanden.

<p>Elk van de taalmappen bevat make-bestanden, WML-broncode en onderliggende
mappen. De namen van de mappen en bestanden zijn niet verschillend om de links
voor alle talen correct te kunnen houden. De mappen kunnen ook .wmlrc-bestanden
bevatten welke nuttige informatie bevatten voor WML.

<p>De map webwml/english/template bevat speciale WML-bestanden welke we
templates noemen, omdat er vanuit alle andere bestanden naar kan worden
verwezen met het commando <code>#use</code>.

<p>Om wijzigingen in de templates te laten doorstromen naar de bestanden die ze
gebruiken, hebben de bestanden deze templates als makefile-vereisten. Aangezien
de grote meerderheid van de bestanden het template "template" gebruiken via een
regel "<code>#use wml::debian::template</code>" bovenaan het bestand, geldt
juist dat template als generieke vereiste (van toepassing op alle bestanden).
Natuurlijk bestaan er uitzonderingen op deze regel.


<toc-add-entry name="scripts">Scripts</toc-add-entry>

<p>De scripts werden hoofdzakelijk geschreven in shell of Perl. Sommige ervan
bestaan apart en andere zijn geïntegreerd in de WML bronbestanden.</p>

<p>De broncode voor de belangrijkste scripts voor het opnieuw bouwen
van www-master bevinden zich in de
<a href="https://salsa.debian.org/webmaster-team/cron.git">git-opslagruimte
webmaster-team/cron</a>.
</p>

<p>De broncode voor de scripts voor het opnieuw bouwen van packages.debian.org
bevinden zich in de
<a href="https://salsa.debian.org/webmaster-team/packages">git-opslagruimte
webmaster-team/packages</a>.</p>


<toc-add-entry name="help">Hoe u kunt helpen</toc-add-entry>

<p>We nodigen iedereen die geïnteresseerd is uit om ons te helpen de site van
Debian zo goed mogelijk te maken. Indien u over waardevolle informatie in
verband met Debian beschikt, welke volgens u ontbreekt in onze pagina's,
<a href="mailto:debian-www@lists.debian.org">deel deze dan
met ons</a> en wij zullen ervoor zorgen dat ze opgenomen wordt.

<p>We kunnen steeds hulp gebruiken bij het ontwerpen van pagina's (op het
gebied van grafische vormgeving en lay-out) en ook bij het zuiver houden van
onze HTML. Regelmatig voeren we de volgende controles uit op de volledige
website:</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">URL-controle</a>
  <li><a href="https://www-master.debian.org/build-logs/validate/">WDG html-validatie</a>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a>
</ul>

<p>Hulp bij het nalezen van de bovenstaande logboeken en het repareren van de
problemen is steeds welkom.</p>

<p>De huidige logboeken over het bouwen van de website zijn te vinden op
<url "https://www-master.debian.org/build-logs/">.</p>

<p>Indien u vloeiend Engels spreekt, stellen we het op prijs dat u onze
pagina's naleest en <a href="mailto:debian-www@lists.debian.org">ons</a> alle
fouten rapporteert.

<p>Indien u een andere taal spreekt, bent u misschien geïnteresseerd om ons te
helpen bij het vertalen van onze pagina's naar uw taal. Indien er al een
vertaling gemaakt werd, maar er problemen mee zijn, kunt u gaan kijken op de
lijst met <a href="translation_coordinators">\
vertaalcoördinatoren</a> en contact opnemen met de leider voor uw taal in
functie van een oplossing voor het probleem. Indien u zelf pagina's wilt
vertalen, kunt u meer informatie vinden op de pagina over
<a href="translating">dat onderwerp</a>.

<p>Er bestaat ook een <a href="todo">TODO-bestand</a>, bekijk het eens.</p>


<toc-add-entry name="nohelp">Hoe u <strong>niet</strong> moet helpen</toc-add-entry>

<p><em>[V] Ik wil <var>leuke webfunctie</var> plaatsen in www.debian.org,
mag ik?</em>

<p>[A] Neen. We willen www.debian.org zo toegankelijk mogelijk maken, dus
<ul>
    <li>geen browserspecifieke "extensies".
    <li>niet louter gebruik maken van afbeeldingen. Afbeeldingen kunnen gebruikt
        worden ter verduidelijking, maar de informatie op www.debian.org moet
        toegankelijk blijven voor een pure tekstuele webbrowser, zoals lynx.
</ul>

<p><em>[V] Ik heb dit leuke idee. Kunt u FOO activeren in de HTTPD van
           www.debian.org, alstublieft?</em>

<p>[A] Neen. We willen het leven makkelijk maken voor beheerders die een
spiegelserver voor www.debian.org opzetten, dus alstublieft geen speciale
HTTPD-functionaliteit. Neen, zelfs geen SSI. Er werd een uitzondering gemaakt
voor "content negotiation", het onderhandelen over inhoud. Dit is omdat het de
enige robuuste manier is om de pagina's in verschillende talen te bedienen.
