#use wml::debian::translation-check translation="d9e5cd3d7df23feb17458b95c465e062e9cd6e5a"
<define-tag pagetitle>Debian 10 is bijgewerkt: 10.11 werd uitgebracht</define-tag>
<define-tag release_date>09-10-2021</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.11</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de elfde update aan van zijn oude
stabiele distributie Debian <release> (codename <q><codenaam></q>). Deze
tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie, afkomstig van een
bijgewerkte Debian-spiegelserver.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone locaties.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst van spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Oplossingen voor diverse problemen</h2>

<p>Deze update van oldstable, de oude stabiele release, voegt een paar belangrijke correcties toe aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction atftp "Reparatie van bufferoverflow [CVE-2021-41054]">
<correction base-files "Update voor de tussenrelease 10.11">
<correction btrbk "Oplossen van een probleem met willekeurige code-uitvoering [CVE-2021-38173]">
<correction clamav "Nieuwe stabiele bovenstroomse release; reparatie van segmentatiefouten van clamdscan wanneer --fdpass en --multipass samen met ExcludePath gebruikt worden">
<correction commons-io "Oplossen van een probleem bij het doorlopen van een pad [CVE-2021-29425]">
<correction cyrus-imapd "Oplossen van een denial-of-service-probleem [CVE-2021-33582]">
<correction debconf "Controleren of whiptail of dialoog daadwerkelijk bruikbaar zijn">
<correction debian-installer "Opnieuw gebouwd tegen buster-proposed-updates; updaten van Linux ABI naar 4.19.0-18">
<correction debian-installer-netboot-images "Opnieuw gebouwd tegen buster-proposed-updates">
<correction distcc "Reparatie van GCC cross-compiler links in update-distcc-symlinks en toevoegen van ondersteuning voor clang en CUDA (nvcc)">
<correction distro-info-data "Update van ingesloten gegevens voor verschillende releases">
<correction dwarf-fortress "Verwijderen van niet-distribueerbare vooraf gecompileerde gedeelde bibliotheken uit het bron-tararchief">
<correction espeak-ng "Oplossing voor het gebruik van espeak met mbrola-fr4 wanneer mbrola-fr1 niet geïnstalleerd is">
<correction gcc-mingw-w64 "Repareren van gcov-hantering">
<correction gthumb "Oplossing voor een probleem met op heap gebaseerde bufferoverflow [CVE-2019-20326]">
<correction hg-git "Oplossing voor een testfalen met recente git-versies">
<correction htslib "Reparatie van autopkgtest op i386">
<correction http-parser "Oplossing voor een probleem met het smokkelen van HTTP-verzoekenn [CVE-2019-15605]">
<correction irssi "Oplossing voor een probleem van gebruik na vrijgave bij het zenden van de SASL-login naar de server [CVE-2019-13045]">
<correction java-atk-wrapper "Ook dbus gebruiken om na te gaan of toegankelijkheidsvoorzieningen geactiveerd zijn">
<correction krb5 "Oplossing voor een crash van de KDC null-verwijzing bij een FAST-verzoek zonder serverveld [CVE-2021-37750]; reparatie voor een geheugenlek in krb5_gss_inquire_cred">
<correction libdatetime-timezone-perl "Nieuwe bovenstroomse stabiele release; updaten van DST-regels voor Samoa en Jordon; bevestiging dat er geen schrikkelseconde nodig is op 31-12-2021">
<correction libpam-tacplus "Voorkomen dat gedeelde vertrouwelijke informatie in platte tekst wordt toegevoegd aan het systeemlogboek [CVE-2020-13881]">
<correction linux "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q>, oplossing voor problemen met lxc-attach; nieuwe bovenstroomse stabiele release; verhogen van ABI-versie naar 18; [rt] Opwaardering naar 4.19.207-rt88; usb: hso: reparatie van foutbehandelingscode van hso_create_net_device [CVE-2021-37159]">
<correction linux-latest "Opwaardering naar kernel-ABI 4.19.0-18">
<correction linux-signed-amd64 "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q>, oplossen van problemen met lxc-attach; nieuwe bovenstroomse stabiele release; verhogen van ABI-versie naar 18; [rt] Opwaardering naar 4.19.207-rt88; usb: hso: reparatie van foutbehandelingscode van hso_create_net_device [CVE-2021-37159]">
<correction linux-signed-arm64 "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q>, oplossen van problemen met lxc-attach; nieuwe bovenstroomse stabiele release; verhogen van ABI-versie naar 18; [rt] Opwaardering naar 4.19.207-rt88; usb: hso: reparatie van foutbehandelingscode van hso_create_net_device [CVE-2021-37159]">
<correction linux-signed-i386 "<q>proc: Track /proc/$pid/attr/ opener mm_struct</q>, oplossen van problemen met lxc-attach; nieuwe bovenstroomse stabiele release; verhogen van ABI-versie naar 18; [rt] Opwaardering naar 4.19.207-rt88; usb: hso: reparatie van foutbehandelingscode van hso_create_net_device [CVE-2021-37159]">
<correction mariadb-10.3 "Nieuwe bovenstroomse stabiele release; oplossen van beveiligingsproblemen [CVE-2021-2389 CVE-2021-2372]; repareren van het uitvoerbare pad van Perl in scripts">
<correction modsecurity-crs "Oplossen van probleem met request body [CVE-2021-35368]">
<correction node-ansi-regex "Oplossen van een denial-of-serviceprobleem op basis van reguliere expressies [CVE-2021-3807]">
<correction node-axios "Oplossen van een denial-of-serviceprobleem op basis van reguliere expressies [CVE-2021-3749]">
<correction node-jszip "Gebruiken van een null prototype-object voor this.bestanden [CVE-2021-23413]">
<correction node-tar "Niet-mappaden verwijderen uit de mapcache [CVE-2021-32803]; absolute paden vollediger strippen [CVE-2021-32804]">
<correction nvidia-cuda-toolkit "Reparatie voor het instellen van NVVMIR_LIBRARY_DIR op ppc64el">
<correction nvidia-graphics-drivers "Nieuwe bovenstroomse stabiele release; denial-of-serviceproblemen oplossen [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095]; nvidia-driver-libs: toevoegen van Recommends: libnvidia-encode1">
<correction nvidia-graphics-drivers-legacy-390xx "Nieuwe bovenstroomse stabiele release; denial-of-serviceproblemen oplossen [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095]; nvidia-legacy-390xx-driver-libs: toevoegen van Recommends: libnvidia-legacy-390xx-encode1">
<correction postgresql-11 "Nieuwe bovenstroomse stabiele release; repareren van foute planning van de herhaalde toepassing van een projectiestap [CVE-2021-3677]; SSL-heronderhandeling vollediger uitsluiten">
<correction proftpd-dfsg "Oplossing voor <q>mod_radius lekt geheugeninhoud naar de radius-server</q>, <q>door de client geïnitieerde heronderhandeling voor FTPS kan niet worden uitgeschakeld</q>, navigatie naar symbolisch gekoppelde mappen, crash van mod_sftp wanneer pubkey-auth gebruikt wordt met DSA-sleutels">
<correction psmisc "Reparatie van regressie in killall dat processen met een langere naam dan 15 tekens niet kan terugvinden">
<correction python-uflash "Updaten van firmware-URL">
<correction request-tracker4 "Oplossen van een probleem van aanvallen op login timing via een zijkanaal [CVE-2021-38562]">
<correction ring "Oplossen van een denial-of-serviceprobleem in de ingebedde kopie van pjproject [CVE-2021-21375]">
<correction sabnzbdplus "Ontsnappen uit mappen voorkomen in de functie renamer [CVE-2021-29488]">
<correction shim "Toevoegen van arm64-patch om sectielay-out aan te passen en crashen te voorkomen; in de onveilige modus niet afbreken als we de MokListXRT-variabele niet kunnen creëren; niet afbreken bij het mislukken van een grub-installatie; in de plaats daarvan een waarschuwing geven">
<correction shim-helpers-amd64-signed "Toevoegen van arm64-patch om sectielay-out aan te passen en crashen te voorkomen; in de onveilige modus niet afbreken als we de MokListXRT-variabele niet kunnen creëren; niet afbreken bij het mislukken van een grub-installatie; in de plaats daarvan een waarschuwing geven">
<correction shim-helpers-arm64-signed "Toevoegen van arm64-patch om sectielay-out aan te passen en crashen te voorkomen; in de onveilige modus niet afbreken als we de MokListXRT-variabele niet kunnen creëren; niet afbreken bij het mislukken van een grub-installatie; in de plaats daarvan een waarschuwing geven">
<correction shim-helpers-i386-signed "Toevoegen van arm64-patch om sectielay-out aan te passen en crashen te voorkomen; in de onveilige modus niet afbreken als we de MokListXRT-variabele niet kunnen creëren; niet afbreken bij het mislukken van een grub-installatie; in de plaats daarvan een waarschuwing geven">
<correction shim-signed "Omzeilen van problemen die op arm64 het opstartproces onklaar maken door op dat platform een oudere werkende versie van een niet-ondertekende shim op te nemen; voor arm64 momenteel terug overschakelen naar een niet-ondertekende build; arm64-patch toevoegen om sectielay-out aan te passen en crashen te voorkomen; in de onveilige modus niet afbreken als we de MokListXRT-variabele niet kunnen creëren; niet afbreken bij het mislukken van een grub-installatie; in de plaats daarvan een waarschuwing geven">
<correction shiro "Oplossen van problemen met het omzeilen van authenticatie [CVE-2020-1957 CVE-2020-11989 CVE-2020-13933 CVE-2020-17510]; updaten van een patch voor Spring Framework compatibiliteit; ondersteunen van Guice 4">
<correction tzdata "Updaten van DST-regels voor Samoa en Jordan; bevestiging dat er geen schrikkelseconde nodig is op 31-12-2021">
<correction ublock-origin "Nieuwe bovenstroomse stabiele release; oplossen van een denial-of-serviceprobleem [CVE-2021-36773]">
<correction ulfius "Ervoor zorgen dat geheugen geïnitialiseerd wordt vóór gebruik [CVE-2021-40540]">
<correction xmlgraphics-commons "Oplossen van een probleem met vervalsing van Server-Side Request [CVE-2020-11988]">
<correction yubikey-manager "Bij yubikey-manager de ontbrekende vereiste python3-pkg-resources toevoegen">
</table>


<h2>Beveiligingsupdates</h2>


<p>Deze revisie voegt de volgende beveiligingsupdates toe aan de release
oldstable. Het beveiligingsteam heeft voor elk van deze updates al een advies
uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2021 4842 thunderbird>
<dsa 2021 4866 thunderbird>
<dsa 2021 4876 thunderbird>
<dsa 2021 4897 thunderbird>
<dsa 2021 4927 thunderbird>
<dsa 2021 4931 xen>
<dsa 2021 4932 tor>
<dsa 2021 4933 nettle>
<dsa 2021 4934 intel-microcode>
<dsa 2021 4935 php7.3>
<dsa 2021 4936 libuv1>
<dsa 2021 4937 apache2>
<dsa 2021 4938 linuxptp>
<dsa 2021 4939 firefox-esr>
<dsa 2021 4940 thunderbird>
<dsa 2021 4941 linux-signed-amd64>
<dsa 2021 4941 linux-signed-arm64>
<dsa 2021 4941 linux-signed-i386>
<dsa 2021 4941 linux>
<dsa 2021 4942 systemd>
<dsa 2021 4943 lemonldap-ng>
<dsa 2021 4944 krb5>
<dsa 2021 4945 webkit2gtk>
<dsa 2021 4946 openjdk-11-jre-dcevm>
<dsa 2021 4946 openjdk-11>
<dsa 2021 4947 libsndfile>
<dsa 2021 4948 aspell>
<dsa 2021 4949 jetty9>
<dsa 2021 4950 ansible>
<dsa 2021 4951 bluez>
<dsa 2021 4952 tomcat9>
<dsa 2021 4953 lynx>
<dsa 2021 4954 c-ares>
<dsa 2021 4955 libspf2>
<dsa 2021 4956 firefox-esr>
<dsa 2021 4957 trafficserver>
<dsa 2021 4958 exiv2>
<dsa 2021 4959 thunderbird>
<dsa 2021 4961 tor>
<dsa 2021 4962 ledgersmb>
<dsa 2021 4963 openssl>
<dsa 2021 4964 grilo>
<dsa 2021 4967 squashfs-tools>
<dsa 2021 4969 firefox-esr>
<dsa 2021 4970 postorius>
<dsa 2021 4971 ntfs-3g>
<dsa 2021 4973 thunderbird>
<dsa 2021 4974 nextcloud-desktop>
<dsa 2021 4975 webkit2gtk>
<dsa 2021 4979 mediawiki>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten werden verwijderd wegens omstandigheden die buiten onze macht vallen:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction birdtray "Incompatibel met recentere Thunderbird-versies">
<correction libprotocol-acme-perl "Ondersteunt enkel de verouderde versie 1 van ACME">

</table>

<h2>Het Debian-installatieprogramma</h2>
<p>Het installatieprogramma werd bijgewerkt om de reparaties die met deze tussenrelease in oldstable opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Voorgestelde updates voor de distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>informatie over de distributie oldstable (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een vereniging van ontwikkelaars van vrije software
die vrijwillig tijd en moeite steken in het produceren van het volledig vrije
besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
distributies op &lt;debian-release@lists.debian.org&gt;.</p>

