#use wml::debian::translation-check translation="d9e5cd3d7df23feb17458b95c465e062e9cd6e5a"
<define-tag pagetitle>Debian 11 is bijgewerkt: 11.1 werd uitgebracht</define-tag>
<define-tag release_date>09-10-2021</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de eerste update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie, afkomstig van een
bijgewerkte Debian-spiegelserver.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone locaties.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst van spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Oplossingen voor diverse problemen</h2>

<p>Deze update van stable, de stabiele release, voegt een paar belangrijke
correcties toe aan de volgende pakketten</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction apr "Voorkomen van out-of-bounds array dereferentie">
<correction atftp "Reparatie van bufferoverflow [CVE-2021-41054]">
<correction automysqlbackup "Reparatie voor een crash bij het gebruik van <q>LATEST=yes</q>">
<correction base-files "Update voor de tussenrelease 11.1">
<correction clamav "Nieuwe stabiele bovenstroomse release; reparatie van segmentatiefouten van clamdscan wanneer --fdpass en --multipass samen met ExcludePath gebruikt worden">
<correction cloud-init "Vermijden van dubbele includedir in /etc/sudoers">
<correction cyrus-imapd "Oplossen van een denial-of-service-probleem [CVE-2021-33582]">
<correction dazzdb "Reparatie voor een use-after-free in DBstats">
<correction debian-edu-config "debian-edu-ltsp-install: uitbreiding van de hoofdservergerelateerde lijst met uitsluitingen; toevoegen van slapd en xrdp-sesman aan de lijst met gemaskeerde services">
<correction debian-installer "Opnieuw gebouwd tegen proposed updates; updaten van Linux ABI naar 5.10.0-9; gebruiken van udebs uit proposed-updates">
<correction debian-installer-netboot-images "Opnieuw gebouwd tegen proposed-updates; gebruiken van udebs uit proposed-updates en stable; met xz gecomprimeerde Packages-bestanden gebruiken">
<correction detox "Reparatie voor het behandelen van grote bestanden">
<correction devscripts "bullseye-backports instellen als doel van de optie --bpo">
<correction dlt-viewer "Ontbrekende qdlt/qdlt*.h header-bestanden toevoegen aan dev-pakket">
<correction dpdk "Nieuwe bovenstroomse stabiele release">
<correction fetchmail "Reparatie voor segmentatiefout en veiligheidsregressie">
<correction flatpak "Nieuwe bovenstroomse stabiele release; geen ongebruikelijke $XDG_RUNTIME_DIR-instelling overnemen in de sandbox">
<correction freeradius "Oplossing voor thread-crash en voorbeeldconfiguratie">
<correction galera-3 "Nieuwe bovenstroomse stabiele release">
<correction galera-4 "Nieuwe bovenstroomse stabiele release; oplossen van een circulair Conflicts-probleem met galera-3 door niet langer te voorzien in een virtueel pakket <q>galera</q>">
<correction glewlwyd "Oplossing voor een mogelijke bufferoverflow tijdens de validering van de FIDO2-ondertekening in de webauthn-registratie [CVE-2021-40818]">
<correction glibc "Herstarten van openssh-server, zelfs wanneer tijdens de opwaardering de configuratie ervan geannuleerd werd; reparatie van de terugval op text wanneer debconf onbruikbaar is">
<correction gnome-maps "Nieuwe bovenstroomse stabiele release; reparatie van een crash wanneer opgestart wordt met bovenaanzicht als laatst gebruikt kaarttype, terwijl er geen tegeldefinitie voor bovenaanzicht gevonden wordt; bij het afsluiten niet soms een defecte laatste weergavepositie noteren; reparatie voor het vastlopen bij het verslepen van routemarkeringen">
<correction gnome-shell "Nieuwe bovenstroomse stabiele release; reparatie voor het vastlopen na het annuleren van (sommige) system-modal dialogen; repareren van woordsuggesties in het schermtoetsenbord; reparatie van crashes">
<correction hdf5 "Aanpassen van pakketvereisten voor een beter verloop van opwaarderingen vanuit oudere releases">
<correction iotop-c "Correct omgaan met UTF-8 procesnamen">
<correction jailkit "Reparatie voor het aanmaken van jails die /dev moeten gebruiken; reparatie voor de controle op de aanwezigheid van de biblioheek">
<correction java-atk-wrapper "Ook dbus gebruiken om na te gaan of toegankelijkheidsvoorzieningen geactiveerd zijn">
<correction krb5 "Oplossing voor een crash van de KDC null-dereferentie bij een FAST-verzoek zonder serverveld [CVE-2021-37750]; reparatie voor een geheugenlek in krb5_gss_inquire_cred">
<correction libavif "De juiste bibliotheekmap gebruiken in het pkgconfig-bestand van libavif.pc">
<correction libbluray "Overschakelen naar ingebedde libasm; de versie van libasm-java is te nieuw">
<correction libdatetime-timezone-perl "Nieuwe bovenstroomse stabiele release; updaten van DST-regels voor Samoa en Jordon; bevestiging dat er geen schrikkelseconde nodig is op 31-12-2021">
<correction libslirp "Oplossen van verschillende problemen met bufferoverflow [CVE-2021-3592 CVE-2021-3593 CVE-2021-3594 CVE-2021-3595]">
<correction linux "Nieuwe bovenstroomse stabiele release; verhogen van ABI-versie naar 9; [rt] Opwaardering naar 5.10.65-rt53; [mipsel] bpf, mips: valideren van conditionele branch offsets [CVE-2021-38300]">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release; verhogen van ABI-versie naar 9; [rt] Opwaardering naar 5.10.65-rt53; [mipsel] bpf, mips: valideren van conditionele branch offsets [CVE-2021-38300]">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release; verhogen van ABI-versie naar 9; [rt] Opwaardering naar 5.10.65-rt53; [mipsel] bpf, mips: valideren van conditionele branch offsets [CVE-2021-38300]">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release; verhogen van ABI-versie naar 9; [rt] Opwaardering naar 5.10.65-rt53; [mipsel] bpf, mips: valideren van conditionele branch offsets [CVE-2021-38300]">
<correction mariadb-10.5 "Nieuwe bovenstroomse stabiele release; oplossingen voor beveiligingsproblemen [CVE-2021-2372 CVE-2021-2389]">
<correction mbrola "Reparatie voor detectie van bestandseinde">
<correction modsecurity-crs "Oplossen van probleem met request body bypass [CVE-2021-35368]">
<correction mtr "Reparatie voor een regressie in JSON-uitvoer">
<correction mutter "Nieuwe bovenstroomse stabiele release; kms: Verbetering van de behandeling van veel voorkomende videomodi die de mogelijke bandbreedte zouden kunnen overschrijden; zorgen voor een geldige venstertextuurgrootte na viewport-wijzigingen">
<correction nautilus "Het openen van meerdere geselecteerde bestanden in meerdere applicatie-instanties vermijden; venstergrootte en -positie niet opslaan bij tegels; verhelpen van enkele geheugenlekken; bijwerken van vertalingen">
<correction node-ansi-regex "Oplossen van een denial-of-serviceprobleem op basis van reguliere expressies [CVE-2021-3807]">
<correction node-axios "Oplossen van een denial-of-serviceprobleem op basis van reguliere expressies [CVE-2021-3749]">
<correction node-object-path "Problemen met prototypevervuiling oplossen [CVE-2021-23434 CVE-2021-3805]">
<correction node-prismjs "Oplossen van een denial-of-serviceprobleem op basis van reguliere expressies [CVE-2021-3801]">
<correction node-set-value "Problemen met prototypevervuiling oplossen [CVE-2021-23440]">
<correction node-tar "Niet-mappaden verwijderen uit de mapcache [CVE-2021-32803]; absolute paden vollediger strippen [CVE-2021-32804]">
<correction osmcoastline "Andere projecties dan WGS84 repareren">
<correction osmpbf "Herbouwen tegen protobuf 3.12.4">
<correction pam "Herstellen van een syntaxisfout in libpam0g.postinst wanneer een systemd unit faalt">
<correction perl "Beveiligingsupdate; repareren van een geheugenlek bij reguliere expressies">
<correction pglogical "Update voor de reparaties aan de momentopnameafhandeling in PostgreSQL 13.4">
<correction pmdk "Herstellen van ontbrekende barrières na niet-temporele memcpy">
<correction postgresql-13 "Nieuwe bovenstroomse stabiele release; repareren van foute planning van de herhaalde toepassing van een projectiestap [CVE-2021-3677]; SSL-heronderhandeling vollediger uitsluiten">
<correction proftpd-dfsg "Oplossing voor <q>mod_radius lekt geheugeninhoud naar de radius-server</q> en voor <q>sftp verbinding wordt afgebroken met </q>Corrupted MAC on input""; escapen van reeds ge-escapete SQL-tekst overslaan">
<correction pyx3 "Oplossen van probleem met horizontale uitlijning van lettertypen met texlive 2020">
<correction reportbug "Bijwerken van suite-namen na de release van bullseye">
<correction request-tracker4 "Oplossen van een probleem van aanvallen op login timing via een zijkanaal [CVE-2021-38562]">
<correction rhonabwy "Reparatie voor JWE CBC-tagberekening en JWS alg:none handtekeningverificatie">
<correction rpki-trust-anchors "Toevoegen van HTTPS-URL aan de LACNIC TAL">
<correction rsync "Opnieuw toevoegen van --copy-devices; reparatie voor regressie in --delay-updates; reparatie voor randgeval in --mkpath; reparatie voor rsync-ssl; reparatie voor --sparce en --inplace; bijwerken van beschikbare opties voor rrsync; verbeteringen aan de documentatie">
<correction ruby-rqrcode-rails3 "Oplossing voor compatibiliteit met ruby-rqrcode 1.0">
<correction sabnzbdplus "Voorkomen dat uit de map ontsnapt kan worden in de functie renamer [CVE-2021-29488]">
<correction shellcheck "Reparatie voor de weergave van de lange opties in de manpagina">
<correction shiro "Oplossen van problemen met het omzeilen van authenticatie [CVE-2020-1957 CVE-2020-11989 CVE-2020-13933 CVE-2020-17510]; updaten van een patch voor compatibiliteit met Spring Framework; ondersteunen van Guice 4">
<correction speech-dispatcher "Reparatie voor het instellen van de stemnaam voor de generieke module">
<correction telegram-desktop "Vermijden van crash wanneer auto-delete is ingeschakeld">
<correction termshark "Stijlen opnemen in het pakket">
<correction tmux "Reparatie van een race-conditie die ertoe leidt dat de configuratie niet wordt geladen als meerdere clients interactie hebben met de server terwijl deze wordt geïnitialiseerd">
<correction txt2man "Repareren van regressie bij het afhandelen van weergaveblokken">
<correction tzdata "Updaten van DST-regels voor Samoa en Jordan; bevestiging dat er geen schrikkelseconde nodig is op 31-12-2021">
<correction ublock-origin "Nieuwe bovenstroomse stabiele release; oplossen van een denial-of-serviceprobleem [CVE-2021-36773]">
<correction ulfius "Ervoor zorgen dat geheugen geïnitialiseerd wordt vóór gebruik [CVE-2021-40540]">
</table>


<h2>Beveiligingsupdates</h2>


<p>Deze revisie voegt de volgende beveiligingsupdates toe aan de stabiele
release. Het beveiligingsteam heeft voor elk van deze updates al een advies
uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2021 4959 thunderbird>
<dsa 2021 4960 haproxy>
<dsa 2021 4961 tor>
<dsa 2021 4962 ledgersmb>
<dsa 2021 4963 openssl>
<dsa 2021 4964 grilo>
<dsa 2021 4965 libssh>
<dsa 2021 4966 gpac>
<dsa 2021 4967 squashfs-tools>
<dsa 2021 4968 haproxy>
<dsa 2021 4969 firefox-esr>
<dsa 2021 4970 postorius>
<dsa 2021 4971 ntfs-3g>
<dsa 2021 4972 ghostscript>
<dsa 2021 4973 thunderbird>
<dsa 2021 4974 nextcloud-desktop>
<dsa 2021 4975 webkit2gtk>
<dsa 2021 4976 wpewebkit>
<dsa 2021 4977 xen>
<dsa 2021 4978 linux-signed-amd64>
<dsa 2021 4978 linux-signed-arm64>
<dsa 2021 4978 linux-signed-i386>
<dsa 2021 4978 linux>
<dsa 2021 4979 mediawiki>
</table>

<p>
Tijdens de laatste fase van de bullseye-freeze werden enkele updates
vrijgegeven via het <a href="https://security.debian.org/">security-archief</a>,
maar zonder een begeleidend DSA. Deze updates worden hieronder beschreven.
</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction apache2 "Oplossing voor mod_proxy HTTP2-verzoekregelinjectie [CVE-2021-33193]">
<correction btrbk "Oplossen van een probleem met willekeurige code-uitvoering [CVE-2021-38173]">
<correction c-ares "Reparatie voor het ontbreken van invoervalidatie op computernamen die worden geretourneerd door DNS-servers [CVE-2021-3672]">
<correction exiv2 "Repareren van overloopproblemen [CVE-2021-29457 CVE-2021-31292]">
<correction firefox-esr "Nieuwe stabiele bovenstroomse release [CVE-2021-29980 CVE-2021-29984 CVE-2021-29985 CVE-2021-29986 CVE-2021-29988 CVE-2021-29989]">
<correction libencode-perl "Encode: @INC-vervuiling beperken bij het laden van ConfigLocal [CVE-2021-36770]">
<correction libspf2 "spf_compile.c: correcte grootte van ds_avail [CVE-2021-20314]; reparatie voor de <q>reverse</q> macro-modificeerder">
<correction lynx "Oplossing voor het lekken van identificatiegegevens als SNI werd gebruikt samen met een URL die identificatiegegevens bevat [CVE-2021-38165]">
<correction nodejs "Nieuwe bovenstroomse stabiele release; oplossing voor een probleem van gebruik na vrijgave [CVE-2021-22930]">
<correction tomcat9 "Oplossing voor een probleem met het omzeilen van de authenticatie [CVE-2021-33037]">
<correction xmlgraphics-commons "Oplossen van een probleem met vervalsing van Server-Side Request [CVE-2020-11988]">
</table>


<h2>Het Debian-installatieprogramma</h2>
<p>Het installatieprogramma werd bijgewerkt om de reparaties die met deze tussenrelease in de stabiele release opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een vereniging van ontwikkelaars van vrije software
die vrijwillig tijd en moeite steken in het produceren van het volledig vrije
besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
distributie op &lt;debian-release@lists.debian.org&gt;.</p>
