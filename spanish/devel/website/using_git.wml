#use wml::debian::template title="Uso de git para manipular el código fuente del sitio web"
#use wml::debian::translation-check translation="74a4415fa07f5810685f1d0568bf4b695cc931b3"

<h2>Introducción</h2>

<p>Git es un <a href="https://en.wikipedia.org/wiki/Version_control">sistema de
control de versiones</a> que ayuda a gestionar el hecho de que varias personas trabajen
simultáneamente en el mismo material. Cada usuario puede tener una copia local del repositorio
principal. Las copias locales pueden estar en la misma máquina o repartidas por todo el mundo.
Los usuarios pueden modificar la copia local como deseen y, cuando el material
modificado está listo, confirmar los cambios y enviarlos al repositorio
principal.</p>

<p>Git no le permite enviar directamente los cambios si el repositorio remoto contiene
alguna modificación en la misma rama hecha con posterioridad al momento en que usted hizo su copia local.
Cuando se encuentre un conflicto de este tipo, actualice su copia local a partir del repositorio
remoto y reorganícela (<code>rebase</code>) aplicando sus cambios a partir del último cambio
confirmado en el repositorio remoto.
</p>

<h3><a name="write-access">Acceso de escritura al repositorio Git</a></h3>

<p>
El código fuente del sitio web Debian se gestiona, en su totalidad, con Git. Se encuentra
en <url https://salsa.debian.org/webmaster-team/webwml/>. Por omisión,
no se permite que los visitantes envíen cambios al repositorio del código fuente.
Necesitará algún tipo de permiso para tener acceso de escritura al
repositorio.
</p>

<h4><a name="write-access-unlimited">Acceso ilimitado de escritura</a></h4>
<p>
Si necesita acceso ilimitado de escritura al repositorio (por ejemplo, si va a
contribuir con frecuencia), considere solicitar acceso de escritura a través de
la interfaz web <url https://salsa.debian.org/webmaster-team/webwml/> tras
identificarse en la plataforma Salsa de Debian.
</p>

<p>
Si no ha participado antes en el desarrollo del sitio web de Debian,
envíe también un correo electrónico a <a href="mailto:debian-www@lists.debian.org">
debian-www@lists.debian.org</a> presentándose antes de solicitar
acceso ilimitado de escritura. Proporcione algo útil en su
presentación, como en qué idioma o en qué parte del sitio web tiene pensado
trabajar y quién respondería por usted.
</p>

<h4><a name="write-access-via-merge-request">Escribir en el repositorio mediante solicitudes de fusión («Merge Requests»)</a></h4>
<p>
Si no desea obtener acceso ilimitado de escritura al repositorio o
no puede hacerlo, siempre puede realizar una solicitud de fusión para que otros
desarrolladores revisen y acepten su trabajo. Por favor, haga las solicitudes de fusión
siguiendo el procedimiento estándar proporcionado por la plataforma GitLab de Salsa a través de
su interfaz web (consulte
<a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Project forking workflow [«Flujo de trabajo de bifurcaciones de proyectos»]</a>
y
<a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">When you work in a fork [«Cuando trabaja en una bifurcación»]</a>
para los detalles).
</p>

<p>
No todos los desarrolladores del sitio web monitorizan las solicitudes de fusión, por lo que
es posible que estas no sean siempre atendidas con prontitud. Si no tiene la seguridad de que su contribución
vaya a ser aceptada, envíe un correo electrónico a la
lista de correo <a href="https://lists.debian.org/debian-www/">debian-www</a>
y solicite una revisión.
</p>

<h2><a name="work-on-repository">Trabajar con el repositorio</a></h2>

<h3><a name="get-local-repo-copy">Obtención de una copia local del repositorio</a></h3>

<p>Primero, necesita instalar git para trabajar con el repositorio. A continuación,
configure los detalles de su usuario y de su dirección de correo electrónico en su computadora (consulte
la documentación general de git para saber cómo hacerlo). Después, puede
clonar el repositorio (es decir, hacer una copia local del mismo)
de una de las dos maneras que describimos a continuación.</p>

<p>La forma recomendada de trabajar en webwml es registrando primero una
cuenta en salsa.debian.org y habilitando el acceso git SSH, para lo que necesita subir una
clave pública SSH a su cuenta. Vea las <a
href="https://salsa.debian.org/help/ssh/README.md">páginas de ayuda
de Salsa</a> para más detalles sobre cómo hacer esto. Después, puede clonar el
repositorio de webwml con la orden:</p>

<pre>
   git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>

<p>Si no tiene una cuenta en Salsa, un método alternativo es
clonar el repositorio utilizando el protocolo HTTPS:</p>

<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>

<p>Utilizando este método obtendrá la misma copia local, pero no
podrá enviar modificaciones directamente al repositorio.</p>

<p>El clonado de todo el repositorio de webwml conlleva la descarga de unos
500MB de datos, lo que puede ser difícil para quien disponga de una conexión
a Internet lenta o inestable. Si es este su caso, puede probar el clonado superficial con
una profundidad mínima al principio para que la descarga inicial sea menor:</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Tras obtener un repositorio (superficial) utilizable, puede ir aumentando la profundidad de
la copia y, finalmente, convertirla en un repositorio local
completo:</p>

<pre>
  git fetch --deepen=1000 # aumenta la profundidad del repo con otros 1000 commits
  git fetch --unshallow   # descarga todos los commits que faltan, convierte el repo en un repo completo
</pre>

<h4><a name="partial-content-checkout">Contenido parcial en la copia de trabajo</a></h4>

<p>Puede crear una copia de trabajo que contenga solo un subconjunto de las páginas de la siguiente manera:</p>

<pre>
   $ git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git
   $ cd webwml
   $ git config core.sparseCheckout true
   Cree el fichero .git/info/sparse-checkout en webwml con un contenido como el siguiente
   (si solo quiere los ficheros base, las páginas en inglés y las traducciones a catalán y español):
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
   Después:
   $ git checkout --
</pre>

<h3><a name="submit-changes">Envío de cambios locales</a></h3>

<h4><a name="keep-local-repo-up-to-date">Mantener actualizado su repo local</a></h4>

<p>Cada pocos días (y, desde luego, ¡antes de empezar a modificar algo!)
debería ejecutar:</p>

<pre>
   git pull
</pre>

<p>para descargar aquellos ficheros del repositorio que hayan cambiado.</p>

<p>
Le recomendamos encarecidamente que tenga su directorio de trabajo git local limpio
antes de ejecutar "git pull" y de realizar el trabajo de edición subsiguiente. Si tiene
cambios sin confirmar o cambios confirmados localmente en la rama actual pero no incluidos
en el repositorio remoto, la ejecución de "git pull" creará automáticamente
commits de fusión o incluso fallará debido a conflictos. Considere mantener su
trabajo en curso en otra rama o utilizar órdenes como "git stash".
</p>

<p>Nota: git es un sistema de control de versiones distribuido (no
centralizado). Esto significa que, cuando confirma cambios, estos cambios solo se
almacenan en su repositorio local. Para compartirlos con otros, además,
tiene que enviar sus cambios al repositorio central alojado en Salsa.</p>

<h4><a name="example-edit-english-file">Ejemplo de edición de ficheros en inglés</a></h4>

<p>
A continuación proporcionamos un ejemplo de cómo editar ficheros en inglés en el repositorio
del código fuente del sitio web. Tras obtener una copia local del repo
utilizando "git clone", y antes de empezar el trabajo de edición, ejecute la orden
siguiente:
</p>

<pre>
   $ git pull
</pre>

<p>Ahora haga las modificaciones en los ficheros. Cuando haya terminado, confirme los cambios
en su repositorio local con:</p>

<pre>
   $ git add ruta/al(os)/fichero(s)
   $ git commit -m "Su mensaje para este commit"
</pre>

<p>Si tiene acceso ilimitado de escritura al repositorio remoto webwml, ahora
puede enviar directamente los cambios al repo de Salsa:</p>

<pre>
   $ git push
</pre>

<p>Si no tiene acceso directo de escritura al repositorio webwml,
considere enviar los cambios a través de la función de solicitud de fusión («Merge Request») proporcionada
por la plataforma GitLab de Salsa o pedir ayuda a otros desarrolladores.
</p>

<p>Este es un resumen muy básico de cómo usar git para manipular el
código fuente del sitio web de Debian. Para más información, consulte
la documentación de git.</p>

<h4><a name="closing-debian-bug-in-git-commits">Cierre de bugs de Debian en commits git</a></h4>

<p>
Si incluye el texto <code>Closes: #</code><var>nnnnnn</var> en la entrada del historial de confirmaciones correspondiente a
su commit, el bug número <code>#</code><var>nnnnnn</var> se cerrará
cuando envíe los cambios. El formato preciso de esto es el mismo que
<a href="$(DOC)/debian-policy/ch-source.html#id24">en las normas de Debian</a>.</p>

<h4><a name="links-using-http-https">Uso de HTTP/HTTPS en los enlaces</a></h4>

<p>Muchos sitos web de Debian soportan SSL/TLS. Use enlaces HTTPS cuando sea
posible y razonable. <strong>Sin embargo</strong>, algunos
sitios web de Debian/DebConf/SPI/etc, o bien no tienen soporte HTTPS,
o bien utilizan solo la autoridad certificadora de SPI (y no una autoridad certificadora SSL en la que confían todos los navegadores). Para
evitar que los usuarios no pertenecientes a Debian obtengan mensajes de error, no incluya enlaces
a esos sitios utilizando HTTPS.</p>

<p>El repositorio git rechazará tanto los commits que contengan enlaces HTTP
a los sitios web de Debian que soportan HTTPS como los que contengan enlaces HTTPS a
los sitios web de Debian/DebConf/SPI que se sabe que, o bien no soportan
HTTPS, o bien usan certificados firmados únicamente por SPI.</p>

<h3><a name="translation-work">Trabajo con traducciones</a></h3>

<p>Las traducciones deberían mantenerse siempre actualizadas con relación al
fichero en inglés correspondiente. La cabecera "translation-check" en los ficheros
traducidos se usa para indicar en qué versión del fichero en inglés está basado
el fichero traducido actual. Si modifica ficheros traducidos, tiene que actualizar la
cabecera translation-check para que coincida con el hash del commit git de la
modificación correspondiente en el fichero en inglés. Puede obtener dicho hash
con:</p>

<pre>
$ git log ruta/al/fichero/en/inglés
</pre>

<p>Si hace una traducción nueva de un fichero, utilice el script <q>copypage.pl</q>.
Este script creará una plantilla para su idioma, incluyendo la cabecera
translation correcta.</p>

<h4><a name="translation-smart-change">Modificación de traducciones con smart_change.pl</a></h4>

<p><code>smart_change.pl</code> es un script diseñado para facilitar
la actualización, de forma conjunta, de ficheros originales y de sus traducciones. Hay
dos maneras de usarlo, dependiendo de los cambios que esté haciendo.</p>

<p>Para utilizar <code>smart_change</code> con el único fin de actualizar las cabeceras
translation-check cuando está trabajando manualmente en los ficheros:</p>

<ol>
  <li>Haga los cambios al fichero o ficheros originales, y confírmelos.</li>
  <li>Actualice las traducciones.</li>
  <li>Ejecute smart_change.pl. El script recogerá los cambios y actualizará
    las cabeceras de los ficheros traducidos.</li>
  <li>Revise los cambios (por ejemplo, con «git diff»).</li>
  <li>Confirme los cambios de las traducciones.</li>
</ol>

<p>O para utilizar smart_change con una expresión regular para hacer
varios cambios en ficheros en una única pasada:</p>

<ol>
  <li>Ejecute <code>smart_change.pl -s s/FOO/BAR/ fich-orig-1 fich-orig-2 ...</code></li>
  <li>Revise los cambios (por ejemplo, con <code>git diff</code>).
  <li>Confirme los cambios en el fichero o ficheros originales.</li>
  <li>Ejecute <code>smart_change.pl fich-orig-1 fich-orig-2</code>
    (esta vez <strong>sin la expresión regular</strong>). Ahora
    solo actualizará las cabeceras de los ficheros traducidos.</li>
  <li>Finalmente, confirme los cambios de las traducciones.</li>
</ol>

<p>Este procedimiento es más laborioso que el anterior (necesita dos confirmaciones), pero
resulta inevitable debido a la manera en que funcionan los hashes de los commits en git.</p>

<h2><a name="notifications">Obtener notificaciones</a></h2>

<h3><a name="commit-notifications">Recibir notificación de las confirmaciones</a></h3>

<p>Hemos configurado el proyecto webwml en Salsa de forma que las confirmaciones se
muestren en el canal IRC #debian-www.</p>

<p>Si quiere recibir notificaciones por correo electrónico cuando se hagan
confirmaciones en el repo de webwml, suscríbase al pseudopaquete
<q>www.debian.org</q> por medio de tracker.debian.org y active allí la palabra clave
<q>vcs</q> siguiendo estos pasos (solo una vez):</p>

<ul>
  <li>Abra un navegador web y vaya a <url https://tracker.debian.org/pkg/www.debian.org></li>
  <li>Suscríbase al pseudopaquete <q>www.debian.org</q>. (Puede identificarse
      mediante SSO o registrar una dirección de correo electrónico y contraseña, si no estaba usando
      ya tracker.debian.org para otros fines).</li>
  <li>Vaya a <url https://tracker.debian.org/accounts/subscriptions/>, después a <q>modify
      keywords</q>, marque <q>vcs</q> (si no está ya marcado) y grabe los cambios.</li>
  <li>A partir de este momento recibirá correos electrónicos cuando alguien confirme cambios en el
      repo de webwml. Pronto añadiremos los demás repositorios del equipo webmaster-team.</li>
</ul>

<h3><a name="merge-request-notifications">Recibir notificación de las solicitudes de fusión («Merge Request»)</a></h3>

<p>
Si desea recibir un correo electrónico de notificación cada vez que se realice una nueva
solicitud de fusión en el repositorio webwml a través de la interfaz web de la plataforma GitLab de
Salsa, puede modificar la configuración de notificaciones para el repositorio
webwml en la interfaz web siguiendo estos pasos:
</p>

<ul>
  <li>Identifíquese en Salsa y vaya a la página del proyecto.</li>
  <li>Haga click en el icono con la campana en la parte superior de la página inicial del proyecto.</li>
  <li>Seleccione el nivel de notificación que prefiera.</li>
</ul>
