#use wml::debian::template title="Debian accesibilidad - Software"
#use wml::debian::translation-check translation="82f3d1721149ec88f5974028ed2c2d8d454b4071" maintainer="Laura Arjona Reina"
{#style#:<link rel="stylesheet" href="style.css" type="text/css" />:#style#}

<define-tag a11y-pkg endtag=required>
<preserve name tag url/>
<set-var %attributes>
<h3><if "<get-var url>"
        <a href="<get-var url>" name="<get-var tag>"><get-var name></a>
      <a href="https://packages.debian.org/<get-var tag>" name="<get-var tag>"><get-var name></a>></h3>
  %body
<restore name tag url/>
</define-tag>

<h2><a id="speech-synthesis" name="speech-synthesis">Síntesis de voz y APIs relacionadas</a></h2>

<p>
  Hay disponible una lista completa en la
  <a href="https://blends.debian.org/accessibility/tasks/speechsynthesis">página de la tarea «speechsynthesis»</a>
</p>

<a11y-pkg name="EFlite" tag=eflite url="http://eflite.sourceforge.net/">
<p>
  Un servidor de voz para <a href="#emacspeak">«Emacspeak»</a> y
  <a href="#yasr">«yasr»</a> (u otro lector de pantalla) que les 
  sirve de interfaz con <a href="#flite">«Festival Lite»</a>, un motor libre texto a voz
  desarrollado en el Centro de Voz de la CMU como una ramificación de
  <a href="#festival">«Festival»</a>.
</p>
<p>
  Debido a limitaciones inherentes de su programa base, por el momento «EFlite» solo proporciona
  soporte para la lengua inglesa.
</p>
</a11y-pkg>
<a11y-pkg name="eSpeak" tag=espeak>
<p>
eSpeak/eSpeak-NG es un sintetizador de voz por software para inglés, y algunos otros idiomas.
</p>
<p>
eSpeak produce voz en inglés de buena calidad. Utiliza un método de síntesis de 
voz diferente del de otros motores texto a voz de fuente abierta (síntesis de 
voz no concatenativa, por lo que también deja una huella muy pequeña), y suena 
bastante diferente. Quizás no es tan natural o <q>suave</q>, pero algunos encuentran
la articulación más clara y fácil de escuchar para largos períodos de tiempo.
</p>
<p>
Se puede usar como programa de consola para que lea texto de un archivo o de la 
entrada estándar. También trabaja bien como <q>hablador</q> con el sistema de 
texto a voz de KDE (KTTS), como alternativa a <a href="#festival">Festival</a>
por ejemplo. Como tal, puede leer texto que se haya seleccionado en el 
escritorio, o directamente desde el navegador Konqueror o el editor Kate.
</p>
  <ul>
    <li>Incluye diferentes voces, cuyas características se pueden alterar.</li>
    <li>Puede producir salida como archivo WAV.</li>
    <li>Puede traducir texto a códigos de fonemas, así que se puede adaptar como
    interfaz para otros motores de síntesis de voz.</li>
    <li>Potencial para otros idiomas. Se incluyen intentos rudimentarios (y 
    probablemente graciosos) de soporte de alemán y esperanto.</li>
    <li>Tamaño compacto. El programa y sus datos locales son en total unos 350Kbytes.</li>
    <li>Escrito en C++.</li>
  </ul>
<p>
eSpeak también se puede usar con <a href="#speech-dispatcher">Speech Dispatcher</a>.
</p>
</a11y-pkg>
<a11y-pkg name="Festival Lite" tag=flite>
<p>
  Un pequeño motor de síntesis de voz rápido en tiempo de ejecución. Es la última
  incorporación al conjunto de herramientas de síntesis de software libre incluyendo
  el sistema de síntesis de voz de la Universidad de Edinburgo y
  el proyecto «FestVox» de la Universidad Carnegie Mellon, herramientas, scripts y 
  documentación para construir voces sintéticas. Sin embargo, «flite» por sí mismo
  no necesita ninguno de estos sistemas para funcionar.
</p>
<p>
  Actualmente sólo da soporte a la lengua inglesa.
</p>
</a11y-pkg>
<a11y-pkg name="Festival" tag="festival"
          url="http://www.cstr.ed.ac.uk/projects/festival/">
<p>
  Un sistema de síntesis de voz plurilingüe desarrollado
  en el <a href="http://www.cstr.ed.ac.uk/">CSTR</a> «[<i>C</i>entre for
  <i>S</i>peech <i>T</i>echnology <i>R</i>esearch]» (Centro para la investigación
  de tecnología del habla) de la
  <a href="http://www.ed.ac.uk/text.html">Universidad de Edinburgo</a>.
</p>
<p>
  Festival ofrece un sistema texto a voz completo con varias APIs, así como
  un entorno de desarrollo e investigación de técnicas de síntesis de voz.
  Está escrito en C++ con un intérprete de órdenes basado en Scheme 
  para el control general.
</p>
<p>
  Además de la investigación en la síntesis de voz, «festival» es útil como un 
  programa de síntesis de voz autónomo. Es capaz de producir voz claramente
  comprensible a partir de texto.
</p>
</a11y-pkg>
<a11y-pkg name="Speech Dispatcher" tag="speech-dispatcher"
          url="http://www.freebsoft.org/speechd">
<p>
  Proporciona un capa independiente de dispositivos para la síntesis de voz.
  Da Soporte varios sintetizadores de voz por «software» y «hardware» como
  base y proporciona una capa genérica para la síntesis de voz y
  reproducir datos PCM a las aplicaciones a través de aquellos programas base distintos.
</p>
<p>
  Varios conceptos de alto nivel, como encolar frente a interrumpir la voz
  y configuraciones de usuario de aplicaciones específicas, se implementan en 
  un dispositivo de forma independiente. Esto es por aquello de liberar al programador de
  la aplicación de tener que reinventar la rueda.
</p>
</a11y-pkg>

<h2><A name="i18nspeech">Síntesis de voz internacionalizada</a></h2>
<p>
Todas las soluciones libres para «software» basadas en la síntesis de voz 
disponibles actualmente parecen compartir una deficiencia común: La mayoría
están principalmente limitadas al inglés, proporcionando sólo un soporte muy
marginal para otras lenguas o ,en lo mayoría de los casos, ninguno.
Entre todos los sintetizadores de voz de software libre para Linux, sólo
«Festival» de la CMU da soporte a más de una lengua natural. «Festival» de la CMU puede 
sintetizar inglés, español y galés. No se da soporte al alemán. No se da soporte al
francés. No se da soporte al ruso. Cuando la internacionalización y localización 
son las tendencias en el software y los servicios «web», ¿es razonable pedir
a la gente ciega interesada en Linux que aprendan inglés sólo para entender
lo que dice su ordenador y llevar toda su correspondencia en una lengua 
extranjera?
</p>
<p>
Desafortunadamente, la síntesis de voz no es realmente el proyecto doméstico 
favorito de Jane Hacker. Crear un «software» sintetizador de 
voz inteligible conlleva tareas que consumen mucho tiempo. La síntesis de voz
concatenativa requiere la cuidadosa creación de una base de datos de fonemas
que contengan todas las posibles combinaciones de sonidos de la lengua objetivo.
También se necesitan desarrollar con sumo cuidado las reglas que determinan las 
transformación de la representación en texto a fonemas individuales , y
normalmente requiere la división del flujo de caracteres en grupos lógicos, como
oraciones, frases y palabras. Tal análisis léxico requiere un léxico específico
de la lengua raramente publicable bajo una licencia libre.
</p>
<p>
Una de las mayores promesas en los sistemas de síntesis de voz es Mbrola, con
bases de datos de fonemas para más de una docena de lenguajes diferentes. La
síntesis en sí misma es software libre. Desafortunadamente, las bases de datos de
fonemas son solo para uso no comercial y no militar. Carecemos de bases de datos de fonemas 
libres para ser usadas en el sistema operativo Debian.
</p>
<p>
Sin un «software» sintetizador de voz ampliamente plurilingüe, Linux
no puede ser aceptado por proveedores de tecnología de asistencia y gente
con discapacidades visuales. ¿Qué podemos hacer para mejorar esto?
</p>
<p>
Básicamente hay dos métodos posibles:
</p>
<ol>
<li>Organizar un grupo de gente deseoso de ayudar en este asunto, e
intentar activamente mejorar la situación. Esto puede ser un poco complicado,
ya que se necesita un montón de conocimiento específico sobre síntesis de voz,
lo que no es que sea fácil si se hace de forma autodidacta. Sin embargo, esto no 
debería descorazonarle. Si piensa que puede motivar un grupo de gente lo 
suficientemente grande para alcanzar algunas mejoras, valdría la pena
hacerlo.</li>
<li>Obtener fondos y contratar a algún instituto que ya tenga el conocimiento de 
cómo crear las bases de datos de fonemas necesarios, léxico y reglas de 
transformación. Este método tiene la ventaja de que tiene mayores probabilidades
de generar resultados de calidad, y también debería alcanzar algunas mejoras 
mucho antes que el primer método. Por supuesto, la licencia bajo la que se 
publicaría todo el trabajo resultante se debería acordar por adelantado, y 
debería pasar los requerimientos de las DFSG. La solución ideal sería, por 
supuesto, convencer a alguna universidad para sufragar tal proyecto con sus 
propios fondos, y contribuir con los resultados a la comunidad del software libre.</li>
</ol>
<p>
Por último pero no menos importante, parece que la mayoría de los productos de síntesis 
comerciales de éxito actuales ya no usan síntesis de voz concatenativa, principalmente 
porque la base de datos de sonidos consume un montón de espacio en disco.
Esto no es muy deseable para productos pequeños empotrados, como por ejemplo voz en un
teléfono móvil. El software libre publicado recientemente como <a href="#espeak">eSpeak</a>
parece intentar este enfoque, que puede merecer mucho la pena.
</p>
<h2><a id="emacs" name="emacs">Extensiones de revisión de pantalla de Emacs</a></h2>
<a11y-pkg name="Emacspeak" tag="emacspeak"
          url="http://emacspeak.sourceforge.net/">
<p>
  Un sistema de salida por voz que permitirá a cualquiera que no pueda ver 
  trabajar directamente en un sistema UNIX. Una vez inicie Emacs con
  «Emacspeak» cargado, obtendrá respuesta hablada para todo lo que haga. Su 
  rendimiento variará dependiendo de cómo de bien sepa usar Emacs. No hay nada que
  no pueda hacer en Emacs :-). Este paquete incluye servidores de voz escritos 
  en tcl para soportar los sintetizadores de voz DECtalk Express y DECtalk 
  MultiVoice. Para otros sintetizadores, busque paquetes separados de 
  servidores de voz como «Emacspeak-ss» o <a href="#eflite">«eflite»</a>.
</p>
</a11y-pkg>
<a11y-pkg name="speechd-el" tag="speechd-el"
          url="http://www.freebsoft.org/speechd-el">
<p>
  Cliente de Emacs para sintetizadores de voz, dispositivos de Braille 
  y otros interfaces de salida alternativos. Proporciona un entorno con
  salida completamente hablada o en Braille. Está dirigido principalmente
  a usuarios con impedimentos visuales que necesitan comunicaciones no
  visuales con Emacs, pero lo puede usar cualquiera que necesite una salida
  de voz sofisticada u otro tipo de salida alternativa para Emacs.
</p>
</a11y-pkg>

<h2><a id="console" name="console">Lectores de consola (modo-texto)</a></h2>

<p>
  Hay disponible una lista completa en la
  <a href="https://blends.debian.org/accessibility/tasks/console">página de la tarea «console screen readers»</a>
</p>

<a11y-pkg name="BRLTTY" tag="brltty" url="https://brltty.app/">
<p>
  Un demonio que proporciona acceso a la consola de Linux para una persona 
  ciega usando un «software» para dispositivos de Braille.
  Maneja el terminal Braille y proporciona funcionalidad para la revisión de la 
  pantalla completa.
</p>
<p>
  Los dispositivos Braille soportados por BRLTTY están listados en la
  <a href="https://brltty.app/doc/KeyBindings/#braille-device-bindings">
  documentación de dispositivos de BRLTTY</a>
</p>
<p>
  BRLTTY también proporciona una infraestructura basada en cliente/servidor
  para aplicaciones que deseen utilizar un dispositivo de mostrado en Braille.
  El demonio escucha conexiones TCP/IP entrantes en un cierto puerto.
  En el paquete
  <a href="https://packages.debian.org/libbrlapi">libbrlapi</a> se proporciona
  una biblioteca de objetos compartidos para clientes. 
  En el paquete 
  <a href="https://packages.debian.org/libbrlapi-dev">libbrlapi-dev</a> se proporciona
  una biblioteca estática, archivos de encabezado y documentación.
  Esta funcionalidad la usa por ejemplo <a href="#gnome-orca">Orca</a>
  para proporcionar soporte a tipos de pantallas que aún no 
  soporta Gnopernicus directamente.
</p>
</a11y-pkg>
<a11y-pkg name="Yasr" tag="yasr" url="http://yasr.sourceforge.net/">
<p>
  Un lector de pantalla de consola de propósito general para GNU/Linux y
  otros sistemas operativos tipo Unix. El nombre «yasr» es un acrónimo que
  se puede interpretar tanto como <q>Yet Another Screen Reader</q> (Otro 
  lector de consola más) o <q>Your All-purpose Screen Reader</q> (Su lector de 
  consola para cualquier propósito).
</p>
<p>
  En la actualidad, «yasr» intenta dar soporte a los sintetizadores por «hardware»
  Speak-out, DEC-talk, BNS, Apollo y DoubleTalk. También puede comunicarse
  con servidores de voz <q>Emacspeak</q> y así ser usado con sintetizadores que no
  soporte directamente, como <a href="#flite"><q>Festival Lite</q></a> (a través 
  de <a href="#eflite">«eflite»</a>) o <q>FreeTTS</q>.
</p>
<p>
  «Yasr» funciona abriendo un pseudoterminal y lanzando una consola, interceptando
  todos los datos que entran y salen. Mira las secuencias de escape que se 
  envían y mantiene una <q>ventana</q> virtual que contiene lo que cree que está en 
  la pantalla. De esta manera no usa ninguna característica específica de Linux 
  y se puede migrar a otros sistemas operativos tipo UNIX sin demasiados problemas.
</p>
</a11y-pkg>


<h2><a id="gui" name="gui">Interfaces gráficas de usuario</a></h2>
<p>
La accesibilidad de las interfaces gráficas de usuarios sólo ha recibido un 
giro significativo recientemente con los variados trabajos de desarrollo
en el <a href="http://www.gnome.org/">Escritorio GNOME</a>, especialmente el
<a href="https://wiki.gnome.org/Accessibility">proyecto de accesibilidad de GNOME</a>.
</p>


<h2><a id="gnome" name="gnome">«Software» de accesibilidad en GNOME</a></h2>

<p>
  Hay disponible una lista completa en la
  <a href="https://blends.debian.org/accessibility/tasks/gnome">página de la tarea «Gnome accessibility»</a>
</p>

<a11y-pkg name="Assistive Technology Service Provider Interface" tag="at-spi">
<p>
  Este paquete contiene los componentes centrales de accesibilidad de GNOME.
  Permite a los distribuidores de tecnología de asistencia, como lectores de 
  pantalla, pedir a todas las aplicaciones que se estén ejecutando en el 
  escritorio información relativa a accesibilidad, a la vez que proporciona 
  mecanismos de enlace para soportar otros conjuntos de herramientas 
  distintos de GTK.
</p>
<p>
  Los conectores para el lenguaje Python se proporcionan en el paquete
  <a href="https://packages.debian.org/python-at-spi">python-at-spi</a>.
</p>
</a11y-pkg>
<a11y-pkg name="The ATK accessibility toolkit" tag="atk">
<p>
  ATK es un conjunto de herramientas que proporcionan interfaces de accesibilidad
  para aplicaciones u otros conjuntos de herramientas. Implementando estas 
  interfaces, esos otros conjuntos de herramientas o aplicaciones se pueden usar 
  con herramientas como lectores de pantalla, ampliadores, y otros 
  dispositivos de entrada alternativos.
</p>
<p>
  La parte que se ejecuta de ATK, que se necesita para ejecutar aplicaciones
  construidas con ello, está disponible en el paquete 
  <a href="https://packages.debian.org/libatk1.0-0">libatk1.0-0</a>.
  Los archivos de desarrollo de ATK, que se necesita para la compilación de
  programas o conjuntos de herramientas que que lo usen, se proporcionan en el paquete
 <a href="https://packages.debian.org/libatk1.0-dev">libatk1.0-dev</a>.
  Los conectores para el lenguaje Ruby se proporcionan en el paquete
  <a href="https://packages.debian.org/ruby-atk">ruby-atk</a>.
</p>
</a11y-pkg>
<a11y-pkg name="gnome-accessibility-themes" tag="gnome-accessibility-themes">
<p>
  El paquete gnome-accessibility-themes (temas de accesibilidad de GNOME) 
  contiene algunos temas de alta accesibilidad para el escritorio GNOME, 
  diseñados para impedidos visuales.
</p>
<p>
  Se proporcionan un total de 7 temas, proporcionando combinaciones de 
  contraste alto, bajo e invertido, así como texto e iconos grandes.
</p>
</a11y-pkg>
<a11y-pkg name="gnome-orca" tag="gnome-orca"
          url="http://live.gnome.org/Orca">
<p>
  Orca es un lector de consola flexible y extensible
  que proporciona acceso a escritorios gráficos usando combinaciones de
  voz, braille y/o magnificador definidos por el usuario. Desarrollado
   desde 2004 por Sun Microsystems, Inc., a través de la <q>Accessibility Program
  Office</q> (oficina del programa de accesibilidad), Orca se ha creado con
  con aportaciones de sus usuarios finales y continuando su compromiso con ellos.
</p>
<p>
  Orca puede usar <a href="#speech-dispatcher">Speech Dispatcher</a> para la síntesis de
 voz. <a href="#brltty">BRLTTY</a> se usa por dar soporte a dispositivos braille
  (y para la integración suave con consola e interfaces gráficos en braille).
</p>
</a11y-pkg>
<h2><a id="kde" name="kde">Software de accessibilidad de KDE</a></h2>

<p>
  Hay disponible una lista completa en la
  <a href="https://blends.debian.org/accessibility/tasks/kde">página de la tarea «KDE accessibility»</a>
</p>

<a11y-pkg name="kmag" tag="kmag">
<p>
  Magnifica una parte de la pantalla igual que si usase una lupa para magnificar
  periódico bien impreso o una fotografía. Esta aplicación es útil para mucha 
  gente: desde investigadores a artistas, diseñadores web o personas con poca visión.
</p>
</a11y-pkg>
<h2><A id="input" name="input">Métodos de entrada de datos no estándar</a></h2>

<p>
  Hay disponible una lista completa en la
  <a href="https://blends.debian.org/accessibility/tasks/input">página de la tarea «Input methods»</a>
</p>

<a11y-pkg name="Dasher" tag="dasher" url="http://www.inference.phy.cam.ac.uk/dasher/">
<p>
  Dasher es un interfaz de entrada de texto eficiente en cuanto a información, manejado
  por acciones naturales de señalado continuas. Dasher es un sistema de entrada 
  competitivo donde quiera que sea que no se pueda usar un teclado completo, por ejemplo,
</p>
  <ul>
   <li>un ordenador de mano («palmtop»)</li>
   <li>un ordenador de viaje</li>
   <li>cuando utilice un ordenador con una mano, por ejemplo un mando de juego, pantalla táctil,
       mando por bola o ratón</li>
   <li>cuando utilice un ordenador sin manos (esto es, por ratón para la cabeza o rastreador del ojo).</li>
  </ul>
<p>
  La versión de seguimiento del ojo de Dasher permite a un usuario experimentado escribir texto tan 
  rápido como una escritura manual normal, 25 palabras por minuto. Usando un ratón,
  los usuarios experimentados pueden escribir a 39 palabras por minuto.
</p>
<p>
  Dasher utiliza un algoritmo de predicción de texto más avanzado que el T9(tm)
  que se usa a veces en los teléfonos móviles, haciéndolo sensible al contexto.
</p>
</a11y-pkg>
<a11y-pkg name="Caribou" tag="caribou" url="https://wiki.gnome.org/Projects/Caribou">
<p>
  Caribou es una tecnología asistiva de entrada pensada para usuarios de conmutador y puntero.
 Proporciona un teclado en pantalla configurable con modo de escaneo.
</p>
</a11y-pkg>
