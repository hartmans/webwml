#use wml::debian::template title="¡Gracias por descargar Debian!" 
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="1d8e9363ec81c198712c4ba45ff77dd67145b026" maintainer="Laura Arjona Reina"

{#meta#:
<meta http-equiv="refresh" content="3;url=<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">
:#meta#}

<p>Esto es Debian <:=substr '<current_initial_release>', 0, 2:>, codenamed <em><current_release_name></em>, netinst (instalación por red), para <: print $arches{'amd64'}; :>.</p>

<p>Si la descarga no comienza automáticamente, pulse <a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso</a>.</p>
<p>Descargar suma de comprobación: <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS">SHA512SUMS</a> <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS.sign">Firma</a></p>



<div class="tip">
	<p><strong>Importante</strong>: Asegúrese de <a href="$(HOME)/CD/verify">verificar la descarga con la suma de comprobación</a>.</p>
</div>

<p>Las imágenes ISO del instalador de Debian son híbridas, lo que significa que pueden escribirse directamente en CD/DVD/BD o en <a href="https://www.debian.org/CD/faq/#write-usb">dispositivos USB</a>.</p>

<h2 id="h2-1">
	Otros instaladores</h2>

<p>En <a href="$(HOME)/distrib/">Obtener Debian</a> se pueden encontrar otros instaladores e imágenes, como «sistemas vivos», 
instaladores fuera de línea para sistemas sin conexión a la red e instaladores para otras arquitecturas de CPU o instancias en la nube.</p>

<p>Desde <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">Imágenes no oficiales incluyendo paquetes de firmware no libre</a>
pueden descargarse instaladores no oficiales con <a href="https://wiki.debian.org/Firmware"><strong>firmware no libre</strong></a>, de ayuda para algunos adaptadores gráficos o de red.</p>

<h2 id="h2-2">Enlaces relacionados</h2>

<p><a href="$(HOME)/releases/<current_release_name>/installmanual">Guía de instalación</a></p>
<p><a href="$(HOME)/releases/<current_release_name>/releasenotes">Notas de publicación</a></p>
<p><a href="$(HOME)/CD/verify">Guía de verificación de las imágenes ISO</a></p>
<p><a href="$(HOME)/CD/http-ftp/#mirrors">Sitios alternativos de descarga</a></p>
<p><a href="$(HOME)/releases">Otras versiones</a></p>
