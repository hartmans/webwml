#use wml::debian::translation-check translation="edf273a4209c8afbee5b25c76291ca9c743d23e3"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto las siguientes vulnerabilidades en el motor web
webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1788">CVE-2021-1788</a>

    <p>Francisco Alonso descubrió que el procesado de contenido web preparado
    maliciosamente puede dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1844">CVE-2021-1844</a>

    <p>Clement Lecigne y Alison Huffman descubrieron que el procesado
    de contenido web preparado maliciosamente puede dar lugar a ejecución de código
    arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1871">CVE-2021-1871</a>

    <p>Un investigador anónimo descubrió que atacantes en ubicaciones remotas podían
    provocar ejecución de código arbitrario.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.32.1-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de webkit2gtk.</p>

<p>Para información detallada sobre el estado de seguridad de webkit2gtk, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4923.data"
