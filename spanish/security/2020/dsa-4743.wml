#use wml::debian::translation-check translation="ea4695cc27694f8fc80eb1e6329c9fd24548d384"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrió un defecto en ruby-kramdown, un conversor y analizador sintáctico
de Markdown rápido y escrito exclusivamente en ruby, que podía proporcionar acceso de lectura no deseado a
ficheros o dar lugar a ejecución de código Ruby embebido cuando se usaba
la extensión &#123;::options /&#125; junto con la opción <q>template</q>.</p>

<p>Esta actualización incluye una opción nueva, <q>forbidden_inline_options</q>, para
limitar las opciones permitidas con la extensión &#123;::options /&#125;. Por
omisión, la opción <q>template</q> está prohibida.</p>

<p>Para la distribución «estable» (buster), este problema se ha corregido en
la versión 1.17.0-1+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de ruby-kramdown.</p>

<p>Para información detallada sobre el estado de seguridad de ruby-kramdown, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/ruby-kramdown">\
https://security-tracker.debian.org/tracker/ruby-kramdown</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4743.data"
