# translation of others.po to Swedish
#
# Martin Ågren <martin.agren@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: others\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: (null)\n"
"PO-Revision-Date: 2020-04-11 14:09+0200\n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Ny-medlemshörnan"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Steg 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Steg 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Steg 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Steg 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Steg 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Steg 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Steg 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Checklista för Ansökande"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Se <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(endast tillgänglig på franska) för mer information."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Mer information"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Se <A href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"A> (endast tillgänglig på spanska) för mer information."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Telefon"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Fax"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Adress"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Produkter"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "t-tröjor"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "kepsar"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "klistermärken"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "muggar"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "andra sorters kläder"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "polotröjor"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "frisbeear"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "musmattor"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "knappar"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "basketkorgar"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "örhängen"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "resväskor"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "paraplyer"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "örngott"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "nyckelringar"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "Schweiziska arméknivar"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "USB-minnen"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "nyckelband"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "övrigt"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr "Tillgängliga språk:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr "Internationell leverans:"

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr "inom Europa"

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr "Ursprungsland:"

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "Donerar pengar till Debian"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr "Pengar används för att organisera lokala fri mjukvaruevenemang"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Med&nbsp;\"Debian\""

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Utan&nbsp;\"Debian\""

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Inkapslad PostScript"

# TODO: Den här går inte att översätta enkelt
#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Powered by Debian]"

# TODO: Den här går inte att översätta enkelt
#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Powered by Debian GNU/Linux]"

# TODO: Den här går inte att översätta enkelt
#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Debian powered]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (liten knapp)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "samma som ovan"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Hur länge har du använt Debian?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Är du en Debianutvecklare (DD)?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "Vilka delar av Debian är du delaktig i?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Vad fick dig intresserad av att arbeta med Debian?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"Har du några tips för kvinnor som är intresserade av att bli mer delaktiga i "
"Debian?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr "Känner du några andra kvinnor i teknologigruppen? Vilka?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Lite mer om dig själv..."

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "Okänd"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "ALL"
#~ msgstr "ALLA"

#~ msgid "Architecture:"
#~ msgstr "Arkitektur:"

#~ msgid "BAD"
#~ msgstr "Ej OK"

#~ msgid "BAD?"
#~ msgstr "Ej OK?"

#~ msgid "Booting"
#~ msgstr "Startar"

#~ msgid "Building"
#~ msgstr "Bygger"

#~ msgid "Download"
#~ msgstr "Hämta"

#~ msgid "Last update"
#~ msgstr "Senaste uppdatering"

#~ msgid "No images"
#~ msgstr "Inga avbildningar"

#~ msgid "No kernel"
#~ msgstr "Ingen kärna"

#~ msgid "Not yet"
#~ msgstr "Inte ännu"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "OK?"
#~ msgstr "OK?"

#~ msgid "Old banner ads"
#~ msgstr "Gamla webbannonser"

#~ msgid "Package"
#~ msgstr "Paket"

#~ msgid "Specifications:"
#~ msgstr "Specifikationer:"

#~ msgid "Status"
#~ msgstr "Status"

#~ msgid "URL"
#~ msgstr "Webbadress"

#~ msgid "Unavailable"
#~ msgstr "Ej tillgänglig"

#~ msgid "Unknown"
#~ msgstr "Okänt"

#~ msgid "Version"
#~ msgstr "Version"

#~ msgid "Wanted:"
#~ msgstr "Önskas:"

#~ msgid "Where:"
#~ msgstr "Plats:"

#~ msgid "Who:"
#~ msgstr "Av:"

#~ msgid "Working"
#~ msgstr "Fungerar"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (trasig)"
