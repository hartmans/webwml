#use wml::debian::template title="Configurando um espelho do repositório Debian"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/sid/archive.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="e466bedb14f9f661d1f4a77514cbe26596877486"

<toc-display />

<toc-add-entry name="whether">Espelhar ou não</toc-add-entry>

<p>Apreciamos todos os novos espelhos, cada futuro(a) mantenedor(a) de espelho
deveria ter certeza que ele pode responder todas estas questões antes de tentar
iniciar seu próprio espelho:</p>

<ul>
  <li>Um espelho é necessário em minha localização? Talvez já existam outros
      espelhos próximos.</li>
  <li>Eu tenho os recursos necessários para hospedar um espelho? Espelhos usam
      um <a href="size">espaço de disco</a> e banda consideráveis, é preciso
      estar apto a arcar com o custo.</li>
  <li>Um espelho é a escolha certa? Se você deseja principalmente auxiliar
      usuários(as) de seu provedor de serviço de internet (ISP) ou estabelecimento,
      talvez um proxy de cache como apt-cacher-ng, squid ou varnish pode ser a
      melhor escolha.</li>
</ul>

<toc-add-entry name="what">O que espelhar</toc-add-entry>

<p>A <a href="./">página principal sobre espelhos</a> lista os repositórios
disponíveis para espelhamento.</p>

<ul>
<li>
Usuários(as) buscarão pelo repositório debian/ para instalar o Debian através da
rede, para construir CDs (com o jigdo) e para atualizar sistemas já
instalados. <em>Recomendamos que você espelhe este repositório.</em></li>

<li>
O debian-cd/ é um repositório que não é idêntico em todos os diferentes servidores
espelho. Em alguns ele contém apenas modelos jigdo para construir
imagens de CD (usados em combinação com o debian/), em alguns ele contém
imagens já construídas de imagens de CD, e em outros sites, ambos.
<br />
Veja a página sobre <a href="$(HOME)/CD/mirroring/">espelhamento das imagens
de CD</a> para mais informações.</li>

<li>
O debian-archive/ contém o verdadeiro <em>archive</em>, as versões
antigas e obsoletas do Debian. Ele geralmente será de interesse apenas para um
pequeno segmento de usuários(as). Se você não sabe se realmente deseja espelhar
isso, é porque provavelmente não precisa.</li>

</ul>

<p>Por favor, consulte a página de <a href="size">tamanho do espelho</a>
para informações mais precisas sobre o tamanho dos espelhos.</p>

<p>O repositório debian-security/ contém as atualizações de segurança lançadas
pela equipe de segurança do Debian. Apesar de soar interessante para todos(as),
não recomendamos aos(as) nossos(as) usuários(as) o uso de espelhos para obter
atualizações de segurança e, ao invés disso, avise para que eles(elas) baixem
diretamente do nosso serviço distribuído security.debian.org. <em>Recomendamos
que o repositório debian-security <strong>não</strong> seja espelhado.</em></p>

<toc-add-entry name="wherefrom">A partir de onde fazer um espelho</toc-add-entry>

<p>Note que <code>ftp.debian.org</code> não é um local canônico dos pacotes do
Debian, em vez disso, ele é meramente um dos vários servidores que são
atualizados por um servidor interno do Debian.

<p>Existem muitos <a href="list-full">espelhos públicos</a> que suportam rsync e
isso é bom para fazer o espelhamento a partir deles. Por gentileza, utilize um
espelho que esteja pŕoximo a você em termos de rede.</p>

<p>Você deve evitar fazer o espelhamento a partir de qualquer nome de serviço
que resolva para mais de um endereço (como <code>ftp.us.debian.org</code>),
pois isso pode causar a sincronização entre diferentes estados através de suas
próprias execuções espelhadas, caso os espelhos fiquem fora de sincronia.
#
Observe também que o HTTP é o único serviço que garantimos a existência no
<code>ftp.CC.debian.org</code>. Se você deseja espelhar usando o rsync (utilizando
o ftpsync, conforme recomendado), sugerimos que você escolha o nome do site adequado
para a máquina que atualmente fornece o <code>ftp.CC.debian.org</code>. (Consulte
o diretório <code>/debian/project/trace</code> deste servidor a fim de compreendê-lo).

<toc-add-entry name="how">Como fazer o espelho</toc-add-entry>

<p>O método recomendado para espelhar é usando o famoso conjunto de scripts
ftpsync, disponível em duas formas:</p>
<ul>
    <li>como um arquivo compactado em <url "https://ftp-master.debian.org/ftpsync.tar.gz"></li>
    <li>como um repositório git: <kbd>git clone https://salsa.debian.org/mirror-team/archvsync.git</kbd> (veja <url "https://salsa.debian.org/mirror-team/archvsync/">)</li>
</ul>

<p>Não utilize seus próprios scripts e não utilize rsyncs com apenas um caminho.
Usar o ftpsync garante que as atualizações seja realizadas de maneira que o apt
não fique confuso.
Em particular, o ftpsync processa traduções, conteúdos e outros arquivos de
metadados em uma ordem em que o apt não esteja executando erros de validação,
dessa forma o(a) usuário(a) atualiza a lista de pacotes enquanto uma execução
espelhada esteja em andamento. Além disso,
também produz arquivos de rastreamento que contêm mais informações úteis para
determinar se um espelho funciona, quais arquiteturas estão contidas nele e a
partir de onde ele sincroniza.</p>

<toc-add-entry name="partial">Fazendo um espelho parcial</toc-add-entry>

<p>Considerando o <a href="size">grande tamanho dos repositórios do Debian</a>,
pode ser aconselhável espelhar apenas partes do repositório. Espelhos públicos
podem conter todos as versões (<q>testing</q>, <q>unstable</q>, etc.), mas eles
podem restringir o conjunto de arquiteturas que contém neles. O arquivo de
configuração para ftpsync possui configurações de ARCH_EXCLUDE e ARCH_INCLUDE
para essa finalidade.</p>

<toc-add-entry name="when">Quando fazer o espelho</toc-add-entry>

<p>O repositório principal é atualizado quatro vezes por dia.
Os espelhos geralmente começam a ser atualizadas em torno de 3:00, 9:00, 15:00
e 21:00 (todos os horários em UTC), mas esses não são horários fixos e você não
deveria fixar seu espelho neles.</p>

<p>Seu espelho deve atualizar algumas horas depois dos pulsos do espelhamento
do repositório principal. Você deve verificar se o site a partir do qual você está
fazendo espelho tem um arquivo de registro de data no subdiretório
<kbd>project/trace/</kbd>. O arquivo do registro de data terá o mesmo nome do
site, e irá conter o horário de término da última atualização do espelho.
Adicione algumas horas a esse horário (por segurança) e faça o espelho dele.</p>

<p><b>É essencial que seu espelho esteja sincronizado com o arquivo principal</b>.
Um mínimo de 4 atualizações por dia (24 horas) garantirá que seu espelho seja um
verdadeiro reflexo do repositório. Por favor, entenda que espelhos que não estão
sincronizados com o repositório principal não estarão na lista oficial de
espelhos.</p>

<p>A maneira mais fácil de se ter automaticamente o espelhamento executado
todos os dias é usando o cron. Veja <kbd>man crontab</kbd> para detalhes.</p>

<p>Note que se seu site for atualizado com um mecanismo push, então você não
precisa se preocupar com nada disso.</p>

<h3>Espelhamento disparado por <q>push</q> (<q>Push-triggered</q>)</h3>

<p>Espelhamento <q>push</q> é uma forma de espelhamento que nós desenvolvemos
para minimizar o tempo que leva para que as mudanças no repositório cheguem
aos espelhos. Um espelho <q>upstream</q> usa um gatilho SSH que diz aos demais
espelhos para se atualizarem.
Para uma descrição mais detalhada de como isto funciona, porque ele é
seguro, e como configurar um espelho <q>push</q>, veja
<a href="push_mirroring">a explicação completa</a>.</p>

<toc-add-entry name="settings">Configurações adicionais recomendadas</toc-add-entry>

<p>Espelhos públicos devem disponibilizar o repositório Debian via HTTP
em <code>/debian</code>.</p>

<p>Além disso, verifique se as listagens de diretórios estão ativadas
(com os nomes completos dos arquivos) e se seguem os links simbólicos.

<p>Se você usa Apache, algo dessa forma deve funcionar:
<pre>
&lt;Directory <var>/path/to/your/debian/mirror</var>&gt;
   Options +Indexes +SymlinksIfOwnerMatch
   IndexOptions NameWidth=* +SuppressDescription
&lt;/Directory&gt;
</pre>

<toc-add-entry name="submit">Como adicionar um espelho à lista de espelhos</toc-add-entry>

<p>
Se você deseja que seu espelho seja listado na lista oficial de espelhos, por favor:
</p>

<ul>
<li>Certifique-se que o seu espelho sincroniza 4 vezes por dia (24 horas) com o
repositório</li>
<li>Certifique-se de que seu espelho inclua os arquivos fonte das arquiteturas
contidos nele</li>
</ul>

<p>Uma vez que um espelho esteja configurado, ele deve ser
<a href="submit">registrado no Debian</a> a fim de ser incluído na
<a href="list">lista de espelhos</a>.
Pedidos podem ser realizados usando nosso
 <a href="submit">formulário web simples</a>.</p>


<p>Quaisquer problemas ou dúvidas podem ser enviadas para
<email mirrors@debian.org>.</p>

<toc-add-entry name="mailinglists">Listas de discussão</toc-add-entry>

<p>Existem duas <a href="../MailingLists/">listas de discussão</a> públicas
sobre os espelhos do Debian,
<a href="https://lists.debian.org/debian-mirrors-announce/">debian-mirrors-announce</a>
e
<a href="https://lists.debian.org/debian-mirrors/">debian-mirrors</a>.
Incentivamos todos os(as) mantenedores(as) de espelho a assinar a lista de
anúncios, pois será usado para divulgação de avisos importantes. Esta lista é
moderada e recebe uma baixa quantidade de tráfego. A segunda lista de
discussão é destinada a discussões gerais e é aberta a todos(as).</p>

<p>Se você tiver alguma dúvida que não tenha sido respondida nessas páginas web,
entre em contato conosco através de <email mirrors@debian.org>
ou através do IRC, #debian-mirrors em <tt>irc.debian.org</tt>.</p>


<toc-add-entry name="private-mirror">Notas para espelhos particulares (parciais)</toc-add-entry>

<p>
Se você deseja operar um espelho apenas para o seu próprio local, e precisa
conter apenas um subconjunto de versões (como a <q>stable</q>), o
<a href="https://packages.debian.org/stable/debmirror">debmirror</a> pode se
encaixar melhor para você.
</p>
