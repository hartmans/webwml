#use wml::debian::template title="Suporte"
#use wml::debian::toc
#use wml::debian::translation-check translation="9c90f0ed4b82c5e7504045fff4274d38f9b9997b"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

O Debian e seu suporte são executados por uma comunidade de voluntários(as).

Se este suporte, dirigido pela comunidade, não atende as suas necessidades,
você pode ler nossa <a href="doc/">documentação</a> ou contratar um
<a href="consultants/">consultor</a>

<toc-display />

<toc-add-entry name="irc">Ajuda em tempo real on-line usando IRC</toc-add-entry>

<p>O <a href="http://www.irchelp.org/">IRC (Internet Relay Chat</a>
é uma forma de bate-papo com pessoas do mundo todo em tempo real.
Os canais IRC dedicados ao Debian podem ser encontrados no
<a href="https://www.oftc.net/">OFTC</a>.</p>

<p>Para conectar-se, você precisa de um cliente IRC. Alguns dos clientes mais
populares são
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> e
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
os quais todos foram empacotados para Debian.
O OFTC também oferece <a href="https://www.oftc.net/WebChat/">WebChat</a>,
uma interface web que lhe permite conectar ao IRC com um navegador sem
a necessidade de instalar algum cliente local.</p>

<p>Uma vez tendo o cliente instalado, você deve informá-lo para conectar-se ao
servidor. Na maioria dos clientes, você fazer isso digitando:</p>

<pre>
/server irc.debian.org
</pre>

<p>Em alguns clientes (como o irssi), você precisará digitar isto ao invés do
anterior:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>Assim que estiver conectado, ingresse no canal <code>#debian-br</code>
para suporte em língua portuguesa, digitando:</p>

<pre>
/join #debian-br
</pre>

<p>Ou, se preferir, ingresse no canal <code>#debian</code> para suporte em
inglês:</p>

<pre>
/join #debian
</pre>

<p>Nota: clientes como HexChat, frequentemente, tem uma interface de usuário(a)
diferente e gráfica para ingressar em servidores/canais.</p>

<p>A esta altura, você se encontrará em uma multidão amigável de utilizadores(as)
do <code>#debian</code>. Você é bem-vindo(a) para fazer perguntas sobre o Debian
lá. Você encontrará o FAQ (dúvidas frequentes) do canal em
<url "https://wiki.debian.org/DebianIRC" />.</p>

<p>Há outras redes de IRC onde pode-se bater papo sobre Debian também.</p>


<toc-add-entry name="mail_lists" href="MailingLists/">Listas de discussão</toc-add-entry>

<p>O Debian é elaborado através do desenvolvimento distribuído por todo
o mundo. Portanto, o e-mail é a maneira preferida para discutir diversos itens.
Muitas das conversas entre os(as) desenvolvedores(as) Debian e os(as)
usuários(as) são gerenciadas através de várias listas de discussão.</p>

<p>Há várias listas públicas disponíveis. Para mais informações, visite
a página das <a href="MailingLists/">listas de discussão do Debian</a>.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<p>Para suporte ao(à) usuário(a) em português, por favor contate a lista
<a href="https://lists.debian.org/debian-user-portuguese/">debian-user-portuguese</a>.
</p>

<p>Para suporte em outras línguas, por favor verifique o
<a href="https://lists.debian.org/users.html">índice de listas para
usuários(as)</a>.
</p>

<p>Existem, naturalmente, muitas outras listas de discussão dedicadas a outros
aspectos do vasto ecossistema do Linux, que não são específicos do Debian.
Use seu buscador favorito para encontrar a lista mais adequada para seu
propósito.</p>


<toc-add-entry name="usenet">Grupos de notícias da Usenet</toc-add-entry>

<p>Muitas de nossas <a href="#mail_lists">listas de discussão</a> podem ser
consultadas como grupos de notícias na hierarquia <kbd>linux.debian.*</kbd>.
Isso também pode ser feito usando uma interface Web como os
<a href="https://groups.google.com/forum/">Grupos Google</a>.</p>


<toc-add-entry name="web">Web sites</toc-add-entry>

<h3 id="forums">Fóruns</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="http://forums.debian.net">Fóruns de usuários(as) Debian</a> é um portal
Web no qual você pode discutir sobre tópicos relacionados ao Debian, submeter
perguntas a respeito do Debian e tê-las respondidas por outros(as) usuários(as).</p>

<toc-add-entry name="maintainers">Encontrando mantenedores(as) de pacote</toc-add-entry>

<p>Há duas maneiras de encontrar mantenedores(as) de pacote. Se precisar contatar
o(a) mantenedor(a) por causa de um bug, simplesmente envie em relatório de bug
(veja o sistema de acompanhamento de bugs abaixo). O(A) mantenedor(a) receberá uma
cópia do relatório de bug.</p>

<p>Se você quer simplesmente se comunicar com o(a) mantenedor(a), então pode usar
os <em>aliases</em> de e-mail especiais configurados para cada pacote. Qualquer
e-mail enviado para &lt;<em>nome-do-pacote</em>&gt;@packages.debian.org será
encaminhado para o(a) mantenedor(a) responsável pelo pacote.</p>


<toc-add-entry name="bts" href="Bugs/">O Sistema de Rastreamento de Bugs (The Bug Tracking System)</toc-add-entry>

<p>A distribuição Debian tem um sistema de acompanhamento de bugs que detalha
os bugs relatados pelos(as) usuários(as) e desenvolvedores(as). Cada bug recebe
um número e é mantido em registro até que seja marcado como tendo sido
resolvido.</p>

<p>Para relatar um bug você pode usar uma das páginas de bugs abaixo;
recomendamos o uso do pacote <q>reportbug</q> para enviar automaticamente
um relatório de bug.</p>

<p>Informações sobre o envio de bugs, visualização dos bugs ativos, e sobre
o sistema de acompanhamento de bugs em geral podem ser obtidas nas
<a href="Bugs/">páginas do sistema de acompanhamento de bugs</a>.</p>


<toc-add-entry name="doc" href="doc/">Documentação</toc-add-entry>

<p>Uma parte importante de qualquer sistema operacional é a documentação, os
manuais técnicos que descrevem a operação e o uso dos programas. Como parte
de seus esforços para criar um sistema operacional livre de alta qualidade, o
Projeto Debian realiza muitos esforços para fornecer a todos(as) os(as)
seus(suas) usuários(as) a documentação apropriada em um formato facilmente
acessível.</p>

<p>Veja a <a href="doc/">página de documentação</a> para uma lista de manuais
Debian e outras documentações, incluindo o Guia de Instalação, o
FAQ do Debian e outros manuais de usuários(as) e desenvolvedores(as).</p>


<toc-add-entry name="consultants" href="consultants/">Consultores(as)</toc-add-entry>

<p>O Debian é software livre e oferece ajuda gratuita através das listas de
discussão. Algumas pessoas não têm tempo ou têm necessidades específicas e
querem contratar alguém para manter ou adicionar funcionalidades ao seu
sistema Debian. Veja a <a href="consultants/">página de consultores(as)</a> para
uma lista de pessoas/empresas.</p>


<toc-add-entry name="release" href="releases/stable/">Problemas conhecidos</toc-add-entry>

<p>Limitações e diversos problemas da versão estável (stable) atual (caso haja)
são descritos nas <a href="releases/stable/">páginas de lançamento</a>.</p>

<p>Preste atenção em particular às
<a href="releases/stable/releasenotes">notas de lançamento</a> e
à <a href="releases/stable/errata">errata</a>.</p>
