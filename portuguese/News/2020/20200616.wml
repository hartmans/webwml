#use wml::debian::translation-check translation="a3ce03f0ff8939281b7a4da3bb955c91e6857f6f"
<define-tag pagetitle>Ampere doa hardware de servidor Arm64 para o Debian para fortificar o ecossistema Arm</define-tag>
<define-tag release_date>2020-06-16</define-tag>
#use wml::debian::news

# Status: [content-frozen]

##
## Tradutores:
## - ao traduzir o arquivo quando ele estiver no repositório
##   publicity-team/announcements, por favor ignore o cabeçalho
##   translation-check. A equipe de publicidade irá adicioná-lo incluindo
##   o cabeçalho correto ao mover o arquivo para o repositório web.
##
## - ao traduzir o arquivo quando ele estiver no repositório
##   webmaster-team/webwml, por favor use o script copypage.pl para
##   criar uma página no diretório de seu idioma incluindo o cabeçalho
##   translation-check correto.
##

<p>
A <a href="https://amperecomputing.com/">Ampere®</a> se juntou ao Debian para
suportar nossa infraestrutura de hardware através da doação de três de seus
servidores Arm64 de alta performance.
Esses servidores Lenovo ThinkSystem HR330A possuem CPU eMAG da Ampere, com um
processador Arm®v8 64-bit especificamente desenhado para servidores em nuvem,
equipado com 256GB de RAM, dois SSDs de 960GB e duas portas de rede 25GbE.
</p>

<p>
Os servidores doados foram instalados na <i>University of British
Columbia</i>, nosso parceiro de hospedagem em Vancouver, Canadá
Os(As) administradores(as) de sistema Debian (DSA) configuraram os
servidores para rodar daemons de compilação arm64/armhf/armel, substituindo os
daemons de compilação rodando em máquinas de desenvolvimento menos poderosas.
Em máquinas virtuais com metade do número de vCPUs alocadas, o resultado é que
o tempo de compilação de pacotes Arm* foi reduzido pela metade com o sistema
eMAG Ampere. Outro benefício deste generoso presente é que ele permitirá que
DSA migre alguns serviços gerais do Debian que estão operando em nossa
infraestrutura atual, e provisionem máquinas virtuais para outras equipes do
Debian (por exemplo: Integração Contínua, Controle de Qualidade, etc.) que
requerem acesso à arquitetura Arm64.
</p>

<p>
<q>Nossa parceria com o Debian apoia a estratégia dos(as) nossos(as)
desenvolvedores(as) para expandir as comunidades de código aberto que rodam em
servidores Ampere, para desenvolver ainda mais o ecossistema Arm64 e viabilizar
a criação de novas aplicações,</q> disse Mauri Whalen, vice-presidente de
engenharia de software na Ampere.
<q>O Debian é uma comunidade bem dirigida e respeitada, e temos orgulho de
trabalhar com eles(as).</q>
</p>

<p>
<q>Os(As) administradores(as) de sistemas Debian são gratos(as) à Ampere pela
doação de servidores Arm64 de nível de operadoras. Ter servidores com interfaces
de gerenciamento padrão integradas como Interface Inteligente de Gerenciamento
de Plataformas (IPMI - Intelligent Platform Management),
e com as garantias e suporte de hardware da organização
Lenovo por trás delas, é precisamente o que a equipe DSA vem esperando para a
arquitetura Arm64. Esses servidores são muito poderosos e muito bem equipados:
antecipamos usá-los para serviços gerais além de daemons de construção
Arm64. Eu acho que eles demonstrarão ser bem atrativos para
operadores(as) de nuvem e estou emocionado que a Ampere Computing tenha feito
essa parceria com o Debian.</q> - Luca Filipozzi, administrador de sistemas
Debian.
</p>

<p>
É somente através da doação de esforço voluntário, equipamentos e serviços
em espécie, e financiamento que o Debian é capaz de cumprir nosso compromisso
de um sistema operacional livre. Agradecemos muito a generosidade da Ampere.
</p>

<h2>Sobre a Ampere Computing</h2>

<p>
A Ampere está projetando o futuro da nuvem em hiperescala e da computação de
ponta com o primeiro processador nativo em nuvem do mundo. Construído para a
nuvem com uma arquitetura moderna baseada em servidor Arm de 64 bits, a Ampere
oferece aos(às) clientes a liberdade de acelerar a entrega de todos os
aplicativos de computação em nuvem. Com desempenho de nuvem  líder do setor,
eficiência de energia e escalabilidade líderes do setor, os processadores Ampere
são personalizados para o crescimento contínuo da nuvem e da computação de
ponta.
</p>

<h2>Sobre o Debian</h2>

<p>
O Projeto Debian foi fundado em 1993 por Ian Murdock para ser um verdadeiro
projeto de comunidade livre. Desde então, o projeto cresceu para ser um dos
maiores e mais influentes projetos de código aberto. Milhares de voluntários(as)
de todo o mundo trabalham juntos(as) para criar e manter o software do Debian.
Disponível em 70 idiomas, e suportando uma grande variedade de tipos de
computador, o Debian se autodenomina <q>o sistema operacional universal</q>.
</p>

<h2>Informações de contato</h2>

<p>
Para mais informações, por favor visite as páginas web do Debian em <a
href="$(HOME)/">https://www.debian.org/</a>, ou envie um e-mail (em inglês)
para &lt;press@debian.org&gt;.
</p>
