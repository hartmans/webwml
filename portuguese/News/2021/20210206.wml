#use wml::debian::translation-check translation="156615cc19b61bffcfb93a5ff5e5e300fcbc9492" maintainer="Thiago Pezzo"
<define-tag pagetitle>Debian 10 atualizado: 10.8 lançado</define-tag>
<define-tag release_date>2021-02-06</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian tem o prazer de anunciar a oitava atualização de sua
versão estável (stable) do Debian <release> (codinome <q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de
segurança, junto com alguns ajustes para problemas sérios. Avisos de segurança
já foram publicados em separado e são referenciados quando disponíveis.</p>

<p>Por favor note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de se desfazer das antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p> 

<p>As pessoas que frequentemente instalam atualizações de security.debian.org
não terão que atualizar muitos pacotes, e a maioria de tais atualizações estão
incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
ao apontar o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções de bugs gerais</h2>

<p>Esta atualização da versão estável (stable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction atftp "Corrige problema de negação de serviço [CVE-2020-6097]">
<correction base-files "Atualiza /etc/debian_version para a versão pontual 10.8">
<correction ca-certificates "Atualiza o conjunto Mozilla CA para 2.40, bloqueia <q>AddTrust External Root</q> expirado">
<correction cacti "Corrige problema de injeção SQL [CVE-2020-35701] e de XSS armazenado">
<correction cairo "Corrige problema uso de máscara em image-compositor [CVE-2020-35492]">
<correction choose-mirror "Atualiza lista de espelhos">
<correction cjson "Corrige loop infinito em cJSON_Minify">
<correction clevis "Corrige criação de initramfs; clevis-dracut: inicia a criação de initramfs na instalação">
<correction cyrus-imapd "Corrige comparação de versão no script cron">
<correction debian-edu-config "Move código de limpeza host keytabs, retirando de gosa-modify-host e inserindo em um script independente, reduzindo chamadas LDAP para uma única consulta">
<correction debian-installer "Usa ABI kernel Linux 4.19.0-14; reconstruído contra proposed-updates">
<correction debian-installer-netboot-images "Reconstruído contra proposed-updates">
<correction debian-installer-utils "Suporta partições em dispositivos UAS USB">
<correction device-tree-compiler "Corrige segfault em <q>dtc -I fs /proc/device-tree</q>">
<correction didjvu "Adiciona build-dependency ausente em tzdata">
<correction dovecot "Corrige quebra quando procurando em mailboxes contendo mensagens MIME malformadas">
<correction dpdk "Nova versão estável do aplicativo original (upstream)">
<correction edk2 "CryptoPkg/BaseCryptLib: corrige derreferência NULL [CVE-2019-14584]">
<correction emacs "Não quebra com OpenPGP User IDs com nenhum endereço de e-mail">
<correction fcitx "Corrige suporte de método de entrada em Flatpaks">
<correction file "Aumenta profundidade de recursão de nome para 50 por padrão">
<correction geoclue-2.0 "Verifica o nível de precisão máxima permitida mesmo para aplicações de sistema; torna a chave API Mozilla configurável e usa uma chave específica do Debian por padrão; corrige exibição do indicador de uso">
<correction gnutls28 "Corrige erro da suíte de teste causado por certificado expirado">
<correction grub2 "Quando atualiza grub-pc não interativamente, recupera se grub-install falha; verifica explicitamente se o dispositivo-alvo existe antes de executar grub-install; grub-install: adiciona cópia de segurança e restauração; não chama grub-install em novas instalações do grub-pc">
<correction highlight.js "Corrige poluição de protótipo [CVE-2020-26237]">
<correction intel-microcode "Atualiza vários microcódigos">
<correction iproute2 "Corrige bugs na saída JSON; corrige condição de disputa que causa DOS do serviço quando usando ip netns add durante o boot">
<correction irssi-plugin-xmpp "Não desencadeia tempo de expiração de conexão irssi core prematuramente, corrigindo então conexões STARTTLS">
<correction libdatetime-timezone-perl "Atualiza para nova versão tzdata">
<correction libdbd-csv-perl "Corrige falha de teste com libdbi-perl 1.642-1+deb10u2">
<correction libdbi-perl "Correção de segurança [CVE-2014-10402]">
<correction libmaxminddb "Corrige sobreleitura de buffer baseado em pilha [CVE-2020-28241]">
<correction lttng-modules "Corrige construção em versões kernel &gt;= 4.19.0-10">
<correction m2crypto "Corrige compatibilidade com OpenSSL 1.1.1i e mais novas">
<correction mini-buildd "builder.py: sbuild call: define '--no-arch-all' explicitamente">
<correction net-snmp "snmpd: adiciona flags cacheTime e execType para EXTEND-MIB">
<correction node-ini "Não permite string inválida e perigosa como nome de seção [CVE-2020-7788]">
<correction node-y18n "Corrige problema com poluição de protótipo [CVE-2020-7774]">
<correction nvidia-graphics-drivers "Nova versão original (upstream); corrige possível negação de serviço e divulgação de informações [CVE-2021-1056]">
<correction nvidia-graphics-drivers-legacy-390xx "Nova versão original (upstream); corrige possível negação de serviço e divulgação de informações [CVE-2021-1056]">
<correction pdns "Correções de segurança [CVE-2019-10203 CVE-2020-17482]">
<correction pepperflashplugin-nonfree "Transforma em pacote fictício (dummy), tomando o cuidado de remover o plug-in anteriormente instalado (não mais funcional, nem suportado)">
<correction pngcheck "Corrige estouro de buffer [CVE-2020-27818]">
<correction postgresql-11 "Nova versão original (upstream); correções de segurança [CVE-2020-25694 CVE-2020-25695 CVE-2020-25696]">
<correction postsrsd "Assegura que etiquetas de tempo (timestamp) não sejam muito longas antes de tentar decodificá-las [CVE-2020-35573]">
<correction python-bottle "Não permite mais <q>;</q> como separador de consulta de string [CVE-2020-28473]">
<correction python-certbot "Usa automaticamente API ACMEv2 para renovações, para evitar problemas com remoção da API ACMEv1">
<correction qxmpp "Corrige potencial SEGFAULT em erro de conexão">
<correction silx "python(3)-silx: adiciona dependência em python(3)-scipy">
<correction slirp "Corrige estouros de buffer [CVE-2020-7039 CVE-2020-8608]">
<correction steam "Nova versão original (upstream)">
<correction systemd "journal: não desencadeia assertion quando journal_file_close() é passado como NULL">
<correction tang "Evita condição de disputa entre keygen e update">
<correction tzdata "Nova versão original (upstream); atualização inclui dados de fuso horário">
<correction unzip "Aplica correções adicionais para CVE-2019-13232">
<correction wireshark "Corrige várias quebras, loops infinitos e vazamento de memória [CVE-2019-16319 CVE-2019-19553 CVE-2020-11647 CVE-2020-13164 CVE-2020-15466 CVE-2020-25862 CVE-2020-25863 CVE-2020-26418 CVE-2020-26421 CVE-2020-26575 CVE-2020-28030 CVE-2020-7045 CVE-2020-9428 CVE-2020-9430 CVE-2020-9431]">
</table>


<h2>Atualizações de segurança</h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança para a versão
estável (stable).
A equipe de Segurança já lançou um aviso para cada uma dessas
atualizações:</p>


<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2020 4797 webkit2gtk>
<dsa 2020 4801 brotli>
<dsa 2020 4802 thunderbird>
<dsa 2020 4803 xorg-server>
<dsa 2020 4804 xen>
<dsa 2020 4805 trafficserver>
<dsa 2020 4806 minidlna>
<dsa 2020 4807 openssl>
<dsa 2020 4808 apt>
<dsa 2020 4809 python-apt>
<dsa 2020 4810 lxml>
<dsa 2020 4811 libxstream-java>
<dsa 2020 4812 xen>
<dsa 2020 4813 firefox-esr>
<dsa 2020 4814 xerces-c>
<dsa 2020 4815 thunderbird>
<dsa 2020 4816 mediawiki>
<dsa 2020 4817 php-pear>
<dsa 2020 4818 sympa>
<dsa 2020 4819 kitty>
<dsa 2020 4820 horizon>
<dsa 2020 4821 roundcube>
<dsa 2021 4822 p11-kit>
<dsa 2021 4823 influxdb>
<dsa 2021 4824 chromium>
<dsa 2021 4825 dovecot>
<dsa 2021 4827 firefox-esr>
<dsa 2021 4828 libxstream-java>
<dsa 2021 4829 coturn>
<dsa 2021 4830 flatpak>
<dsa 2021 4831 ruby-redcarpet>
<dsa 2021 4832 chromium>
<dsa 2021 4833 gst-plugins-bad1.0>
<dsa 2021 4834 vlc>
<dsa 2021 4835 tomcat9>
<dsa 2021 4837 salt>
<dsa 2021 4838 mutt>
<dsa 2021 4839 sudo>
<dsa 2021 4840 firefox-esr>
<dsa 2021 4841 slurm-llnl>
<dsa 2021 4843 linux-latest>
<dsa 2021 4843 linux-signed-amd64>
<dsa 2021 4843 linux-signed-arm64>
<dsa 2021 4843 linux-signed-i386>
<dsa 2021 4843 linux>
</table>


<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos devido a circunstâncias além do nosso
controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction compactheader "Incompatível com versões Thunderbird atuais">

</table>

<h2>Instalador do Debian</h2>
<p>O instalador foi atualizado para incluir as correções incorporadas
na versão estável (stable) pela versão pontual.</p>

<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas para a versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>informações da versão estável (stable) (notas de lançamento, errata, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional livre Debian.</p>

<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com a equipe de
lançamento da estável (stable) em &lt;debian-release@lists.debian.org&gt;.</p>
