#use wml::debian::template title="Vulnerabilidades do boot seguro UEFI no GRUB2 - 2021"
#use wml::debian::translation-check translation="eaa2e2477454c72064e63ad9f58a59ec94b9d105"

<p>
Desde
o anúncio do grupo de bugs
<a href="$(HOME)/security/2020-GRUB-UEFI-SecureBoot">"BootHole"</a>
no GRUB2 em julho de 2020, pesquisadores(as) da área de segurança e
desenvolvedores(as) no Debian e demais projetos continuam a busca por
outros problemas que podem permitir o contorno do boot seguro UEFI. Vários mais
tem sido detectados. Veja o
<a href="$(HOME)/security/2021/dsa-4867">alerta de segurança 4867-1 do Debian</a>
para mais detalhes completos. O objetivo deste documento é explicar as
consequências desta vulnerabilidade de segurança e quais passos estão sendo
tomados para remediá-la.</p>

<ul>
  <li><b><a href="#what_is_SB">Contexto: o que é o boot seguro UEFI?</a></b></li>
  <li><b><a href="#grub_bugs">Múltiplos bugs do GRUB2 encontrados</a></b></li>
  <li><b><a href="#revocations">Revogações de chave necessárias para corrigir a cadeia de boot seguro</a></b></li>
  <li><b><a href="#revocation_problem">Quais são os efeitos de revogação de chave?</a></b></li>
  <li><b><a href="#package_updates">Pacotes e chaves atualizados</a></b>
  <ul>
    <li><b><a href="#grub_updates">1. GRUB2</a></b></li>
    <li><b><a href="#linux_updates">2. Linux</a></b></li>
    <li><b><a href="#shim_updates">3. Shim e SBAT</a></b></li>
    <li><b><a href="#fwupdate_updates">4. Fwupdate</a></b></li>
    <li><b><a href="#fwupd_updates">5. Fwupd</a></b></li>
    <li><b><a href="#key_updates">6. Chaves</a></b></li>
  </ul></li>
  <li><b><a href="#buster_point_release">Versão pontual Debian 10.10 (<q>buster</q>),
        mídias de instalação e live atualizadas</a></b></li>
  <li><b><a href="#more_info">Mais informações</a></b></li>
</ul>

<h1><a name="what_is_SB">Contexto: o que é o boot seguro UEFI?</a></h1>

<p>
O boot seguro UEFI (UEFI Secure Boot - SB) é um mecanismo de verificação
para garantir que o código executado por um firmware do UEFI do computador
é confiável. Ele é projetado para proteger um sistema contra código malicioso
sendo carregado e executado cedo no processo de boot, antes do sistema
operacional ter sido carregado.
</p>

<p>
O SB funciona usando um checksum criptográfico e assinaturas. Cada
programa que é carregado pelo firmware inclui uma assinatura e
um checksum, e antes de permitir a execução, o firmware verificará se o
programa é confiável pela validação do checksum e da assinatura.
Quando o SB está habilitado em um sistema, qualquer tentativa de executar
um programa não confiável não será permitida. Isto bloqueia códigos
não esperados/não autorizados de rodarem no ambiente UEFI.
</p>

<p>
A maior parte do hardware X86 vem de fábrica com as chaves da Microsoft
pré-carregadas. Isto significa que o firmware desses sistemas confiarão
nos binários que foram assinados pela Microsoft. A maioria dos sistemas modernos
será entregue com o SB habilitado - eles não executarão qualquer código
não assinado por padrão, mas é possível alterar a configuração do firmware
para, ou desabilitar o SB, ou adicionar chaves de assinatura extras.
</p>

<p>
O Debian, como muitos outros sistemas operacionais baseados em Linux, usa um
programa chamado shim para estender essa confiança do firmware para outros
programas que nós precisamos que estejam seguros durante o boot inicial: o
gerenciador de inicialização GRUB2, o kernel do Linux e ferramentas de
atualização de firmware (fwupd e fwupdate).
</p>

<h1><a name="grub_bugs">Múltiplos bugs do GRUB2 encontrados</a></h1>

<p>
Um bug foi encontrado no módulo <q>acpi</q> do GRUB2. Este módulo tem o 
objetivo de fornecer um driver de interface para ACPI ("Advanced Configuration 
and Power Interface" - Interface Avançada de Configuração e Energia), 
um componente muito comum nos hardwares computacionais atuais. Infelizmente o
módulo ACPI permite atualmente a um(a) usuário(a) privilegiado(a) carregar
tabelas próprias no boot seguro e realizar alterações arbitrárias no estado do
sistema: permitindo assim que se possa quebrar facilmente a cadeia de boot
seguro. Essa brecha na segurança foi resolvida agora.
</p>

<p>
Como no caso do BootHole, em vez de simplesmente resolver aquele bug em
particular, o pessoal de desenvolvimento continuou com auditorias e análises
aprofundadas do código-fonte do GRUB2. Seria irresponsável
consertar uma grande falha sem procurar por outras! Foram encontrados alguns
outros locais onde as alocações de memória interna poderiam ser sobrecarregadas
gerando entradas inesperadas, e alguns outros locais onde a memória poderia ser
utilizada após ser liberada. Correções para todas essas falhas foram
compartilhadas e testadas pela comunidade.
</p>

<p>
Novamente, veja o <a href="$(HOME)/security/2021/dsa-4867">alerta de segurança
4867-1 do Debian</a> para uma lista completa de problemas encontrados.
</p>


<h1><a name="revocations">Revogações de chave necessárias para corrigir a cadeia de boot seguro</a></h1>

<p>
O Debian e outros(as) fornecedores(as) de sistemas operacionais obviamente <a
href="#package_updates">lançarão versões corrigidas</a> do GRUB2 e do
Linux. Entretanto, isto pode não ser uma correção completa para os problemas
vistos aqui. Intervenientes maliciosos ainda serão capazes de usar versões mais
antigas e vulneráveis para contornar o boot seguro.
</p>

<p>
Para impedir isso, o próximo passo será a Microsoft colocar na lista de
bloqueio aqueles binários inseguros para que sejam barrados na execução sob o
SB. Isto é alcançado usando a lista <b>DBX</b>, uma funcionalidade do projeto
do boot seguro UEFI. Todas as distribuições Linux entregues com cópias do shim
assinadas pela Microsoft foram solicitadas a fornecer detalhes dos binários ou
chaves envolvidas para facilitar este processo. O <a
href="https://uefi.org/revocationlistfile">arquivo da lista de revogação
UEFI</a> será atualizado para incluir esta informação. Em <b>algum</b>
ponto no futuro, os sistemas começarão a usar aquela lista atualizada e
recusarão a execução dos binários vulneráveis sob o boot seguro.
</p>

<p>
A linha do tempo <i>exata</i> para que esta mudança seja implementada não está
clara ainda. Os(As) fornecedores(as) de BIOS/UEFI incluirão a nova lista de
revogação nas novas construções de firmware para novos hardwares em algum
momento. Também a Microsoft <b>talvez possa</b> enviar atualizações para
sistemas existentes via atualizações do Windows. Algumas distribuições Linux
podem enviar atualizações através de seus próprios processos de atualizações
de segurança. O Debian <b>ainda</b> não fez isso, mas nós estamos examinando
a situação para o futuro.
</p>

<h1><a name="revocation_problem">Quais são os efeitos da revogação de chave?</a></h1>

<p>
A maior parte dos(as) fornecedores(as) são cautelosos(as) sobre aplicações
automáticas de atualizações que revoguem as chaves usadas no boot seguro.
Instalações existentes de software com SB habilitado podem, repentinamente,
recusar o boot completamente, a menos que o(a) usuário(a) seja cuidadoso(a)
em também instalar todas as atualizações necessárias de software. Sistemas
Windows/Linux em dual boot podem repentinamente parar o boot do Linux.
Mídias de instalação e live antigas também falharão no boot, é claro,
potencialmente fazendo com que seja mais difícil a recuperação de sistemas.
</p>

<p>
Existem duas maneiras óbvias de consertar um sistema como este que não
inicializa:
</p>

<ul>
  <li>Novo boot no modo de <q>recuperação</q>
    usando <a href="#buster_point_release">mídias mais novas de instalação</a> e
    aplicando as atualizações necessárias; ou</li>
  <li>Desabilitando temporariamente o boot seguro para retomar o acesso ao
    sistema, aplicar as atualizações e reabilitá-lo.</li>
</ul>

<p>
Ambas podem parecer opções simples, mas cada uma pode consumir muito
tempo para usuários(as) com múltiplos sistemas. Também esteja ciente de que, por
projeto, habilitar e desabilitar o boot seguro são ações que necessitam de
acesso direto à máquina. Normalmente, <b>não</b> é possível alterar essa
configuração fora da configuração de firmware do computador. Máquinas servidores
remotos podem precisar de cuidado extra aqui, por essa mesma razão.
</p>

<p>
Devido a esses motivos, é altamente recomendado que <b>todos(as)</b>
usuários(as) Debian sejam cautelosos(as) e instalem todas
as <a href="#package_updates">atualizações recomendadas</a> para seus
sistemas tão logo quanto possível, para reduzir as chances de problemas
no futuro.
</p>

<h1><a name="package_updates">Pacotes e chaves atualizados</a></h1>

<p>
<b>Nota:</b> sistemas executando o Debian 9 (<q>stretch</q>) e mais antigos
<b>não</b> necessariamente receberão atualizações aqui, uma vez que o Debian 10
(<q>buster</q>) foi a primeira versão do Debian a incluir suporte para boot
seguro UEFI.
</p>

<p>
Há cinco pacotes-fonte no Debian que serão atualizados devido
às alterações do boot seguro UEFI descritas aqui:
</p>

<h2><a name="grub_updates">1. GRUB2</a></h2>

<p>
As versões atualizadas do pacote GRUB2 do Debian estão disponíveis agora
através do repositório debian-security para a versão do Debian 10 estável
(<q>buster</q>). Versões corrigidas logo estarão no repositório normal do Debian
para as versões de desenvolvimento do Debian (instável (unstable) e teste
(testing)).
</p>

<h2><a name="linux_updates">2. Linux</a></h2>

<p>
As versões atualizadas dos pacotes linux do Debian estarão disponíveis
em breve através do repositório buster-proposed-updates para a versão do Debian
10 estável (stable) (<q>buster</q>) e serão incluídas na versão pontual 10.10
que está por vir. Novos pacotes logo estarão no repositório do Debian para
versões de desenvolvimento do Debian (instável (unstable) e teste (testing)).
Nós esperamos que pacotes corrigidos sejam enviados em breve para
buster-backports também.
</p>

<h2><a name="shim_updates">3. Shim e SBAT</a></h2>

<p>
A série de bugs "BootHole" foi a primeira vez que uma revogação de chave em
larga escala foi necessária no ecossistema de boot seguro UEFI. Este fato
demonstrou uma infeliz falha no projeto de revogação no SB: com uma grande
quantidade de diferentes distribuições Linux e binários UEFI, o tamanho da lista
de revogação cresceu rapidamente. Muitos sistemas computacionais possuem apenas
uma quantidade limitada de espaço para armazenamento de dados de revogação de
chaves, podendo ser preenchidos rapidamente, deixando esses sistemas quebrados
de várias maneiras.
</p>

<p>
Para combater esse problema, os(as) desenvolvedores(as) dos pacotes shim
criaram um método muito mais eficiente em termos de espaço e tempo para
bloquear binários inseguros no UEFI no futuro. O método é chamado 
<b>SBAT</b> (<q>Secure Boot Advanced Targeting</q> ou "Direcionamento avançado
de boot seguro" em tradução livre). O método funciona através do rastreamento da
geração de números de programas assinados. Em vez de revogar as assinaturas
individualmente no momento em que os problemas são encontrados, contadores são
utilizados para indicar quais versões antigas de programas não são consideradas
seguras. Revogar uma série antiga de binários do GRUB2, por exemplo, agora se
torna um caso de atualizar uma variável UEFI contendo o número de geração para
GRUB2; quaisquer versões do GRUB2 mais antigas que esse número não serão mais 
consideradas seguras. Para muito mais informações sobre o SBAT, acesse a 
<a href="https://github.com/rhboot/shim/blob/main/SBAT.md">documentação do
SBAT</a>.


<p>
<b>Infelizmente</b>, o desenvolvimento desse novo shim SBAT ainda não
está pronto. Os(As) desenvolvedores(as) esperavam lançar uma nova versão do 
pacote shim com essa nova e importante funcionalidade, mas encontraram problemas
inesperados. O desenvolvimento prossegue. Por toda comunidade Linux é esperado
que o pacote shim seja atualizado em breve. Até que esteja pronto, todos(as) nós 
vamos continuar utilizando nossos atuais binários assinados do shim.
</p>

<p>
Versões atualizadas do pacote shim do Debian estarão disponíveis assim
que o trabalho for finalizado. Eles serão anunciados nos locais apropriados.
Nesse momento publicaremos uma nova versão pontual 10.10 junto com 
o lançamento de novos pacotes shim para as versões de desenvolvimento do Debian
(instável (unstable) e teste (testing)).
</p>

<h2><a name="fwupdate_updates">4. Fwupdate</a></h2>

<p>
As versões atualizadas dos pacotes fwupdate do Debian estarão disponíveis em
breve através do repositório buster-proposed-updates para a versão do Debian 10
estável (stable) (<q>buster</q>) e serão incluídas na versão pontual 10.10 que
está por vir. O fwupdate já havia sido removido das versões instável (unstable)
e teste (testing) a um tempo atrás, substituído pelo fwupd.
</p>

<h2><a name="fwupd_updates">5. Fwupd</a></h2>

<p>
As versões atualizadas dos pacotes fwupd do Debian estarão disponíveis em breve
através do repositório buster-proposed-updates para a versão do Debian 10
estável (stable) (<q>buster</q>) e serão incluídas na versão pontual 10.10 que
está por vir. Novos pacotes também estão no repositório do Debian para versões
de desenvolvimento do Debian (instável (unstable) e teste (testing)).
</p>

<h2><a name="key_updates">6. Chaves</a></h2>

<p>
O Debian gerou novas chaves de assinaturas e certificados para seus pacotes
de boot seguro. Costumávamos utilizar somente um certificado para todos os 
nossos pacotes:
</p>

<ul>
  <li>"Debian Secure Boot Signer 2020"
  <ul>
    <li>(fingerprint <code>3a91a54f9f46a720fe5bbd2390538ba557da0c2ed5286f5351fe04fff254ec31)</code></li>
  </ul></li>
</ul>

<p>
A partir de agora serão utilizadas chaves e certificados separados para cada um
dos cinco tipos diferentes de pacotes envolvidos, o que dará melhor
flexibilidade no futuro:
</p>

<ul>
  <li>"Debian Secure Boot Signer 2021" - fwupd
  <ul>
    <li>(fingerprint <code>309cf4b37d11af9dbf988b17dfa856443118a41395d094fa7acfe37bcd690e33</code>)</li>
  </ul></li>
  <li>"Debian Secure Boot Signer 2021" - fwupdate
  <ul>
    <li>(fingerprint <code>e3bd875aaac396020a1eb2a7e6e185dd4868fdf7e5d69b974215bd24cab04b5d</code>)</li>
  </ul></li>
  <li>"Debian Secure Boot Signer 2021" - grub2
  <ul>
    <li>(fingerprint <code>0ec31f19134e46a4ef928bd5f0c60ee52f6f817011b5880cb6c8ac953c23510c</code>)</li>
  </ul></li>
  <li>Debian Secure Boot Signer 2021" - linux
  <ul>
    <li>(fingerprint <code>88ce3137175e3840b74356a8c3cae4bdd4af1b557a7367f6704ed8c2bd1fbf1d</code>)</li>
  </ul></li>
  <li>"Debian Secure Boot Signer 2021" - shim
  <ul>
    <li>(fingerprint <code>40eced276ab0a64fc369db1900bd15536a1fb7d6cc0969a0ea7c7594bb0b85e2</code>)</li>
  </ul></li>
</ul>

<h1><a name="buster_point_release">Lançamento pontual Debian 10.10
(<q>buster</q>), mídias de instalação e live atualizadas</a></h1>

<p>
Todas as correções descritas aqui estão marcadas para inclusão na
versão pontual Debian 10.10 (<q>buster</q>), que será lançada em breve.
A versão 10.10 seria, portanto, uma boa escolha para os(as) usuários(as) que 
procuram mídia do Debian para instalação e live. Imagens anteriores 
talvez não funcionem com boot seguro no futuro, assim que as revogações 
forem lançadas.
</p>

<h1><a name="more_info">Mais informações</a></h1>

<p>
Muitas outras informações sobre a configuração do boot seguro do Debian estão no
wiki do Debian - veja
<a href="https://wiki.debian.org/SecureBoot">https://wiki.debian.org/SecureBoot</a>.</p>

<p>
Outras fontes sobre este tópico incluem:
</p>

<ul>
  <li><a href="https://access.redhat.com/security/vulnerabilities/RHSB-2021-003">artigo
    da vulnerabilidade do Red Hat</a></li>
  <li><a href="https://www.suse.com/support/kb/doc/?id=000019892">Orientação da SUSE</a></li>
  <li><a href="https://wiki.ubuntu.com/SecurityTeam/KnowledgeBase/GRUB2SecureBootBypass2021">artigo
      de segurança do Ubuntu</a></li>
</ul>
