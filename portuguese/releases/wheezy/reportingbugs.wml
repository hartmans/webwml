#use wml::debian::template title="Debian 7 -- Relatando problemas" BARETITLE=true
#use wml::debian::translation-check translation="8844e216b88b5bab6744f5ffb34f62f802cff3fc"

# Translators: copy of squeeze/reportingbug

<h2><a name="report-release">Com as notas de lançamento</a></h2>

<p>Os erros nas <a href="releasenotes">notas de lançamento</a> devem ser
<a href="$(HOME)/Bugs/Reporting">relatados como bugs</a> contra o pseudopacote
<tt>release-notes</tt>. Discussões desse documento são coordenadas através da
lista de discussão debian-doc em <a href="mailto:debian-doc@lists.debian.org">\
&lt;debian-doc@lists.debian.org&gt;</a>. Caso você tenha problemas
com o documento que não sejam apropriados para um bug, você provavelmente deve
enviar um e-mail para a lista.</p>

<h2><a name="report-installation">Com a instalação</a></h2>

<p>Caso você encontre algum problema no sistema de instalação, por favor
relate bugs contra o pacote <tt>installation-reports</tt>. Preencha o
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">modelo de relatório</a>
para garantir que você incluiu todas as informações necessárias.</p>

<p>Caso você tenha sugestões ou correções para o
<a href="installmanual">manual de instalação</a>, você deve
<a href="$(HOME)/Bugs/Reporting">registrá-las como bugs</a> contra o
<tt>installation-guide</tt>, que é o pacote fonte no qual essa documentação é
mantida.</p>

<p>Caso você tenha problemas com o sistema de instalação que não sejam
apropriados para um bug (por exemplo, você não está certo se realmente é um bug
ou não, a parte do sistema com problema não está clara, etc), você
provavelmente deve enviar um e-mail para a lista de discussão,
<a href="mailto:debian-boot@lists.debian.org">\
&lt;debian-boot@lists.debian.org&gt;</a>.</p>

<h2><a name="report-upgrade">Com uma atualização</a></h2>

<p>Caso você tenha problemas durante atualização do seu sistema a partir de
versões anteriores, por favor registre um bug contra o pacote
<tt>upgrade-reports</tt>, que é o pseudopacote usado para acompanhar essa
informação. Para mais informações sobre como submeter relatórios de atualização,
por favor, leia as <a href="releasenotes">notas de lançamento</a>.</p>

<h2><a name="report-package">Quaisquer outros problemas</a></h2>

<p>Caso você tenha problemas com o sistema após a instalação, tente encontrar
qual é o pacote com problema e <a href="$(HOME)/Bugs/Reporting">registre um bug</a>
contra esse pacote.</p>
