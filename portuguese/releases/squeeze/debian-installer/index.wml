#use wml::debian::template title="Debian &ldquo;squeeze&rdquo; -- Informações de instalação" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/squeeze/release.data"
#use wml::debian::translation-check translation="f36546f515e33fb0e590b3db17a516bf3d605f5f"

<h1>Instalando o Debian <current_release_squeeze></h1>

<p><strong>O Debian GNU/Linux 6.0 foi substituído pelo
<a href="../../wheezy/">Debian 7.0 (<q>wheezy</q>)</a>. Algumas
destas imagens de instalação podem não estar disponíveis, ou podem não
funcionar mais, é recomendado que você instale o wheezy em vez disso.
</strong></p>


<p>
<strong>Para instalar o Debian</strong> <current_release_squeeze>
(<em>squeeze</em>), baixe qualquer uma das seguintes imagens:
</p>

<div class="line">
<div class="item col50">
	<p><strong>imagem de CD netinst (geralmente 135-175 MB)</strong></p>
		<netinst-images />
</div>

<div class="item col50 lastcol">
	<p><strong>imagem de CD businesscard (geralmente 20-50 MB)</strong></p>
		<businesscard-images />
</div>

</div>

<div class="line">
<div class="item col50">
	<p><strong>conjuntos completos de CDs</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>conjuntos completos de DVDs</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>outras imagens (netboot, pendrive usb, etc)</strong></p>
<other-images />
</div>
</div>

<div id="firmware_nonfree" class="warning">
<p>
Se algum hardware do seu sistema <strong>requerer firmware não livre para ser
carregado</strong> com o controlador de dispositivos, você pode usar um dos
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/squeeze/current/">\
arquivos tarball de pacotes de firmwares comuns</a> ou baixar uma imagem
<strong>não oficial</strong> que inclui esses firmwares
<strong>não livres</strong>. Instruções de como usar esses arquivos tarball e
informações gerais sobre carregamento de firmware durante a instalação podem
ser encontradas no guia de instalação (veja documentação abaixo).
</p>
<div class="line">
<div class="item col50">
<p><strong>imagens de CD netinst (geralmente 175-240 MB)</strong>
<strong>não livre</strong> <strong>com firmware</strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>

<p>
<strong>Notas</strong>
</p>
<ul>
   <li>
	Para baixar imagens completas de CD e DVD, recomenda-se usar
	o jigdo.
    </li><li>
	Para arquiteturas pouco comuns, somente um número limitado de imagens
	dos conjuntos de CD e DVD está disponível como um arquivo ISO.
	Os conjuntos completos estão disponíveis somente via jigdo.
    </li><li>
        As imagens de <em>CD</em> multiarquitetura suportam i386/amd64/powerpc
	e alpha/hppa/ia64 respectivamente; a instalação é similar a instalar
	a partir de uma imagem netinst para uma única arquitetura.
    </li><li>
        As imagens de <em>DVD</em> multiarquitetura suportam i386/amd64/powerpc;
	a instalação é similar a instalar a partir de uma imagem de CD completa
	para uma única arquitetura; o DVD também contém o código-fonte para
	todos os pacotes incluídos.
    </li><li>
        Para as imagens de instalação, arquivos de verificação
        (<tt>SHA256SUMS</tt>, <tt>SHA512SUMS</tt> e outros) estão disponíveis a
        partir do mesmo diretório das imagens.
    </li>
</ul>


<h1>Documentação</h1>

<p>
<strong>Se você lê somente um documento</strong> antes da instalação, leia
nosso <a href="../i386/apa">Howto de instalação</a>, um rápido passo a passo
do processo de instalação. Outras documentações úteis incluem:
</p>

<ul>
<li><a href="../installmanual">Guia de instalação do squeeze</a><br />
instruções detalhadas de instalação</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ do instalador do
Debian</a> e <a href="$(HOME)/CD/faq/">FAQ do Debian-CD</a><br />
perguntas comuns e respostas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
documentação mantida pela comunidade</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Esta é uma lista de problemas conhecidos no instalador que acompanha o
Debian <current_release_squeeze>. Se você teve algum problema
instalando o Debian e não vê seu problema listado aqui, por favor, envie-nos um
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">relatório de
instalação</a> descrevendo o problema ou
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">verifique a
wiki</a> para outros problemas conhecidos.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Errata para a versão 6.0</h3>

<dl class="gloss">
	<dt> Alguns sistemas sparc não podem ser instalados usando CD-ROM </dt>
        <dd>O instalador do Debian para o squeeze não inclui drivers do kernel
        PATA, o que torna impossível concluir a instalação a partir da mídia de
        CD em sistemas que exigem que esses drivers acessem a unidade de CD-ROM
        (por exemplo, Ultra 10), pois o instalador irá falha em detectá-lo.
        O problema pode ser contornado inicializando o instalador por rede,
        eliminando a necessidade de acessar a unidade de CD-ROM durante a
        instalação.
	<br />
	Veja <a href="https://bugs.debian.org/610906">#610906</a>.<br />
	Isso será corrigido na próxima versão pontual do squeeze (6.0.1).</dd>

	<dt>Possivelmente não funcionando a detecção de dispositivos braille
	USB</dt>
        <dd>Ao permitir que o instalador Debian detecte um dispositivo braille
        USB conectado, posteriormente ele pode acabar mostrando apenas "a tela
        não está no modo de texto". Isso ocorre devido a uma potencial corrida
        entre a detecção e o início do armazenador dos quadros. Uma solução
        alternativa é passar <tt>brltty</tt> na linha de comando do kernel para
        forçar a detecção.<br />
	Veja <a href="https://bugs.debian.org/611648">#611648</a>.
	</dd>

	<dt>Nenhum driver de rede para Sparc T2+</dt>
	<dd>O driver de rede niu, exigido pelos sistemas sparc T2 + mais
	recentes, não está incluído no d-i, tornando impossível a instalação
	neles usando qualquer método que dependa da configuração inicial da
	rede. O driver está incluído nos pacotes do kernel; portanto, espera-se
	que a rede opere normalmente após a instalação ser concluída.<br />
	Veja <a href="https://bugs.debian.org/608516">#608516</a>.
	</dd>

	<dt>Os sistemas que usam placas gráficas aty podem não inicializar o
	instalador corretamente</dt>
	<dd>Os sistemas que utilizam placas gráficas aty (por exemplo,
	Ultra 10) podem não inicializar o instalador corretamente, com o kernel
	congelando no início do estágio de inicialização com a última mensagem
	"console [tty0] enabled, bootconsole disabled". Este é um bug do kernel,
	para o qual um patch está disponível, mas isso pode não estar totalmente
	corrigido.<br />
        O problema pode ser contornado adicionando um parâmetro de inicialização
        do kernel 'video=atyfb:off' para desativar o framebuffer durante a
        inicialização, o que permite que o instalador (e o kernel normal) seja
        inicializado nesses sistemas.<br />
	Veja <a href="https://bugs.debian.org/609466">#609466</a>.
	</dd>

	<dt>A seleção de teclado, com instalador gráfico, não funciona para alguns
	layouts</dt>
	<dd>A pré-seleção do teclado não está funcionando com instalações
	gráficas para algumas combinações (Bulgária, alemão suíço, Suécia e
	brasileiro). Além disso, a escolha feita não deve ser usada e o sistema
	é padronizado para inglês americano (/etc/default/keyboard).<br />
	Veja <a href="https://bugs.debian.org/610843">#610843</a>.
	</dd>

	<dt>Potenciais problemas de instalação com placas de rede baseadas em
	RTL8169</dt>
	<dd>O instalador do Debian pode não conseguir usar placas de rede
	baseadas na família RTL8169 durante a instalação, o que inclui o
	download de pacotes durante a instalação através dessas placas.
	O sistema instalado não é afetado pelo problema.<br />
	Veja <a href="https://bugs.debian.org/558316">#558316</a> e bugs
	similares unificados.<br />
	Isso será corrigido na próxima versão pontual do squeeze (6.0.1).
	</dd>

	<dt>Falha ao inicializar após a instalação bem-sucedida de um root
	btrfs</dt>
	<dd>A instalação termina normalmente, mas após a reinicialização,
	resulta em um prompt do initramfs busybox.<br />
	Veja <a href="https://bugs.debian.org/608538">#608538</a>.
	</dd>

	<dt>O Windows não é adicionado à lista do grub</dt>
	<dd>O Instalador do Debian detecta o Windows durante a instalação, mas
	não o adiciona ao menu de inicialização do grub. Como solução
	alternativa, após a instalação, execute o update-grub.<br />
	Veja <a href="https://bugs.debian.org/608025">#608025</a>.
	</dd>

	<dt>Cria tabela de partições incompatível com o Mac OS 9</dt>
	<dd>Foi relatado que a ferramenta de particionamento no instalador
	torna a tabela de partições irreconhecível pelo Mac OS 9, que não é
	mais inicializada.
	Embora as partições HFS+ sejam compatíveis com Linux e Mac OS X, é
	recomendável tomar todas as precauções ao instalar em uma máquina com
	Mac OS 9. <br />
	Veja <a href="https://bugs.debian.org/604134">#604134</a>.
	</dd>

        <dt> O particionamento falha no kFreeBSD</dt>
	<dd>Há relatos de falha no particionamento no kFreeBSD.
	O problema parece estar relacionado ao alinhamento de
	partições/partições estendidas.<br />
	Veja <a href="https://bugs.debian.org/593733">#593733</a>,
	<a href="https://bugs.debian.org/597088">#597088</a> e
	<a href="https://bugs.debian.org/602129">#602129</a>.
	</dd>

	<dt>A placa de rede/gráfica/armazenamento não funciona corretamente </dt>
	<dd>Existem vários hardwares, principalmente placas de rede, placas
	gráficas e controladores de armazenamento, que exigem firmware binário
	não livre para funcionar corretamente.<br />
        O Debian está comprometido com os valores do software livre e nunca faz
        com que o sistema exija software não livre (veja o
        <a href="https://www.debian.org/social_contract">contrato social</a>
        do Debian). Portanto, o firmware não livre não está incluído no
        instalador.<br />
        Mas se você deseja carregar qualquer firmware externo durante a
        instalação, você é livre para fazê isso. O processo está documentado no
        manual de instalação.
	</dd>

	<dt>Problema de instalação do zipl que torna o s390 desinstalável </dt>
	<dd>Se uma partição dedicada para o diretório /boot for criada, a
	inicialização do sistema falhará após a instalação se o /boot estiver
	montado antes do /. <br />
	Veja <a href="https://bugs.debian.org/519254">#519254</a>.
	</dd>

	<dt>Roteadores com bugs podem causar problemas de rede </dt>
	<dd>Se você tiver problemas de rede durante a instalação, isso pode ser
	causado por um roteador em algum lugar entre você e o espelho Debian
	que não lida corretamente com dimensionamento da janela.
	Veja <a href="https://bugs.debian.org/401435">#401435</a> e esse
	<a href="http://kerneltrap.org/node/6723">artigo kerneltrap</a> para
	detalhes.<br />
	Você pode contornar esse problema desabilitando o dimensionamento da
	janela TCP. Abra um shell e digite o seguinte comando:<br />
	<tt>echo 0 &gt; /proc/sys/net/ipv4/tcp_window_scaling</tt><br />
        Para o sistema instalado você provavelmente não deve desativar
        completamente o dimensionamento da janela TCP. O comando a seguir
        definirá intervalos de leitura e gravação que devem funcionar com
        praticamente qualquer roteador:<br />
	<tt>echo 4096 65536 65536 &gt;/proc/sys/net/ipv4/tcp_rmem</tt><br />
	<tt>echo 4096 65536 65536 &gt;/proc/sys/net/ipv4/tcp_wmem</tt>
	</dd>

<!-- deixando isso para possível uso futuro...
	<dt>i386: é necessário mais de 32 mb de memória para instalar</dt>
	<dd>
	A quantidade mínima de memória necessária para instalar com êxito no
	i386 é 48 mb, em vez dos 32 mb anteriores. Esperamos reduzir os
	requisitos de volta para 32 mb posteriormente. Os requisitos de memória
	também podem ter sido alterados para outras arquiteturas.
	</dd>
-->

	<dt>Partições acima de 16TiB não são suportadas por ext4</dt>
	<dd>
	As ferramentas de criação de sistema de arquivos ext4 não suportam a
	criação de sistemas de arquivos acima de 16TiB de tamanho.
	</dd>

	<dt>s390: recursos não suportados</dt>
	<dd>
	<ul>
		<li>o suporte para a disciplina DASD DIAG não está disponível
		    no momento</li>
	</ul>
	</dd>

  </dl>

<if-stable-release release="squeeze">
<p>
Versões melhoradas do sistema de instalação estão sendo desenvolvidas para a
próxima versão do Debian e também podem ser usadas para instalar o squeeze.
Para detalhes, veja
<a href="$(HOME)/devel/debian-installer/">a página do projeto
Debian-Installer</a>.
</p>
</if-stable-release>
