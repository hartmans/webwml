#use wml::debian::translation-check translation="d5484a72e71b8da89fe25f18d5d9248fab4b56c6" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>Данное обновление содержит обновлённый микрокод ЦП для некоторых типов ЦП Intel
и предоставляет решения для уязвимостей, которые могут приводить к
повышению привилегий вместе с VT-d и различным атакам через
сторонние каналы.</p>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 3.20210608.2~deb10u1.</p>

<p>Заметьте, что было сообщено о двух регрессиях; для некоторых ЦП CoffeeLake
данное обновление приводит к поломке iwlwifi
(https://github.com/intel/Intel-Linux-Processor-Microcode-Data-Files/issues/56),
а для некоторых ЦП Skylake R0/D0 в системах, использующих очень старую прошивку/BIOS,
система может зависать во время загрузки
(https://github.com/intel/Intel-Linux-Processor-Microcode-Data-Files/issues/31)</p>

<p>Если эти проблемы проявляются у вас, то мы можете восстановить систему, отключив загрузку
микрокода во время запуска системы (как это описано в файле README.Debian, который также доступен онлайн по адресу
<a href="https://salsa.debian.org/hmh/intel-microcode/-/blob/master/debian/README.Debian">\
https://salsa.debian.org/hmh/intel-microcode/-/blob/master/debian/README.Debian)</a></p>

<p>Рекомендуется обновить пакеты intel-microcode.</p>

<p>С подробным статусом поддержки безопасности intel-microcode можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4934.data"
