#use wml::debian::translation-check translation="43ee4d624248d2eac95a5997ba86398be35914ec" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 11: выпуск 11.2</define-tag>
<define-tag release_date>2021-12-18</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о втором обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction authheaders "Новый выпуск основной ветки разработки с исправлениями ошибок">
<correction base-files "Обновление /etc/debian_version для редакции 11.2">
<correction bpftrace "Исправление индексации массива">
<correction brltty "Исправление работы в X при использовании sysvinit">
<correction btrbk "Исправление регрессии в обновлении для CVE-2021-38173">
<correction calibre "Исправление синтаксической ошибки">
<correction chrony "Исправление привязки сокета к сетевому устройству с именем, имеющем длину более 3 символов, при включении фильтра системных вызовов">
<correction cmake "Добавление PostgreSQL 13 к списку известных версий">
<correction containerd "Новый стабильный выпуск основной ветки разработки; обработка двусмысленного грамматического разбора OCI-манифестов [CVE-2021-41190]; поддержка <q>clone3</q> в профиле seccomp по умолчанию">
<correction curl "Удаление -ffile-prefix-map из curl-config, исправляющее проблему совместной установки libcurl4-gnutls-dev на мультиарихтектурных системах">
<correction datatables.js "Исправление недостаточного экранирования массивов, передаваемых функции экранирования HTML [CVE-2021-23445]">
<correction debian-edu-config "pxe-addfirmware: исправление пусти сервера TFTP; улучшение поддержки настройки и сопровождения LTSP chroot">
<correction debian-edu-doc "Обновление руководства Debian Edu Bullseye из вики; обновление переводов">
<correction debian-installer "Повторная сборка с учётом proposed-updates; обновление ABI ядра до -10">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction distro-info-data "Обновление поставляемых данных для Ubuntu 14.04 и 16.04 ESM; добавление Ubuntu 22.04 LTS">
<correction docker.io "Исправление возможного изменения прав доступа к системным файлам узла [CVE-2021-41089]; блокировка прав доступа к файлам в /var/lib/docker [CVE-2021-41091]; предотвращение отправки данных учётной записи в регистр по умолчанию [CVE-2021-41092]; добавление поддержки для системного вызова <q>clone3</q> в политике seccomp по умолчанию">
<correction edk2 "Уязвимость Address Boot Guard TOCTOU [CVE-2019-11098]">
<correction freeipmi "Установка файлов pkgconfig в правильный каталог">
<correction gdal "Исправление поддержки распаковки BAG 2.0 в драйвере LVBAG">
<correction gerbv "Исправленеи записи за пределами выделенного буфера памяти [CVE-2021-40391]">
<correction gmp "Исправление переполнений целых чисел и буфера [CVE-2021-43618]">
<correction golang-1.15 "Новый стабильный выпуск основной ветки разработки; исправление <q>net/http: паника из-за быстрого чтения persistConn после паники обработчика</q> [CVE-2021-36221]; исправление <q>archive/zip: переполнение в проверке перед выделением памяти может вызвать OOM-панику</q> [CVE-2021-39293]; исправление выхода за пределы выделенного буфера памяти [CVE-2021-38297], чтения за пределами буфера [CVE-2021-41771], отказа в обслуживании [CVE-2021-44716 CVE-2021-44717]">
<correction grass "Исправление грамматического разбора форматов GDAL, в которых описание содержит двоеточие">
<correction horizon "Повторное включение переводов">
<correction htmldoc "Исправление переполнения буфера [CVE-2021-40985 CVE-2021-43579]">
<correction im-config "Предпочтение Fcitx5 над Fcitx4">
<correction isync "Исправление многочисленных переполнений буфера [CVE-2021-3657]">
<correction jqueryui "Исправление проблем с выполнением недоверенного кода [CVE-2021-41182 CVE-2021-41183 CVE-2021-41184]">
<correction jwm "Исправление аварийной остановки при использовании пункта меню <q>Move</q>">
<correction keepalived "Исправление слишком широкого правила DBus [CVE-2021-44225]">
<correction keystone "Исправление утечки информации, позволяющей определить существование пользователей [CVE-2021-38155]; применение некоторых улучшений производительности в keystone-uwsgi.ini по умолчанию">
<correction kodi "Исправление переполнения буфера в списках проигрывания PLS [CVE-2021-42917]">
<correction libayatana-indicator "Масштабирование иконок при загрузке из файла; предотвращение регулярных аварийных остановок в апплетах-индикаторах">
<correction libdatetime-timezone-perl "Обновление поставляемых данных">
<correction libencode-perl "Исправление утечек памяти в Encode.xs">
<correction libseccomp "Добавление поддержки системных вызовов вплоть до версии Linux 5.15">
<correction linux "Новый выпуск основной ветки разработки; увеличение ABI до 10; RT: обновление до 5.10.83-rt58">
<correction linux-signed-amd64 "Новый выпуск основной ветки разработки; увеличение ABI до 10; RT: обновление до 5.10.83-rt58">
<correction linux-signed-arm64 "Новый выпуск основной ветки разработки; увеличение ABI до 10; RT: обновление до 5.10.83-rt58">
<correction linux-signed-i386 "Новый выпуск основной ветки разработки; увеличение ABI до 10; RT: обновление до 5.10.83-rt58">
<correction lldpd "Исправление переполнения динамической памяти [CVE-2021-43612]; прекращение установки тега VLAN в случае, если клиент его не установил">
<correction mrtg "Исправление ошибок в именах переменных">
<correction node-getobject "Исправление загрязнения прототипа [CVE-2020-28282]">
<correction node-json-schema "Исправление загрязнения прототипа [CVE-2021-3918]">
<correction open3d "Проверка того, что python3-open3d зависит от python3-numpy">
<correction opendmarc "Исправление opendmarc-import; увеличение максимальной поддерживаемой длины токенов в заголовках ARC_Seal, что исправляет аварийные остановки">
<correction plib "Исправление переполнения целых чисел [CVE-2021-38714]">
<correction plocate "Исправление проблемы, когда символы не из ASCII экранируются неправильно">
<correction poco "Исправление установки файлов CMake">
<correction privoxy "Исправление утечек памяти [CVE-2021-44540 CVE-2021-44541 CVE-2021-44542]; исправление межсайтового скриптинга [CVE-2021-44543]">
<correction publicsuffix "Обновление поставляемых данных">
<correction python-django "Новый стабильный выпуск с исправлениями безопасности: исправление потенциального обхода контроля доступа на основе путей URL [CVE-2021-44420]">
<correction python-eventlet "Исправленеи совместимости с dnspython 2">
<correction python-virtualenv "Исправление аварийной остановки при использовании --no-setuptools">
<correction ros-ros-comm "Исправление отказа в обслуживании [CVE-2021-37146]">
<correction ruby-httpclient "Использование системного хранилища сертификатов">
<correction rustc-mozilla "Новый пакет с исходным кодом для поддержки сборки более новых версий firefox-esr и thunderbird">
<correction supysonic "Создание символьной ссылки на библиотеку jquery вместо её прямой загрузки; создание правильных символьных ссылок на минимизированные файлы bootstrap CSS">
<correction tzdata "Обновленеи данных для Фиджи и Палестины">
<correction udisks2 "Опции монтирования: всегда использовать errors=remount-ro для файловых систем ext [CVE-2021-3802]; использование команды mkfs для форматирования разделов exfat; добавление Recommends exfatprogs в качестве предпочитаемой альтернативы">
<correction ulfius "Исправление использования собственных распределителей ресурсов с ulfius_url_decode и ulfius_url_encode">
<correction vim "Исправление переполнения динамической памяти [CVE-2021-3770 CVE-2021-3778], использование указателей после освобождения памяти [CVE-2021-3796]; удаление альтернатив vim-gtk во время перехода vim-gtk -&gt; vim-gtk3, что упрощает обновления с выпуска buster">
<correction wget "Исправление загрузок более 2ГБ на 32-битных системах">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2021 4980 qemu>
<dsa 2021 4981 firefox-esr>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4984 flatpak>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4988 libreoffice>
<dsa 2021 4989 strongswan>
<dsa 2021 4992 php7.4>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4996 wpewebkit>
<dsa 2021 4998 ffmpeg>
<dsa 2021 5002 containerd>
<dsa 2021 5003 ldb>
<dsa 2021 5003 samba>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5007 postgresql-13>
<dsa 2021 5008 node-tar>
<dsa 2021 5009 tomcat9>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5016 nss>
<dsa 2021 5017 xen>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5022 apache-log4j2>
</table>



<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
