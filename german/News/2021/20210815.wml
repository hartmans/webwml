<define-tag pagetitle>Debian Edu / Skolelinux Bullseye — Eine vollständige Linux-Lösung für Ihre Schule</define-tag>
<define-tag release_date>2021-08-15</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="e7ce9859ec2794a8c0d0978182d2d513ed96cfa0" maintainer="Wolfgang Schweer"

<p>
Sind Sie der Administrator für einen Computerraum oder ein ganzes Schulnetzwerk?
Möchten Sie Server, Workstations und Laptops so installieren, dass sie
zusammenarbeiten?
Wollen Sie die Stabilität von Debian mit bereits vorkonfigurierten
Netzwerkdiensten?
Wünschen Sie sich ein webbasiertes Werkzeug zur Verwaltung von Systemen und
mehreren hundert oder noch mehr Benutzerkonten?
Haben Sie sich gefragt, ob und wie ältere Rechner genutzt werden könnten?
</p>

<p>
Dann ist Debian Edu richtig für Sie. Die Lehrer selbst (oder ihr technischer
Support) können innerhalb von einigen Tagen eine vollständige Lernumgebung für
viele Benutzer und mit vielen Rechnern einrichten. Debian Edu kommt mit
hunderten vorinstallierter Anwendungen, weitere Debian-Pakete können jederzeit
hinzugefügt werden.
</p>

<p>
Das Debian Edu Entwicklerteam freut sich, die Veröffentlichung von Debian Edu 11
<q>Bullseye</q> bekanntzugeben; es beruht auf Debian 11 <q>Bullseye</q>.
Es wäre gut, wenn Sie diese Veröffentlichung testen und uns eine Rückmeldung
(&lt;debian-edu@lists.debian.org&gt;) geben könnten, damit wir weitere
Verbesserungen vornehmen können.
</p>

<h2>Über Debian Edu und Skolelinux</h2>

<p>
<a href="https://blends.debian.org/edu"> Debian Edu, auch unter dem Namen
Skolelinux bekannt</a>, ist eine auf Debian basierende Distribution,
die eine gebrauchsfertige Umgebung für ein komplett konfiguriertes Schulnetzwerk
bereitstellt. Unmittelbar nach der Installation steht ein Schulserver zur
Verfügung, auf dem alle für ein Schulnetzwerk notwendigen Dienste laufen.
Es müssen nur noch Benutzer und Maschinen hinzugefügt werden, was mit der
komfortablen Weboberfläche von GOsa² erledigt wird. Eine Umgebung für das Booten
über das Netzwerk steht ebenfalls zur Verfügung; damit können nach der
anfänglichen Installation des Hauptservers von CD / DVD / BD oder USB-Stick
alle anderen Rechner über das Netz installiert werden.
Ältere Computer (bis zu zehn Jahre alt oder sogar älter) können als LTSP
Thin Clients oder Diskless Workstations eingesetzt werden; diese booten
über das Netzwerk und benötigen überhaupt keine Installation oder Konfiguration.
Der Debian Edu Schulserver bietet Authentifizierung mittels LDAP-Verzeichnis
und Kerberos, zentralisierte Benutzerverzeichnisse, DHCP-Server, Web-Proxy und
viele weitere Dienste. Die Arbeitsumgebung enthält mehr als 70 unterrichtsbezogene
Software-Pakete, weitere stehen im Debian-Depot zur Verfügung. Schulen können
zwischen Xfce, GNOME, LXDE, MATE, KDE Plasma, Cinnamon und LXQt als graphischer
Arbeitsumgebung wählen.
</p>

<h2>Neuerungen in Debian Edu 11 <q>Bullseye</q></h2>

<p>Einige Punkte der Veröffentlichungs-Informationen für Debian Edu 11 <q>Bullseye</q>,
das auf Debian 11 <q>Bullseye</q> beruht.
Die vollständige Liste inclusive weiterer Informationen gibt es im
(englischprachigen) <a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Features#New_features_in_Debian_Edu_Bullseye">Debian Edu manual chapter</a> bzw.
entsprechend im
<a href="https://jenkins.debian.net/userContent/debian-edu-doc/debian-edu-doc-de/debian-edu-bullseye-manual.html#Features">
Debian Edu Handbuch</a>.
</p>

<ul>
<li>
Neues <a href="https://ltsp.org">LTSP</a> zur Unterstützung von Diskless
Workstations. Thin Clients werden weiterhin unterstützt, jetzt unter Verwendung
von <a href="https://wiki.x2go.org">X2Go</a>-Technologie.
</li>
<li>
Booten über das Netzwerk  wird unter Verwendung von iPXE anstelle von PXELINUX
bereitgestellt, um mit LTSP konform zu sein.
</li>
<li>
Der grafische Modus des Debian-Installers wird für iPXE-Installationen verwendet.
</li>
<li>
Samba ist jetzt als <q>Standalone-Server</q> mit Unterstützung für SMB2/SMB3
konfiguriert.
</li>
<li>
DuckDuckGo wird als Standardsuchanbieter sowohl für Firefox ESR als auch für
Chromium verwendet.
</li>
<li>
Neues Werkzeug zum Einrichten von freeRADIUS mit Unterstützung für die beiden
Methoden EAP-TTLS/PAP und PEAP-MSCHAPV2.
</li>
<li>
Verbessertes Werkzeug zur Konfiguration eines neuen Systems mit
<q>Minimal</q>-Profil als dediziertes Gateway.
</li>
</ul>

<h2>Download-Optionen, Installationsschritte und Handbuch</h2>

<p>
Separate Netzwerk-Installer CD-Images für 64-Bit- und 32-Bit-PCs
sind verfügbar. Nur in seltenen Fällen (für PCs älter als etwa 15 Jahre) wird
das 32-Bit-Image erforderlich sein. Die Images können hier heruntergeladen
werden:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Alternativ gibt es auch erweiterte BD-Images (mit einer Größe
von mehr als 5 GB). Damit ist es möglich, auch ohne Internetverbindung ein
vollständiges Debian-Edu-Netzwerk einzurichten - für alle unterstützten
Arbeitsumgebungen und alle von Debian unterstützten Sprachen. Diese Images
können hier heruntergeladen werden:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
Die Images können mittels der im Downloadverzeichnis vorhandenen signierten
Checksums verifiziert werden.
<br />
Sobald ein Image heruntergeladen wurde, kann geprüft werden, ob
<ul>
<li>
dessen Checksum mit derjenigen in der angegebenen Datei übereinstimmt, und ob
</li>
<li>
die Datei mit den Checksums nicht manipuliert wurde.
</li>
</ul>
Ausführliche Informationen über diese Schritte enthalten die
<a href="https://www.debian.org/CD/verify">Verifizierungshinweise</a>.
</p>

<p>
Da Debian Edu 11 <q>Bullseye</q> vollständig auf Debian 11 <q>Bullseye</q> beruht,
stehen die Quellen aller Pakete im Debian-Depot zur Verfügung.
</p>

<p>
Bitte beachten Sie die (englischprachige) Statusseite:
<a href="https://wiki.debian.org/DebianEdu/Status/Bullseye">Debian Edu Bullseye status page</a>.
Diese enthält aktualisierte Information über Debian Edu 11 <q>Bullseye</q> sowie
Hinweise, wie <code>rsync</code> zum Herunterladen der ISO-Images verwendet
werden kann.
</p>

<p>
Wenn Sie ein Upgrade von Debian Edu 10 <q>Buster</q> durchführen, lesen Sie
bitte den zugehörigen Abschnitt im (englischprachigen) <a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Upgrades">Debian Edu manual chapter</a> bzw. den entsprechenden Abschnitt im deutschen Handbuch.
</p>

<p>
Bitte lesen Sie vor der Installation den entsprechenden (englischprachigen) Handbuchabschnitt:
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Installation#Installing_Debian_Edu">Debian Edu manual chapter</a>
bzw. den entsprechenden Abschnitt im deutschen Handbuch.
</p>

<p>
Nach der Installation müssen diese ersten Schritte erfolgen:
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/GettingStarted">first steps</a>.
Einen entsprechenden Abschnitt gibt es auch im deutschen Handbuch.
</p>

<p>
Unter <a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/">Debian Edu wiki pages</a>
gibt es die neueste Version des Debian Edu <q>Bullseye</q> Handbuchs in
englischer Sprache. Vollständige Übersetzungen des Handbuchs gibt es für
Deutsch, Französisch, Italienisch, Dänisch, Niederländisch, Norwegisch Bokmål,
Japanisch, Chinesisch (vereinfacht) und Portugiesisch (Portugal).
Teilweise übersetzte Versionen existieren für Spanisch, Rumänisch und Polnisch.
Eine Übersicht der <a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
letzten veröffentlichten Versionen des Handbuchs</a> ist verfügbar.
</p>

<p>
Weitere Informationen über Debian 11 <q>Bullseye</q> selbst gibt es in den
Veröffentlichungs-Informationen und in der Installationsanleitung, zu finden unter
<a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Über Debian</h2>

<p>
Das Debian-Projekt ist ein Zusammenschluss von Entwicklern freier Software,
die Ihre Zeit und Anstrengungen einbringen, um ein vollständig freies
Betriebssystem zu erstellen, welches als Debian bekannt ist.
</p>

<h2>Kontaktinformationen</h2>

<p>
Weitere Informationen finden Sie auf der Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a> oder senden Sie eine
E-Mail (auf Englisch) an &lt;press@debian.org&gt;.
</p>

