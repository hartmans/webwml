#use wml::debian::template title="Danke, dass Sie Debian herunterladen!" 
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="1d8e9363ec81c198712c4ba45ff77dd67145b026" maintainer="Erik Pfannenstein"

{#meta#:
<meta http-equiv="refresh" content="3;url=<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">
:#meta#}

<p>
Dies ist Debian <:=substr '<current_initial_release>', 0, 2:>, Codename <em><current_release_name></em>, Netinst für <: print $arches{'amd64'}; :>.
</p>

<p>
Falls Ihr Download nicht automatisch beginnt, klicken Sie auf <a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso</a>.
</p>
<p>
Download-Prüfsumme: <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS">SHA512SUMS</a> <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS.sign">Signatur</a>
</p>



<div class="tip">
	<p><strong>Wichtig:</strong> Bitte achten Sie darauf, dass Sie <a href="$(HOME)/CD/verify">Ihre heruntergeladene Datei mit der Prüfsumme verifizieren</a>.</p>
</div>

<p>
Debian-Installer-ISOs sind Hybrid-Abbilder, was bedeutet, dass sie wahlweise direkt auf CD/DVD/BD-Rohlinge gebrannt oder auf <a href="https://www.debian.org/CD/faq/#write-usb">USB-Sticks</a> geschrieben werden können.
</p>

<h2 id="h2-1">Andere Installer</h2>

<p>
Andere Installer und Images wie Live-Systeme, Offline-Installer für Systeme ohne Netzwerkverbindung und Installer für andere CPU-Architekturen sowie Cloud-Instanzen sind auf der Seite <a href="$(HOME)/distrib/">Debian bekommen</a> zu finden.
</p>

<p>
Inoffizielle Installer mit <a href="https://wiki.debian.org/Firmware"><strong>unfreier Firmware</strong></a>, die bei einigen Netzwerk- und Videoadaptern helfen können, können von <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">Unofficial non-free images including firmware packages</a> heruntergeladen werden.
</p>

<h2 id="h2-2">Verwandte Links</h2>

<p><a href="$(HOME)/releases/<current_release_name>/installmanual">Installationsanleitung</a></p>
<p><a href="$(HOME)/releases/<current_release_name>/releasenotes">Veröffentlichungshinweise</a></p>
<p><a href="$(HOME)/CD/verify">Anleitung zum Verifizieren der ISO-Datei</a></p>
<p><a href="$(HOME)/CD/http-ftp/#mirrors">Alternative Download-Quellen</a></p>
<p><a href="$(HOME)/releases">Andere Veröffentlichungen</a></p>

