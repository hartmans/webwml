#use wml::debian::template title="Supporto"
#use wml::debian::translation-check translation="9c90f0ed4b82c5e7504045fff4274d38f9b9997b" maintainer="Luca Monducci"
#use wml::debian::toc

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<p>Debian e anche l'assistenza sono gestiti da una comunità di volontari.</p>

<p>Se l'assistenza fornita dalla comunità non fosse soddisfacente per le proprie
necessità è possibile leggere la <a href="doc/">documentazione</a> oppure
ingaggiare un <a href="consultants/">consulente</a>.

<toc-display />

<toc-add-entry name="irc">Aiuto in linea e in tempo reale tramite IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a> è un
mezzo per chiacchierare con persone in tutto il mondo in tempo reale. Su
<a href="https://www.oftc.net/">OFTC</a> è possibile trovare un canale
dedicato a Debian.</p>

<p>Per collegarsi è necessario usare un client IRC. Alcuni tra i più
popolari sono
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> e
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
tutti presenti in Debian.
OFTC dispone anche di <a href="https://www.oftc.net/WebChat/">WebChat</a>,
un'intefaccia web per collegarsi a IRC usando un browser senza la necessit&agrave;
di installare in locale un client.</p>

<p>Dopo aver avviato il client è
necessario indicare a quale server collegarsi, con la maggior parte dei
client si fa digitando:</p>

<pre>
/server irc.debian.org
</pre>

<p>Con alcuni client (tra i quali irssi) è necessario usare quest'altro comando:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>Una volta connesso è possibile raggiungere il canale <code>#debian</code>
digitando</p>

<pre>
/join #debian
</pre>

<p>Nota: client come HexChat sfruttano alcune caratteristiche grafiche per
collegarsi a canali e server.</p>

<p>A questo punto sarete tra la simpatica gente che abita
<code>#debian</code>, si possono porre domande su Debian qui.
Si possono vedere le risposte alle domande ricorrenti sul canale
all'indirizzo <url "https://wiki.debian.org/it/DebianIRC" />.</p>

<p>C'è un gran numero di altre reti IRC in cui possono trovare
informazioni su Debian.</p>


<toc-add-entry name="mail_lists" href="MailingLists/">Liste di messaggi</toc-add-entry>

<p>La distribuzione Debian è sviluppata in maniera distribuita in
tutto il mondo. Il mezzo preferito per comunicare e discutere è l'email, la
maggior parte delle conversazioni tra sviluppatori Debian e utenti è effettuata
attraverso le liste di messaggi (in inglese <i>mailing list</i>).</p>

<p>Esistono diverse liste di messaggi pubbliche. Per maggiori dettagli si
veda la pagina dedicata alle <a href="MailingLists/">liste di messaggi
Debian</a>.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<p>Per il supporto in lingua italiano, contattare la 
<a href="https://lists.debian.org/debian-italian/">mailing list
debian-italian</a>.</p>

<p>Per il supporto in lingua inglese, contattare la 
<a href="https://lists.debian.org/debian-user/">mailing list
debian-user</a>.</p>

<p>Per il supporto in altre lingue, si veda l'<a
href="https://lists.debian.org/users.html">indice delle mailing
list per gli utenti</a>.</p>

<p>Ovviamente esistono molte altre liste di messaggi, dedicate ai diversi
aspetti di Linux, che non sono specifiche per Debian. Usare il motore di
ricerca preferito per trovare la lista più adatta ai propri scopi.</p>


<toc-add-entry name="usenet">Newsgroup usenet</toc-add-entry>

<p>Molte delle nostre <a href="#mail_lists">liste di messaggi</a> sono
consultabili come newsgroup all'interno della gerarchia <kbd>linux.debian.*</kbd>.
La consultazione di questi newsgroup può essere fatta anche utilizzando una
interfaccia web quale <a href="https://groups.google.com/forum/">Google Groups</a>.


<toc-add-entry name="web">Siti web</toc-add-entry>

<h3 id="forums">Forum</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="http://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="http://forums.debian.net">Debian User Forums</a> è un portale
web in cui è possibile porre domande su Debian alle quali rispondono altri
utenti.</p>


<toc-add-entry name="maintainers">Comunicare con i manutentori dei pacchetti</toc-add-entry>

<p>Ci sono due modi per comunicare con i manutentori dei pacchetti. Se si ha
bisogno di contattare un manutentore a causa di un bug, è sufficiente
inviare la segnalazione del bug (si veda la sezione sul Sistema di Tracciamento
dei Bug). Il manutentore riceverà una copia della segnalazione.</p>

<p>Se si vuole semplicemente mettersi in contatto con un manutentore,
allora si può usare uno speciale alias di posta elettronica disponibile
per ogni pacchetto: tutte le email inviate a
&lt;<em>nome&nbsp;pacchetto</em>&gt;@packages.debian.org
sono automaticamente inoltrate al manutentore del pacchetto.</p>


<toc-add-entry name="bts" href="Bugs/">Il Sistema di Tracciamento dei Bug</toc-add-entry>

<p>La distribuzione Debian dispone di un sistema di tracciamento
dei bug che registra in modo dettagliato i bug segnalati da utenti e
sviluppatori. A ogni bug è assegnato un numero che lo identifica anche
quando viene in qualche modo gestito e marcato come chiuso.</p>

<p>È possibile segnalare un bug usando una delle pagine dei bug elencate
in seguito, tuttavia si raccomanda l'uso del pacchetto Debian
<q>reportbug</q> per creare automaticamente la segnalazione del bug.</p>

<p>Informazioni su come segnalare un bug, su come vedere le segnalazioni
attualmente attive e sul sistema di tracciamento dei bug in generale si
possono trovare nelle <a href="Bugs/">pagine web del sistema di tracciamento
dei bug</a>.</p>


<toc-add-entry name="doc" href="doc/">Documentazione</toc-add-entry>

<p>Un aspetto importante per qualsiasi sistema operativo è la documentazione,
nei manuali tecnici sono descritte le operazioni e come usare i programmi.
Tra le attività del lavoro per creare un sistema operativo di alta-qualità,
il Progetto Debian sta facendo ogni sforzo per fornire a tutti i propri utenti
la documentazione corretta in modo semplice e accessibile.</p>

<p>Consultare la <a href="doc/">pagina sulla documentazione</a> per la
lista dei manuali Debian e altri documenti, compresa la Guida
all'Installazione, le FAQ Debian e altra documentazione per gli utenti e
per gli sviluppatori.</p>


<toc-add-entry name="consultants" href="consultants/">Consulenti</toc-add-entry>

<p>Debian è software libero e offre aiuto gratuito attraverso le liste di
messaggi. Chi non ha tempo oppure ha particolari necessità ed è disposto a
pagare qualcuno per gestire o installare nuove funzionalità sul proprio
sistema Debian. Si veda la <a href="consultants/">pagina dei consulenti</a>
per un elenco di persone/aziende che fanno questo di lavoro.</p>


<toc-add-entry name="other">Altre risorse di supporto</toc-add-entry>

<p>Controllare la pagina dei <a href="misc/related_links">collegamenti
comuni</a>.</p>


<toc-add-entry name="release" href="releases/stable/">Problemi
conosciuti</toc-add-entry>

<p>Limiti e problemi gravi dell'attuale distribuzione stabile (se presenti)
sono descritti nella <a href="releases/stable/">pagina del rilascio</a>.</p>

<p>In particolare fare attenzione alle <a
href="releases/stable/releasenotes">note di rilascio</a> e alla <a
href="releases/stable/errata">errata</a>.</p>
