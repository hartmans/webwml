#use wml::debian::template title="유용한 번역 제안 "
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="d49333f670c2639f601fd741d3dd5f060de97e2b" maintainer="Seunghun Han (kkamagui)"

<p> 번역에 국한되지 않은 일은 일반적인 내용들을 설명해 놓은
<a href="working">웹 페이지 작업하기</a>를 보세요.

<h2>무엇을 번역할까?</h2>

<p>소개를 위해 <a href="translating#completenew">새 번역 시작 지침</a>을
참조하세요.</p>

<p>페이지를 번역하기 시작했다면, 사용자가 가장 많이 방문할 페이지부터 시작할 것을 권합니다.
아래에 몇 가지 가이드라인이 있고, <a href="stats/">번역 통계</a>의 페이지 리스트는 인기순으로 정렬되어 있습니다.</p>

<dl>
<dt><strong>가장 중요(Most important):</strong></dt>
  <dd>
  <ul>
    <li>주(main) 디렉터리: index.wml, contact.wml, donations.wml,
        social_contract.wml, support.wml</li>
    <li>intro/ 디렉터리: about.wml, cn.wml, free.wml, why_debian.wml</li>
    <li>releases/ 디렉터리: index.wml</li>
    <li>releases/<current_release_name>/ 디렉터리: index.wml,
        installmanual.wml, releasenotes.wml</li>
    <li>distrib/ 디렉터리: index.wml, packages.wml, netinst.wml, ftplist.wml</li>
    <li>mirror/ 디렉터리: list.wml</li>
    <li>CD/ 디렉터리: index.wml</li>
    <li>doc/ 디렉터리: index.wml</li>
    <li>MailingLists/ 디렉터리: index.wml</li>
    <li>security/ 디렉터리: index.wml</li>
  </ul>
  </dd>
<dt><strong>보통(Standard):</strong></dt>
  <dd>앞에서 얘기한 디렉터리의 나머지 파일들, 그리고 아래 파일들:
  <ul>
    <li>Bugs/index.wml, Bugs/Reporting.wml</li>
    <li>banners/index.wml</li>
    <li>blends/index.wml</li>
    <li>consultants/index.wml</li>
    <li>doc/ddp.wml</li>
    <li>events/index.wml</li>
    <li>international/index.wml와 여러분의 언어로 번역된 페이지 또는 디렉터리 </li>
    <li>logos/index.wml</li>
    <li>mirror/index.wml</li>
    <li>misc/index.wml</li>
    <li>News/index.wml</li>
    <li>News/weekly/index.wml</li>
    <li>ports/index.wml</li>
    <li>partners/index.wml</li>
  </ul>
  </dd>
<dt><strong>선택(Optional):</strong></dt>
  <dd> 앞서 언급한 디렉터리의 다른 파일들. 자주 업데이트되어 최신으로 유지하기가
  어려운 아래 하위 디렉터리를 포함합니다:
  <ul>
    <li>MailingLists/desc/</li>
    <li>News/</li>
    <li>doc/books.wml</li>
    <li>events/</li>
    <li>security/</li>
  </ul>
  </dd>
<dt><strong>가장 덜 중요(Least Important):</strong></dt>
  <dd> devel/ 및 vote/ 디렉터리에 있는 파일들. 이런 파일은 개발자를 위한 것이며
  개발자의 주 언어는 영어입니다. 여러분이 훌륭한 번역팀을 갖추고 있을 때 번역하는
  것이 좋습니다. </dd>
</dl>

<p>
<strong> 여러분이 유지보수할 시간이 있을 때 파일을 번역하는 것이 중요합니다.
소수의 잘 관리된 페이지가 다수의 오래된 페이지보다 유용하거든요.
</strong>

<h2> 원문에 번역을 얼마나 밀접하게 유지할 것인가? </h2>

<p> 여러분이 번역을 할 때, 컨텐츠를 수정하고 싶은 때가 있습니다.
예를 들자면, 지원 페이지 같은 것이죠. 아마 여러분은 특정 언어와
관련된 메일링 리스트, 예를 들어 한국어 버전에 대한 debian-user-korean 같은 것을
페이지에 추가하려고 할지 모릅니다.

<p> 여러분이 더 중대한 변화를 주려고 한다면, 다른 버전들과 내용을 비슷하게
유지하기 위해 <a href="mailto:debian-www@lists.debian.org">debian-www list</a>로
알려주세요.

<p> 페이지는 유용해야 합니다. 여러분이 모국어 사용자를 돕기 위한 정보가 있다면,
얼마든지 추가하세요. 여러분은 모국어를 사용하는 방문자에게 흥미로운 내용을 제공하기
위해 international/&lt;Language&gt;.wml 파일을 사용할 수 있습니다.

<p> 여러분이 모든 사용자게에 유용한 정보를 알고 있다면 debian-www에 알려주세요.

<h2>번역자는 파일이 업데이트가 필요한지를 어떻게 아나요?</h2>

<p>번역자가 <a href="uptodate">웹 사이트 번역을 최신으로 유지하기</a> 위해
사용할 수 있는 메커니즘이 있습니다.

<h2>gettext 템플릿(template)의 번역을 어떻게 최신으로 유지하나요?</h2>

<p> 영문 파일을 업데이트 한 후에, 원본파일로 여러분의 .po 파일을 업데이트하려면
<kbd>make update-po</kbd> 명령어를 <code>po/</code> 하위에 위치한 여러분의 번역과
관련된 디렉터리에서 실행합니다.
<a href="https://lists.debian.org/debian-www-cvs/">debian-www-cvs 메일링 리스트</a>의
로그 메시지를 보면 이런 작업을 수행해야 하는지 파악하는데 도움이 됩니다.
아니면 간단하게 여러분이 주기적으로 명령어를 수행해주는 방법도 있죠. </p>

<p> 변경된 내용을 간략하게 보려면 <kbd>make stats</kbd> 명령어를 사용하세요.
Gettext가 "<code>#, fuzzy</code>"로 값을 추측해야 하는 태그(tag)와
<code>msgstr</code>뒤에 빈 문자열이 있는 단순한 형태의 새 태그를 만들어 줄겁니다.</p>

<h2>번역 페이지가 오래되었는지 사용자는 어떻게 아나요?</h2>

<P> <a href="uptodate">웹 사이트 번역을 최신으로 유지하기</a>에 사용되는
<code>번역 검사(translation-check)</code>가 오래된 번역이라는 메모를 표시해줄겁니다.

<h2>번역 때 주의할 것</h2>

<p>아래는 번역할 때 특별한 주의를 요하는 페이지와 디렉터리 목록입니다:

<dl>
<dt><tt>뉴스(News/)</tt>
   <dd> 여러분이 원하는 만큼 많은 혹은 적은 뉴스를 번역할 수 있습니다.
   뉴스의 인덱스는 항목의 제목을 가지고 자동으로 생성됩니다.
   항목이 번역이되면 번역된 제목이 인덱스에 사용됩니다.</dd>

<dt><tt>보안(security/)</tt>
   <dd> 보안 디렉터리는 뉴스(News)/ 디렉터리와 비슷하게 설정되어 있습니다.
   한 가지 차이점은, 여러분이 <em>번역하지 말아야 하는</em> .data 파일이 있다는 겁니다.
   </dd>

<dt><tt>CD 판매사(CD/vendors/)</tt>
   <dd>CD/vendors/에 있는 *.wml 파일들만 번역되어야 합니다.
   태그의 번역은 po/vendors.<var>xy</var>.po 파일 안의 gettext를 통해
   추가됩니다.
   </dd>

<dt><tt>데비안 조직도(intro/organization.wml)</tt>
   <dd> 태그는 po/organization.<var>xy</var>.po 파일 안의 gettext를 통해
   번역됩니다.
   </dd>

<dt><tt>메일링 리스트 가입 및 탈퇴(MailingLists/{un,}subscribe.wml)</tt>
   <dd> 위의 두 파일은 <tt>mklist</tt> 스크립트를 통해 생성되므로 여러분이
   직접 수정할 수 없습니다. 여러분은 desc/ 하위에 있는 파일들을 수정할 수 있으며,
   해당 파일들에는 메일링 리스트의 설명이 들어 있습니다.
   태그는 po/mailinglists.<var>xy</var>.po 파일 안의 gettext를 통해 번역됩니다.
   </dd>

<dt><tt>컨설턴트(consultants/index.wml)</tt>
   <dd> 태그는 po/consultants.<var>xy</var>.po 파일 안의 gettext를 통해
   번역됩니다.
   </dd>

<dt><tt>릴리스(releases/*/{installmanual,releasenotes}.wml)</tt>
   <dd> &lt;: :&gt;로 둘러쌓인 펄(Perl) 코드를 제외하고 번역해주세요.
   단, permute_as_list의 <strong>두 번째 인자</strong>는 예외적으로 번역합니다. </dd>

<dt><tt>포팅(ports/)</tt>
   <dd> 포팅 페이지는 불안정, 즉 자주 변합니다. 여러분이 업데이트에 시간을 쓸
   결심을 했을 때 번역하기 바랍니다.
   </dd>

<dt><tt>웹사이트(devel/website/)</tt>
   <dd> 웹 페이지를 수정하거나 번역하는 사람들을 위한 곳입니다.
   그러니 우선 순위가 낮습니다. </dd>
</dl>
