#use wml::debian::template title="데비안 퓨어 블렌드" BARETITLE="true"
#use wml::debian::translation-check translation="6d2e0afb56e760364713c2cca2c9f6407c9a744f" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.

<p>데비안 퓨어 블렌드는 특정 필요를 가진 사람들의 그룹을 위한 해결책입니다 .
특정 패키지의 모임(meta-packages)을 제공할 뿐 아니라, 의도한 목적을 위한 설치와 구성을 쉽게 합니다.
다른 사람들(어린이, 과학자, 게이머, 법률가, 의료진, 시각 장애인 등) 모임의 흥미를 다룹니다.
공통 목표는 그들의 고객을 위해 설치와 컴퓨터 관리를 단순화하고, 그들이 사용하는 소프트웨어를 
만든 사람과 연결하거나 그들이 사용하는 소프트웨어를 패키징하는 사람과 함께합니다.
</p>

<p>데비안 퓨어 블렌드에 대해 <a
href="https://blends.debian.org/blends/">Pure Blends Manual</a>에서 좀 더 볼 수 있습니다.</p>

<ul class="toc">
<li><a href="#released">릴리스된 퓨어 블렌드</a></li>
<li><a href="#unreleased">다가오는 퓨어 블렌드</a></li>
<li><a href="#development">개발</a></li>
</ul>

<div class="card" id="released">
<h2>릴리스된 퓨어 블렌드</h2>
<div><p>"릴리스 됨"은 다른 블렌드에 대해 다른 의미를 가질 수 있습니다.
대부분의 경우 그 블렌드에 데비안 안정 릴리스에 나온 메타패키지 또는 설치관리자를 가짐을 뜻합니다.
블렌드는 설치 매체를 제공하거나 파생 배포의 기초를 형성할 수 있습니다.
특정 블렌드의 더 자세한 정보는 개별 블렌드 페이지를 보세요.
</p>

<table class="tabular" summary="">
<tbody>
 <tr>
  <th>블렌드</th>
  <th>설명</th>
  <th>빠른 링크</th>
 </tr>
#include "../../english/blends/released.data"
</tbody>
</table>
</div>
</div>

<div class="card" id="unreleased">
  <h2>다가오는 퓨어 블렌드</h2>
  <div>
   <p>These blends are works-in-progress and have not yet made a stable release
   although they may be available in the <a
href="https://www.debian.org/releases/testing/">testing</a> or <a
href="https://www.debian.org/releases/unstable">unstable</a> distribution. Some
upcoming pure blends may still rely on non-free components.</p>
  
<table class="tabular" summary="">
<tbody>
 <tr>
  <th>블렌드</th>
  <th>설명</th>
  <th>빠른 링크</th>
 </tr>
#include "../../english/blends/unreleased.data"
</tbody>
</table>
  </div>
 </div>
 
 <div class="card" id="development">
  <h2>개발</h2>
  <div>
   <p>데비안 퓨어 블렌드의 개발에 관심 있다면,
<a
href="https://wiki.debian.org/DebianPureBlends">위키</a>에서 개발 정보를 찾을 수 있습니다.
   </p>
  </div>
</div><!-- #main -->

